use std::slice;
use std::sync::Arc;

use crate::core::geometry::Ray;
use crate::core::intersection::Intersection;
use crate::core::math::Lerp;
use crate::core::monte_carlo;
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                      S T R A T I F I E D   S A M P L E R
// -----------------------------------------------------------------------------

// --------------S T R A T I F I E D   S A M P L E R   S T R U C T--------------
#[derive(Debug, Clone, PartialEq)]
pub struct StratifiedSampler {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // common sampler data
    common: SamplerCommon,
    // the number of strata in the x and y directions
    n_strata: (usize, usize),
    // whether samples should be jittered
    jitter: bool,
    // the current pixel position
    pos: (i32, i32),
    // scratch pad memory
    sample_buffer: Vec<GFloat>,
}

// ------S T R A T I F I E D   S A M P L E R   I M P L E M E N T A T I O N------
impl StratifiedSampler {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [StratifiedSampler] from the given parameters
    ///
    /// * `pixels_min` The minimum x and y coordinates of the range of pixels to
    ///                generate samples for.
    /// * `pixels_max` The maximum x and y coordinates (exclusive) of the range
    ///                of pixels to generate samples for.
    /// * `n_strata` The number of strata in the x and y directions.
    /// * `jitter` Whether samples should be jittered.
    /// * shutter_open The time at which the camera shutter opens.
    /// * shutter_close The time at which the camera shutter closes.
    pub fn new(
        pixels_min: (i32, i32),
        pixels_max: (i32, i32),
        n_strata: (usize, usize),
        jitter: bool,
        shutter_open: TFloat,
        shutter_close: TFloat
    ) -> StratifiedSampler {
        StratifiedSampler{
            common: SamplerCommon::new(
                pixels_min,
                pixels_max,
                n_strata.0 * n_strata.1,
                shutter_open,
                shutter_close
            ),
            n_strata: n_strata,
            jitter: jitter,
            pos: pixels_min,
            sample_buffer: vec![0.0; 5 * n_strata.0 * n_strata.1]
        }
    }
}

// -----------------S A M P L E R   I M P L E M E N T A T I O N-----------------
impl Sampler for StratifiedSampler {
    fn common(&self) -> &SamplerCommon {
        &self.common
    }

    fn get_more_samples(
        &mut self,
        samples: &mut Vec<Sample>,
        rng: &mut RNG
    ) -> usize {
        // done sampling?
        if self.pos.1 == self.common.pixels_max.1 {
            return 0;
        }
        let n_samples = self.n_strata.0 * self.n_strata.1;
        // generate initial stratified samples into sample_buffer memory
        let buf_ptr = self.sample_buffer.as_mut_ptr();
        let image_samples;
        let lens_samples;
        let time_samples;
        unsafe {
            image_samples = slice::from_raw_parts_mut(
                buf_ptr,
                2 * n_samples
            );
            lens_samples = slice::from_raw_parts_mut(
                buf_ptr.offset(2 * n_samples as isize),
                2 * n_samples
            );
            time_samples = slice::from_raw_parts_mut(
                buf_ptr.offset(4 * n_samples as isize),
                n_samples
            );
        }
        monte_carlo::stratified_sample_2d(
            image_samples,
            self.n_strata,
            self.jitter,
            rng
        );
        monte_carlo::stratified_sample_2d(
            lens_samples,
            self.n_strata,
            self.jitter,
            rng
        );
        monte_carlo::stratified_sample_1d(
            time_samples,
            self.jitter,
            rng
        );
        // shift stratified image samples to pixel coordinates
        for i in 0..n_samples {
            image_samples[(i * 2) + 0] += self.pos.0 as GFloat;
            image_samples[(i * 2) + 1] += self.pos.1 as GFloat;
        }
        // decorrelate sample dimensions
        monte_carlo::shuffle(lens_samples, n_samples, 2, rng);
        monte_carlo::shuffle(time_samples, n_samples, 1, rng);
        // initialise stratified samples with sample values
        for i in 0..n_samples {
            let s = &mut samples[i];
            let cs: &mut CameraSample = &mut s.camera_sample;
            cs.image_pos = (image_samples[2 * i], image_samples[(2 * i) + 1]);
            cs.lens_pos = (lens_samples[2 * i], lens_samples[(2 * i) + 1]);
            cs.time = TFloat::lerp(
                time_samples[i] as TFloat,
                self.common.shutter_open,
                self.common.shutter_close
            );
            // generate stratified samples for Integrators
            for j in 0..s.n_1d.len() {
                let data_i = s.data_1d_i[j];
                let data_size = s.n_1d[j];
                monte_carlo::latin_hypercube(
                    &mut s.data[data_i..(data_i + data_size)],
                    data_size,
                    1,
                    rng
                );
            }
            for j in 0..s.n_2d.len() {
                let data_i = s.data_2d_i[j];
                let data_size = s.n_2d[j];
                monte_carlo::latin_hypercube(
                    &mut s.data[data_i..(data_i + data_size)],
                    data_size,
                    2,
                    rng
                );
            }
        }
        // advance to next pixel for stratified sampling
        self.pos.0 += 1;
        if self.pos.0 == self.common.pixels_max.0 {
            self.pos.0 = self.common.pixels_min.0;
            self.pos.1 += 1;
        }
        // return
        n_samples
    }

    fn max_sample_count(&self) -> usize {
        return self.n_strata.0 * self.n_strata.1;
    }

    fn report_results(
        &self,
        _samples: &Vec<Sample>,
        _rays: &Vec<Ray>,
        _ls: &Vec<Spectrum>,
        _intersections: &Vec<Option<Intersection>>,
        // TODO: could be removed if we don't prefil samples?
        _sample_count: usize
    ) -> (bool, f32) {
        // compute progress
        let dx = self.common.pixels_max.0 - self.common.pixels_min.0;
        let dy = self.common.pixels_max.1 - self.common.pixels_min.1;
        let n_pixels = dy * dx;
        let px = self.pos.0 - self.common.pixels_min.0;
        let py = self.pos.1 - self.common.pixels_min.1;
        let current_pixel = (py * dx) + px;
        (true, (current_pixel as f32) / (n_pixels as f32))
    }

    fn get_sub_sampler(
        &self,
        num: usize,
        count: usize
    ) -> Option<SamplerArc> {
        let sub_window = self.compute_sub_window(num, count);
        if sub_window.0.0 == sub_window.1.0 ||
           sub_window.0.1 == sub_window.1.1 {
            return None;
        }
        Some(Arc::new(StratifiedSampler::new(
            sub_window.0,
            sub_window.1,
            self.n_strata,
            self.jitter,
            self.common.shutter_open,
            self.common.shutter_close
        )))
    }

    fn round_size(&self, size: usize) -> usize {
        // no prefered size
        size
    }
}
