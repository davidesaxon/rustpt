//! # Sampler implementations

pub mod stratified;

pub use stratified::StratifiedSampler;
