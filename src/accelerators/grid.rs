use std::rc::Rc;
use std::sync::{Arc, RwLock};
use std::time::Instant;

use log::debug;

use crate::core::geometry::*;
use crate::core::intersection::Intersection;
use crate::core::primitive::{self, Primitive, PrimitiveArc};
use crate::core::reflection::BSDF;
use crate::core::types::GFloat;


// -----------------------------------------------------------------------------
//                        G R I D   A C C E L E R A T O R
// -----------------------------------------------------------------------------

// ----------------G R I D   A C C E L E R A T O R   S T R U C T----------------
/// TODO: document
pub struct GridAccelerator {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // The unique identifier of this [Primitive]
    primitive_id: u64,
    // the list of all primitives held by this accelerator
    primitives: Vec<PrimitiveArc>,
    // the bounds that encapsulate all the data of this GridAccelerator
    bounds: AABB,
    // the number of voxels in the x, y, and z axis respectively
    num_voxels: [usize; 3],
    /// the width of voxels for each axis
    width: Vector,
    /// the inverse width of voxels for each axis
    inv_width: Vector,
    /// the voxels of this GridAccelerator
    voxels: Vec<Option<Voxel>>,
}

// --------G R I D   A C C E L E R A T O R   I M P L E M E N T A T I O N--------
impl GridAccelerator {
    // -------------------------C O N S T R U C T O R S-------------------------
    /// Constructs a new [GridAccelerator] that contains the given [Primitive]s.
    ///
    /// If refine_immediately is `true` all [Primitive]s will be fully refined
    /// in order to build an optimal grid.
    pub fn new(
        primitives: Vec<PrimitiveArc>,
        refine_immediately: bool
    ) -> GridAccelerator {
        debug!(
            "Building new Grid Accelerators for {} primitives",
            primitives.len()
        );
        // construct object
        // do refinement?
        let primitives = if refine_immediately {
            debug!(
                "Grid Accelerator performing immediate refinement of all
                primitives..."
            );
            let timer = Instant::now();
            let mut prims: Vec<PrimitiveArc> = Vec::new();
            for primitive in &primitives {
                // TODO: do we really need to retain primitives that can be
                //       refined here?
                prims.push(Arc::clone(&primitive));
                prims.extend(primitive.fully_refine());
            }
            debug!(
                "Grid Accelerator immediate refinement took {:.2} seconds",
                timer.elapsed().as_secs_f32()
            );
            prims
        }
        else {
            primitives
        };
        // construct object
        let mut this = GridAccelerator{
            primitive_id: primitive::get_next_primitive_id(),
            primitives: primitives,
            bounds: AABB::degenerate(),
            num_voxels: [0; 3],
            width: Vector::zero(),
            inv_width: Vector::zero(),
            voxels: Vec::new()
        };
        // compute bounds
        for primitive in &this.primitives {
            this.bounds = this.bounds.union(&primitive.world_bound());
        }
        let delta = &this.bounds.max - &this.bounds.min;
        // find voxels per unit distance for grid
        let inv_max_width = 1.0 / delta[this.bounds.maximum_extent() as usize];
        let cube_root = 3.0 * (this.primitives.len() as GFloat).powf(1.0 / 3.0);
        let voxels_per_unit_dist = cube_root * inv_max_width;
        // choose grid resolution and compute voxel widths
        for axis in 0..3 {
            this.num_voxels[axis] =
                (delta[axis] * voxels_per_unit_dist).round() as usize;
            this.num_voxels[axis] = this.num_voxels[axis].clamp(1, 64);
            this.width[axis] = delta[axis] / (this.num_voxels[axis] as GFloat);
            this.inv_width[axis] = if this.width[axis] == 0.0 {
                0.0
            }
            else {
                1.0 / this.width[axis]
            };
        }
        // allocate voxels
        let total_voxels =
            this.num_voxels[0] * this.num_voxels[1] * this.num_voxels[2];
        // note: can't use vec.resize because Voxel is not clonable
        for _ in 0..total_voxels {
            this.voxels.push(None);
        }
        // TODO: this can be improved since primitives can be added to voxels
        //       that don't contain them. page 201
        // add the primitives to the voxels
        for primitive in &this.primitives {
            // find voxel extent of primitive
            let pb = primitive.world_bound();
            let v_min = this.pos_to_voxel(&pb.min);
            let v_max = this.pos_to_voxel(&pb.max);
            // add primitive to overlapping voxels
            for z in v_min[2]..(v_max[2] + 1) {
                for y in v_min[1]..(v_max[1] + 1) {
                    for x in v_min[0]..(v_max[0] + 1) {
                        let offset = this.voxel_offset(&[x, y, z]);
                        match this.voxels[offset].as_mut() {
                            None => {
                                this.voxels[offset] =
                                    Some(Voxel::new(&primitive));
                            },
                            Some(v) => v.add_primitive(&primitive),
                        }
                    }
                }
            }
        }
        // return
        this
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    // converts a world space position into the coordinates of the voxel that
    // contains the point
    fn pos_to_voxel(&self, p: &Point) -> [i32; 3] {
        let mut vp: [i32; 3] = [0; 3];
        for axis in 0..3 {
            vp[axis] =
                ((p[axis] - self.bounds.min[axis]) * self.inv_width[axis])
                .floor() as i32;
            vp[axis] = vp[axis].clamp(0, (self.num_voxels[axis] - 1) as i32);
        }
        // return
        vp
    }

    // same as pos_to_voxel, but only performs the operation a single given axis
    fn _pos_to_voxel_axis(&self, p: &Point, axis: usize) -> i32 {
        let v =
            ((p[axis] - self.bounds.min[axis]) * self.inv_width[axis])
            .floor() as i32;
        v.clamp(0, (self.num_voxels[axis] - 1) as i32)
    }

    // Converts voxel index coordinates into the lower corner world position of
    // the voxel
    fn _voxel_to_pos(&self, v: &[i32; 3]) -> Point {
        let mut p = self.bounds.min;
        for axis in 0..3 {
            p[axis] += (v[axis] as GFloat) * self.width[axis];
        }
        // return
        p
    }

    fn voxel_to_pos_axis(&self, v: i32, axis: usize) -> GFloat {
        self.bounds.min[axis] + (v as GFloat) * self.width[axis]
    }

    // Returns the 1 dimensional offset into the voxel list for the given 3D
    // voxel coordinate
    fn voxel_offset(&self, v: &[i32; 3]) -> usize {
        ((v[2] as usize) * self.num_voxels[0] * self.num_voxels[1]) +
        ((v[1] as usize) * self.num_voxels[0]) + (v[0] as usize)
    }

    // Setups up digital differential analyzer info for Primitive::intersect and
    // Primitive::intersect_p
    fn setup_dda(&self, ray: &Ray) -> Option<DDAInfo> {
        // check ray against overall grid bounds
        let ray_t = if self.bounds.contains(&ray.eval(ray.min_t)) {
            ray.min_t
        }
        else if let Some((t0, _)) = self.bounds.intersect_p(&ray) {
            t0
        }
        else {
            return None;
        };
        let grid_intersect = ray.eval(ray_t);
        let mut dda_info = DDAInfo::new(self.pos_to_voxel(&grid_intersect));
        for axis in 0..3 {
            // handle ray with positive direction for voxel stepping
            if ray.d[axis] >= 0.0 {
                // if ray.d[axis] is 0 this will result as infinity
                dda_info.next_crossing_t[axis] =
                    ray_t +
                    (self.voxel_to_pos_axis(dda_info.v_pos[axis] + 1, axis) -
                     grid_intersect[axis]) / ray.d[axis];
                dda_info.delta_t[axis] = self.width[axis] / ray.d[axis];
                dda_info.step[axis] = 1;
                dda_info.out[axis] = self.num_voxels[axis] as i32;
            }
            // handle ray with negative direction for voxel stepping
            else {
                dda_info.next_crossing_t[axis] =
                    ray_t +
                    (self.voxel_to_pos_axis(dda_info.v_pos[axis], axis) -
                     grid_intersect[axis]) / ray.d[axis];
                dda_info.delta_t[axis] = (-self.width[axis]) / ray.d[axis];
                dda_info.step[axis] = -1;
                dda_info.out[axis] = -1;
            }
        }
        // return
        Some(dda_info)
    }
}

// ------------------------P R I M I T I V E   T R A I T------------------------
impl Primitive for GridAccelerator {
    fn id(&self) -> u64 {
        self.primitive_id
    }

    fn world_bound(&self) -> AABB {
        self.bounds
    }

    fn intersect(
        &self,
        ray: &mut Ray,
        _this: &PrimitiveArc
    ) -> Option<Intersection> {
        // set up 3d digital differential analyzer (DDA) for ray
        let mut d = if let Some(dda_info) = self.setup_dda(ray) {
            dda_info
        }
        else {
            return None;
        };
        // walk ray through voxel grid
        // TODO: need to aquire a read lock here!
        let mut hit: Option<Intersection> = None;
        loop {
            // check for intersection in current voxel
            if let Some(voxel) =
                self.voxels[self.voxel_offset(&d.v_pos)].as_ref()
            {
                if let Some(i) = voxel.intersect(ray) {
                    hit = Some(i);
                }
            }
            // find step axis for stepping to next voxel
            let bits =
                (((d.next_crossing_t[0] < d.next_crossing_t[1]) as i32) << 2) +
                (((d.next_crossing_t[0] < d.next_crossing_t[2]) as i32) << 1) +
                ((d.next_crossing_t[1] < d.next_crossing_t[2]) as i32);
            let bits_to_axis: [usize; 8] = [2, 1, 2, 1, 2, 2, 0, 0];
            let step_axis = bits_to_axis[bits as usize];
            // advance to the next voxel
            if ray.max_t < d.next_crossing_t[step_axis] {
                // no need to check, ray has already intersected before the next
                // voxel
                break;
            }
            d.v_pos[step_axis] += d.step[step_axis];
            if d.v_pos[step_axis] == d.out[step_axis] {
                // ray has left the voxel grid - no need to check further
                break;
            }
            d.next_crossing_t[step_axis] += d.delta_t[step_axis];
        }
        // TODO: need release read lock here!
        // return
        hit
    }

    fn intersect_p(&self, ray: &Ray) -> bool {
        // set up 3d digital differential analyzer (DDA) for ray
        let mut d = if let Some(dda_info) = self.setup_dda(ray) {
            dda_info
        }
        else {
            return false;
        };
        // walk ray through voxel grid
        // TODO: need to aquire a read lock here!
        loop {
            // check for intersection in current voxel
            if let Some(voxel) =
                self.voxels[self.voxel_offset(&d.v_pos)].as_ref()
            {
                if voxel.intersect_p(ray) {
                    return true;
                }
            }
            // find step axis for stepping to next voxel
            let bits =
                (((d.next_crossing_t[0] < d.next_crossing_t[1]) as i32) << 2) +
                (((d.next_crossing_t[0] < d.next_crossing_t[2]) as i32) << 1) +
                ((d.next_crossing_t[1] < d.next_crossing_t[2]) as i32);
            let bits_to_axis: [usize; 8] = [2, 1, 2, 1, 2, 2, 0, 0];
            let step_axis = bits_to_axis[bits as usize];
            // advance to the next voxel
            if ray.max_t < d.next_crossing_t[step_axis] {
                // no need to check, ray has already intersected before the next
                // voxel
                break;
            }
            d.v_pos[step_axis] += d.step[step_axis];
            if d.v_pos[step_axis] == d.out[step_axis] {
                // ray has left the voxel grid - no need to check further
                break;
            }
            d.next_crossing_t[step_axis] += d.delta_t[step_axis];
        }
        // TODO: need release read lock here!
        // return
        false
    }

    fn get_bsdf(
        &self,
        _dg: &Rc<DifferentialGeometry>,
        _object_to_world: &Transform
    ) -> BSDF {
        panic!("GridAccelerator::get_bsdf should never be called");
    }
}

// -----------------------------------------------------------------------------
//                                   V O X E L
// -----------------------------------------------------------------------------

// ---------------------------V O X E L   S T R U C T---------------------------
struct Voxel {
    /// the primitives that overlap this voxel
    primitives: RwLock<Vec<PrimitiveArc>>,
    /// whether all primitives are intersectable or some need further refinement
    all_intersectable: RwLock<bool>,
}

// -------------------V O X E L   I M P L E M E N T A T I O N-------------------
impl Voxel {
    // -------------------------------CONSTRUCTORS------------------------------
    // constructs a new Voxel with given Primitive overlapping this Voxel
    pub fn new(prim: &PrimitiveArc) -> Voxel {
        Voxel{
            primitives: RwLock::new(vec![Arc::clone(prim)]),
            all_intersectable: RwLock::new(false)
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    // adds the given Primitive as overlapping this Voxel
    pub fn add_primitive(&mut self, prim: &PrimitiveArc) {
        self.primitives.write().unwrap().push(Arc::clone(prim));
    }

    // checks whether the given ray intersects with the contents of this voxel
    // and returns the intersection information
    pub fn intersect(&self, ray: &mut Ray) -> Option<Intersection> {
        // refine primitives in voxel if needed
        if !(*self.all_intersectable.read().unwrap()) {
            self.refine();
        }
        // loop over primitives in voxel and find intersections
        let mut hit: Option<Intersection> = None;
        for primitive in &*self.primitives.read().unwrap() {
            if let Some(i) = primitive.intersect(ray, &primitive) {
                hit = Some(i);
            }
        }
        // return
        hit
    }

    // checks whether the given ray intersects with the contents of this voxel
    pub fn intersect_p(&self, ray: &Ray) -> bool {
        // refine primitives in voxel if needed
        if !(*self.all_intersectable.read().unwrap()) {
            self.refine();
        }
        // loop over all primitives in the voxel and find intersections
        for primitive in &*self.primitives.read().unwrap() {
            if primitive.intersect_p(ray) {
                return true;
            }
        }
        // return
        false
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    fn refine(&self) {
        for primitive in &mut *self.primitives.write().unwrap() {
            // refine primitive if its not intersectable
            if !primitive.can_intersect() {
                let prims = primitive.fully_refine();
                if prims.len() == 1 {
                    *primitive = Arc::clone(&prims[0]);
                }
                else if prims.len() != 0 {
                    // TODO: use prim name?
                    debug!(
                        "Building sub GridAccelerator for primitive {}",
                        primitive.id()
                    );
                    *primitive =
                        Arc::new(GridAccelerator::new(prims, false));
                }
            }
        }
        *self.all_intersectable.write().unwrap() = true;
    }
}

// -----------------------------------------------------------------------------
//                                D D A   I N F O
// -----------------------------------------------------------------------------

// ------------------------D D A   I N F O   S T R U C T------------------------
// Struct that stores info needed for the digital differential analyzer
// algorithm
struct DDAInfo {
    v_pos: [i32; 3],
    next_crossing_t: [GFloat; 3],
    delta_t: [GFloat; 3],
    step: [i32; 3],
    out: [i32; 3],
}

// ----------------D D A   I N F O   I M P L E M E N T A T I O N----------------
impl DDAInfo {
    // -------------------------------CONSTRUCTORS------------------------------
    fn new(v_pos: [i32; 3]) -> DDAInfo {
        DDAInfo {
            v_pos: v_pos,
            next_crossing_t: [0.0; 3],
            delta_t: [0.0; 3],
            step: [0; 3],
            out: [0; 3]
        }
    }
}
