//! # Accelerators implementations

pub mod grid;

pub use grid::GridAccelerator;
