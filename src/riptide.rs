use std::time::Instant;

use log::{debug, info};

use crate::core::config::Config;
use crate::core::spectrum::sampled::SampledSpectrum;

// -----------------------------------------------------------------------------
//                               F U N C T I O N S
// -----------------------------------------------------------------------------

/// Initialises Riptide. Must be called before any other API functions can be
/// called.
pub fn init(config: &Config) {
    let timer = Instant::now();

    // initialise logging
    let logger = fern::Dispatch::new();
    logger
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                "riptide",
                record.level(),
                message
            ))
        })
        .level(config.verbosity)
        // TODO: add support for logging elsewhere
        .chain(std::io::stdout())
        .apply()
        .unwrap();

    info!("Initialising Riptide...");

    // initialise sampled spectrum data
    SampledSpectrum::init();

    debug!(
        "Riptide initialisation took {:.2} seconds",
        timer.elapsed().as_secs_f32()
    );
}
