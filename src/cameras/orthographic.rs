use crate::core::camera::*;
use crate::core::film::*;
use crate::core::geometry::*;
use crate::core::monte_carlo;
use crate::core::sampler::CameraSample;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                     O R T H O G R A P H I C   C A M E R A
// -----------------------------------------------------------------------------

// ------------------------------F U N C T I O N S------------------------------

// builds a [Transform] that represents an orthographic projection
fn build_orthographic_transform(z_near: GFloat, z_far: GFloat) -> Transform {
    &Transform::scaling(1.0, 1.0, 1.0 / (z_far - z_near)) *
    &Transform::translation(&Vector::new(0.0, 0.0, -z_near))
}

// -------------O R T H O G R A P H I C   C A M E R A   S T R U C T-------------
/// Camera that views the scene as an orthographic projection
#[derive(Clone)]
pub struct OrthographicCamera {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: CameraCommon,
    // matrices
    camera_to_screen: Transform,
    raster_to_camera: Transform,
    raster_to_screen: Transform,
    screen_to_raster: Transform,
    // depth of field
    lens_radius: GFloat,
    focal_distance: GFloat,
    // offsets for differential rays
    dx_camera: Vector,
    dy_camera: Vector,
}

// -----O R T H O G R A P H I C   C A M E R A   I M P L E M E N T A T I O N-----
impl OrthographicCamera {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [OrthographicCamera] from the given parameters
    ///
    /// TODO: document parameters
    pub fn new(
        camera_to_world: &AnimatedTransform,
        screen_window: (GFloat, GFloat, GFloat, GFloat),
        shutter_open: TFloat,
        shutter_close: TFloat,
        lens_radius: GFloat,
        focal_distance: GFloat,
        film: &FilmArc
    ) -> OrthographicCamera {
        // compute projective camera transformations
        let camera_to_screen = build_orthographic_transform(0.0, 1.0);
        let screen_to_raster =
            &(&Transform::scaling(
                film.resolution().0 as GFloat,
                film.resolution().1 as GFloat,
                1.0
            ) *
            &Transform::scaling(
                1.0 / (screen_window.1 - screen_window.0),
                1.0 / (screen_window.2 - screen_window.3),
                1.0
            )) *
            &Transform::translation(&Vector::new(
                -screen_window.0,
                -screen_window.3,
                0.0
            ));
        let raster_to_screen = screen_to_raster.inverse();
        let raster_to_camera = &camera_to_screen.inverse() * &raster_to_screen;
        // compute differential changes in origin for orthographic camera rays
        let dx_camera = raster_to_camera.eval(&Vector::new(1.0, 0.0, 0.0));
        let dy_camera = raster_to_camera.eval(&Vector::new(0.0, 1.0, 0.0));
        // construct and return
        OrthographicCamera{
            common: CameraCommon::new(
                camera_to_world,
                shutter_open,
                shutter_close,
                film
            ),
            camera_to_screen: camera_to_screen,
            raster_to_camera: raster_to_camera,
            raster_to_screen: raster_to_screen,
            screen_to_raster: screen_to_raster,
            lens_radius: lens_radius,
            focal_distance: focal_distance,
            dx_camera: dx_camera,
            dy_camera: dy_camera
        }
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    pub fn generate_ray_common(&self, sample: &CameraSample) -> Ray {
        // generate raster and camera samples
        let p_ras = Point::new(sample.image_pos.0, sample.image_pos.1, 0.0);
        let p_camera = self.raster_to_camera.eval(&p_ras);
        // create the ray
        let mut ray = Ray::new_max(
            &p_camera,
            &Vector::new(0.0, 0.0, 1.0),
            0.0,
            GFloat::INFINITY
        );
        // modify ray for depth of field
        if self.lens_radius > 0.0 {
            // sample point on lens
            let lens_pos = monte_carlo::concentric_sample_disk(
                sample.lens_pos.0,
                sample.lens_pos.1
            );
            // compute point on plane of focus
            let ft = self.focal_distance / ray.d.z;
            let p_focus = ray.eval(ft);
            // update ray for effect of lens
            ray.o = Point::new(lens_pos.0, lens_pos.1, 0.0);
            ray.d = (&p_focus - &ray.o).normalize();
        }
        // set ray time
        ray.time = sample.time;
        // return
        ray
    }
}

// ------------------C A M E R A   I M P L E M E N T A T I O N------------------
impl Camera for OrthographicCamera {
    fn common(&self) -> &CameraCommon {
        &self.common
    }

    fn generate_ray(&self, sample: &CameraSample) -> (Ray, GFloat) {
        let mut ray = self.generate_ray_common(sample);
        ray = self.common.camera_to_world.eval(ray.time, &ray);
        // return
        (ray, 1.0)
    }

    fn generate_ray_differential(&self, sample: &CameraSample) -> (Ray, GFloat) {
        let mut ray = self.generate_ray_common(sample);
        // set ray differentials
        ray.differentials = Some(RayDifferentials{
            rx_o: &ray.o + &self.dx_camera,
            rx_d: ray.d,
            ry_o: &ray.o + &self.dy_camera,
            ry_d: ray.d
        });
        ray = self.common.camera_to_world.eval(ray.time, &ray);
        // return
        (ray, 1.0)
    }
}
