use crate::core::camera::*;
use crate::core::film::*;
use crate::core::geometry::*;
use crate::core::math::Matrix44;
use crate::core::monte_carlo;
use crate::core::sampler::CameraSample;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                      P E R S P E C T I V E   C A M E R A
// -----------------------------------------------------------------------------

// ------------------------------F U N C T I O N S------------------------------

// builds a [Transform] that represents an perspective projection
fn build_perspective_transform(
    fov: GFloat,
    z_near: GFloat,
    z_far: GFloat
) -> Transform {
    // perform projective divide
    let persp = Matrix44::new([
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [
            0.0,
            0.0,
            z_far / (z_far - z_near),
            -((z_far * z_near) / (z_far - z_near))
        ],
        [0.0, 0.0, 1.0, 0.0],
    ]);
    // scale to canonical viewing volume
    let inv_tan_ang = 1.0 / (fov.to_radians() / 2.0).tan();
    // return
    &Transform::scaling(inv_tan_ang, inv_tan_ang, 1.0) *
    &Transform::new(&persp, &persp.inverse())
}

// --------------P E R S P E C T I V E   C A M E R A   S T R U C T--------------
/// Camera that views the scene as a perspective projection
#[derive(Clone)]
pub struct PerspectiveCamera {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: CameraCommon,
    // matrices
    camera_to_screen: Transform,
    raster_to_camera: Transform,
    raster_to_screen: Transform,
    screen_to_raster: Transform,
    // depth of field
    lens_radius: GFloat,
    focal_distance: GFloat,
    // offsets for differential rays
    dx_camera: Vector,
    dy_camera: Vector,
}

// ------P E R S P E C T I V E   C A M E R A   I M P L E M E N T A T I O N------
impl PerspectiveCamera {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [PerspectiveCamera] from the given parameters
    ///
    /// TODO: document parameters
    pub fn new(
        camera_to_world: &AnimatedTransform,
        screen_window: (GFloat, GFloat, GFloat, GFloat),
        shutter_open: TFloat,
        shutter_close: TFloat,
        lens_radius: GFloat,
        focal_distance: GFloat,
        fov: GFloat,
        film: &FilmArc
    ) -> PerspectiveCamera {
        // TODO: check for scale
        // https://github.com/mmp/pbrt-v2/blob/master/src/core/camera.cpp#L50
        // compute projective camera transformations
        let camera_to_screen = build_perspective_transform(fov, 1e-2, 1000.0);
        let screen_to_raster =
            &Transform::scaling(
                film.resolution().0 as GFloat,
                film.resolution().1 as GFloat,
                1.0
            ) *
            &(
                &Transform::scaling(
                    1.0 / (screen_window.1 - screen_window.0),
                    1.0 / (screen_window.2 - screen_window.3),
                    1.0
                ) *
                &Transform::translation(&Vector::new(
                    -screen_window.0,
                    -screen_window.3,
                    0.0
                ))
            );
        let raster_to_screen = screen_to_raster.inverse();
        let raster_to_camera = &camera_to_screen.inverse() * &raster_to_screen;
        // compute differential changes in origin for perspective camera rays
        let camera_origin = raster_to_camera.eval(&Point::origin());
        let dx_camera = raster_to_camera.eval(
            &(&Point::new(1.0, 0.0, 0.0) - &camera_origin)
        );
        let dy_camera = raster_to_camera.eval(
            &(&Point::new(0.0, 1.0, 0.0) - &camera_origin)
        );
        // construct and return
        PerspectiveCamera{
            common: CameraCommon::new(
                camera_to_world,
                shutter_open,
                shutter_close,
                film
            ),
            camera_to_screen: camera_to_screen,
            raster_to_camera: raster_to_camera,
            raster_to_screen: raster_to_screen,
            screen_to_raster: screen_to_raster,
            lens_radius: lens_radius,
            focal_distance: focal_distance,
            dx_camera: dx_camera,
            dy_camera: dy_camera
        }
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    pub fn generate_ray_common(&self, sample: &CameraSample) -> (Ray, Point) {
        // generate raster and camera samples
        let p_ras = Point::new(sample.image_pos.0, sample.image_pos.1, 0.0);
        let p_camera = self.raster_to_camera.eval(&p_ras);
        // create the ray
        let mut ray = Ray::new_max(
            &Point::origin(),
            &Vector::from_point(&p_camera),
            0.0,
            GFloat::INFINITY
        );
        // modify ray for depth of field
        if self.lens_radius > 0.0 {
            // sample point on lens
            let lens_pos = monte_carlo::concentric_sample_disk(
                sample.lens_pos.0,
                sample.lens_pos.1
            );
            // compute point on plane of focus
            let ft = self.focal_distance / ray.d.z;
            let p_focus = ray.eval(ft);
            // update ray for effect of lens
            ray.o = Point::new(lens_pos.0, lens_pos.1, 0.0);
            ray.d = (&p_focus - &ray.o).normalize();
        }
        // set ray time
        ray.time = sample.time;
        // return
        (ray, p_camera)
    }
}

// ------------------C A M E R A   I M P L E M E N T A T I O N------------------
impl Camera for PerspectiveCamera {
    fn common(&self) -> &CameraCommon {
        &self.common
    }

    fn generate_ray(&self, sample: &CameraSample) -> (Ray, GFloat) {
        let (mut ray, _) = self.generate_ray_common(sample);
        ray = self.common.camera_to_world.eval(ray.time, &ray);
        // return
        (ray, 1.0)
    }

    fn generate_ray_differential(&self, sample: &CameraSample) -> (Ray, GFloat) {
        let (mut ray, p_camera) = self.generate_ray_common(sample);
        // set ray differentials
        let rx_d =
            (&Vector::from_point(&p_camera) + &self.dx_camera).normalize();
        let ry_d =
            (&Vector::from_point(&p_camera) + &self.dy_camera).normalize();
        ray.differentials = Some(RayDifferentials{
            rx_o: ray.o,
            rx_d: rx_d,
            ry_o: ray.o,
            ry_d: ry_d
        });
        ray = self.common.camera_to_world.eval(ray.time, &ray);
        // return
        (ray, 1.0)
    }
}
