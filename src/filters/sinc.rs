use crate::core::filter::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                     L A N C Z O S   S I N C   F I L T E R
// -----------------------------------------------------------------------------

// -------------L A N C Z O S   S I N C   F I L T E R   S T R U C T-------------
/// Triangle image reconstruction filter
pub struct LanczosSincFilter {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: FilterCommon,
    tau: GFloat,
}

// -----L A N C Z O S   S I N C   F I L T E R   I M P L E M E N T A T I O N-----
impl LanczosSincFilter {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [LanczosSincFilter] with the given parameters
    ///
    /// * `width` The size of the filter
    /// * `tau` Controls how many cycles the sinc function passes through before
    ///         it is clamped to zero
    pub fn new(width: (GFloat, GFloat), tau: GFloat) -> LanczosSincFilter {
        LanczosSincFilter{common: FilterCommon::new(width), tau: tau}
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    pub fn sinc_1d(&self, x: GFloat) -> GFloat {
        let x = x.abs();
        if x < 1e-5 {
            return 1.0;
        }
        if x > 1.0 {
            return 0.0;
        }
        let sinc = (x * self.tau).sin() / (x * self.tau);
        let lanczos = x.sin() / x;
        sinc * lanczos
    }
}

// ------------------F I L T E R   I M P L E M E N T A T I O N------------------
impl Filter for LanczosSincFilter {
    fn common(&self) -> &FilterCommon {
        &self.common
    }

    fn evalulate(&self, pos: (GFloat, GFloat)) -> GFloat {
        (self.common.width.0 - pos.0.abs()).max(0.0) *
        (self.common.width.1 - pos.1.abs()).max(0.0)
    }
}
