//! # Filter implementations

pub mod box_filter;
pub mod gaussian;
pub mod mitchell;
pub mod sinc;
pub mod triangle;

pub use box_filter::BoxFilter;
pub use gaussian::GaussianFilter;
pub use mitchell::MitchellFilter;
pub use sinc::LanczosSincFilter;
pub use triangle::TriangleFilter;
