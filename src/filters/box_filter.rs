use crate::core::filter::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                              B O X   F I L T E R
// -----------------------------------------------------------------------------

// ----------------------B O X   F I L T E R   S T R U C T----------------------
/// Box image reconstruction filter
pub struct BoxFilter {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: FilterCommon,
}

// --------------B O X   F I L T E R   I M P L E M E N T A T I O N--------------
impl BoxFilter {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new(width: (GFloat, GFloat)) -> BoxFilter {
        BoxFilter{common: FilterCommon::new(width)}
    }
}

// ------------------F I L T E R   I M P L E M E N T A T I O N------------------
impl Filter for BoxFilter {
    fn common(&self) -> &FilterCommon {
        &self.common
    }

    fn evalulate(&self, _pos: (GFloat, GFloat)) -> GFloat {
        1.0
    }
}
