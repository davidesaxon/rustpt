use crate::core::filter::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                               F U N C T I O N S
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
//                         M I T C H E L L   F I L T E R
// -----------------------------------------------------------------------------

// -----------------M I T C H E L L   F I L T E R   S T R U C T-----------------
/// Mitchell-Netravali image reconstruction filter
pub struct MitchellFilter {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: FilterCommon,
    b: GFloat,
    c: GFloat
}

// ---------M I T C H E L L   F I L T E R   I M P L E M E N T A T I O N---------
impl MitchellFilter {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [MitchellFilter] with the given parameters. It is
    /// recommended that b and c lie along the line B + 2C = 1 
    pub fn new(
        width: (GFloat, GFloat),
        b: GFloat,
        c: GFloat
    ) -> MitchellFilter {
        MitchellFilter{common: FilterCommon::new(width), b: b, c: c}
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    pub fn mitchell_1d(&self, x: GFloat) -> GFloat {
        let x = (2.0 * x).abs();
        if x > 1.0 {
            return
                ((-self.b - 6.0 * self.c) * x * x * x +
                (6.0 * self.b + 30.0 * self.c) * x * x +
                (-12.0 * self.b - 48.0 * self.c) * x +
                (8.0 * self.b + 24.0 * self.c)) * (1.0 / 6.0);
        }
        ((12.0 - 9.0 * self.b - 6.0 * self.c) * x * x * x +
        (-18.0 + 12.0 * self.b + 6.0 * self.c) * x * x +
        (6.0 - 2.0 * self.b)) * (1.0 / 6.0)
    }
}

// ------------------F I L T E R   I M P L E M E N T A T I O N------------------
impl Filter for MitchellFilter {
    fn common(&self) -> &FilterCommon {
        &self.common
    }

    fn evalulate(&self, pos: (GFloat, GFloat)) -> GFloat {
        self.mitchell_1d(pos.0 * self.common.inv_width.0) *
        self.mitchell_1d(pos.1 * self.common.inv_width.1)
    }
}
