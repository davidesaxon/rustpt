use crate::core::filter::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                         G A U S S I A N   F I L T E R
// -----------------------------------------------------------------------------

// -----------------G A U S S I A N   F I L T E R   S T R U C T-----------------
/// Image reconstruction filter that applies a Gaussian bump centred around the
/// pixel
pub struct GaussianFilter {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: FilterCommon,
    alpha: GFloat,
    exp: (GFloat, GFloat),
}

// ---------G A U S S I A N   F I L T E R   I M P L E M E N T A T I O N---------
impl GaussianFilter {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a [GaussianFilter] with the given parameters
    ///
    /// * `width` The size of the filter
    /// * `alpha` Controls the falloff of the filter, where smaller values have
    ///           have a slower falloff.
    pub fn new(
        width: (GFloat, GFloat),
        alpha: GFloat,
    ) -> GaussianFilter {
        GaussianFilter{
            common: FilterCommon::new(width),
            alpha: alpha,
            exp: (
                (-alpha * width.0 * width.0).exp(),
                (-alpha * width.1 * width.1).exp()
            )
        }
    }
}

// ------------------F I L T E R   I M P L E M E N T A T I O N------------------
impl Filter for GaussianFilter {
    fn common(&self) -> &FilterCommon {
        &self.common
    }

    fn evalulate(&self, pos: (GFloat, GFloat)) -> GFloat {
        // TODO: move to util function?
        ((-self.alpha * pos.0 * pos.0).exp() - self.exp.0).max(0.0) *
        ((-self.alpha * pos.1 * pos.1).exp() - self.exp.1).max(0.0)
    }
}
