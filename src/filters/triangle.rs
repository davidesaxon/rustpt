use crate::core::filter::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                         T R I A N G L E   F I L T E R
// -----------------------------------------------------------------------------

// -----------------T R I A N G L E   F I L T E R   S T R U C T-----------------
/// Triangle image reconstruction filter
pub struct TriangleFilter {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: FilterCommon,
}

// ---------T R I A N G L E   F I L T E R   I M P L E M E N T A T I O N---------
impl TriangleFilter {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new(width: (GFloat, GFloat)) -> TriangleFilter {
        TriangleFilter{common: FilterCommon::new(width)}
    }
}

// ------------------F I L T E R   I M P L E M E N T A T I O N------------------
impl Filter for TriangleFilter {
    fn common(&self) -> &FilterCommon {
        &self.common
    }

    fn evalulate(&self, pos: (GFloat, GFloat)) -> GFloat {
        (self.common.width.0 - pos.0.abs()).max(0.0) *
        (self.common.width.1 - pos.1.abs()).max(0.0)
    }
}
