pub mod accelerators;
pub mod cameras;
pub mod core;
pub mod films;
pub mod filters;
pub mod integrators;
pub mod proto;
pub mod renderers;
pub mod riptide;
pub mod samplers;
pub mod shapes;

use std::env;
use std::path::Path;
use std::process;

#[cfg(test)]
#[macro_use]
extern crate approx;
#[macro_use]
extern crate bitflags;
extern crate num_cpus;
extern crate png;
extern crate tiff;


use crate::core::config::Config;

// TODO: REMOVE ME
use std::sync::Arc;
use log::info;
use crate::cameras::PerspectiveCamera;
use crate::films::ImageFilm;
use crate::filters::*;
use crate::core::camera::*;
use crate::core::film::*;
use crate::core::filter::*;
use crate::core::geometry::*;
use crate::core::integrator::*;
use crate::core::sampler::*;
use crate::core::scene::Scene;
use crate::core::types::*;
use crate::proto::environment::Environment;
use crate::proto::integrators::*;
use crate::proto::scene_builder;
use crate::renderers::sample_renderer::SampleRenderer;
use crate::samplers::stratified::StratifiedSampler;


fn main() {
    // parse the command line arguments and build the program configuration
    // TODO:
    let config = Config::new(env::args()).unwrap_or_else(|err| {
    	println!("ERROR: Problem parsing arguments: {}", err);
        process::exit(1);
	});

    // initialise riptide
    riptide::init(&config);

    // TODO: process scene description
    // TODO: render?
    // TODO: cleanup

    // create the camera
    let cam_t_base = Transform::translation(&Vector::new(0.0, 0.0, -100.0));

    let shutter_duration: TFloat = 0.5;
    let shutter_open = shutter_duration * -0.5;
    let shutter_close = shutter_duration * 0.5;

    // load the environment
    let environment = Arc::new(
        Environment::new(&"res/dev/ibls/oribi.png".to_string())
    );

    // build the scene
    let _root = scene_builder::build();

    // render frames
    let tt = 160;
    let mut r = -25.0;
    let r_acc = -0.1;
    for i in 0..1 {
    // for i in 50..51 {
        // create the camera
        let d: GFloat = if i < 160 {
            i as GFloat
        }
        else {
            (tt - (i - tt)) as GFloat
        };
        r += d * r_acc;
        let r1 = r + ((d + (shutter_duration as GFloat))  * r_acc);

        let cam_t_start = Arc::new(
            &Transform::y_rotation(r) *
            &cam_t_base
        );
        let cam_t_end = Arc::new(
            &Transform::y_rotation(r1) *
            &cam_t_base
        );

        let aperture = 0.3;

        let image_path_str = format!("dev/test_image.{}.tiff", i);
        let film: FilmArc = Arc::new(ImageFilm::new(
            (1024, 1024),
            ((0.0, 0.0), (1.0, 1.0)),
            &(Arc::new(MitchellFilter::new((2.0, 2.0), 0.3, 0.3)) as FilterArc),
            &Path::new(&image_path_str)
        ));
        let _camera: CameraArc = Arc::new(PerspectiveCamera::new(
            &AnimatedTransform::new(
                shutter_open,
                &cam_t_start,
                shutter_close,
                &cam_t_end
            ),
            (-1.0, 1.0, -1.0, 1.0),
            shutter_open,
            shutter_close,
            aperture,
            55.0,
            90.0,
            &film
        ));

        // construct the sampler and renderer
        let sampler: SamplerArc = Arc::new(StratifiedSampler::new(
            (0, 0),
            (1024, 1024),
            (2, 2),
            true,
            shutter_open,
            shutter_close
        ));
        let surface_integrator: SurfaceIntegratorArc = Arc::new(
            ProtoSurfaceIntegrator{}
        );
        let volume_integrator: VolumeIntegratorArc = Arc::new(
            ProtoVolumeIntegrator{}
        );
        let _renderer = SampleRenderer::new(
            &_camera,
            &sampler,
            &surface_integrator,
            &volume_integrator,
            &environment
        );

        // construct the scene
        let _scene = Arc::new(Scene::new(&_root));

        // render!
        // _renderer.render(&_scene);

        // write image
        info!("Writing image: {}", image_path_str);
        // _camera.common().film.write_image(1.0);
    }
}
