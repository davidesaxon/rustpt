use crate::core::types::TFloat;


// -----------------------------------------------------------------------------
//                      F A C E   F O R W A R D   T R A I T
// -----------------------------------------------------------------------------

/// Trait that signifies the type can be faced forwards in relation to another
/// object
pub trait FaceForward<Rhs=Self> {
    /// Returns a version of this so that's orientated to the hemisphere of the
    /// given object
    fn face_forward(&self, rhs: &Rhs) -> Self;
}

// -----------------------------------------------------------------------------
//                              E V A L   T R A I T
// -----------------------------------------------------------------------------

/// Trait that signifies the type can evalute a transform for the other type
pub trait Eval<Rhs> {
    type Output;

    /// Evalutes this transform for the given object
    fn eval(&self, rhs: &Rhs) -> Self::Output;
}

// -----------------------------------------------------------------------------
//                     E V A L   A N I M A T E D   T R A I T
// -----------------------------------------------------------------------------

/// Trait that signifies the type can evalute an animated transform for the
/// other type at a given time
pub trait EvalAnimated<Rhs> {
    type Output;

    /// Evalutes this transform for the given object at time
    fn eval(&self, time: TFloat, rhs: &Rhs) -> Self::Output;
}
