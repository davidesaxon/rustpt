use std::cell::RefCell;
use std::sync::Arc;

use crate::core::geometry::Ray;
use crate::core::math::{Dot, Matrix44};
use crate::core::shape::*;
use crate::core::types::GFloat;
use super::normal::Normal;
use super::point::Point;
use super::vector::Vector;


// -----------------------------------------------------------------------------
//                           D I F F E R E N T I A L S
// -----------------------------------------------------------------------------

// Represents the differentals of a [DifferentialGeometry]
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Differentials {
    /// TODO: document
    pub dpdx: Vector,
    /// TODO: document
    pub dpdy: Vector,
    /// TODO: document
    pub dudx: GFloat,
    /// TODO: document
    pub dudy: GFloat,
    /// TODO: document
    pub dvdx: GFloat,
    /// TODO: document
    pub dvdy: GFloat,
}

// -----------------------------------------------------------------------------
//                   D I F F E R E N T I A L   G E O M E T R Y
// -----------------------------------------------------------------------------

// -----------D I F F E R E N T I A L   G E O M E T R Y   S T R U C T-----------
/// Represents a point on a surface
#[derive(Clone)]
pub struct DifferentialGeometry {
    /// The [Point] of this [DifferentialGeometry] on surface
    pub p: Point,
    /// The outside facing [Normal] of the surface this [DifferentialGeometry]
    /// is on
    pub n: Normal,
    /// The u co-ordinate of this [DifferentialGeometry] on a surface
    pub u: GFloat,
    /// The v co-ordinate of this [DifferentialGeometry] on a surface
    pub v: GFloat,
    /// The u value of the paramatric partial derivatives
    pub dpdu: Vector,
    /// The v value of the paramatric partial derivatives
    pub dpdv: Vector,
    /// The u value of the partial derivatives of the change in surface normal
    pub dndu: Normal,
    /// The v value of the partial derivatives of the change in surface normal
    pub dndv: Normal,
    /// The [Shape] of the surface this [DifferentialGeometry] is on
    pub shape: ShapeArc,
    /// The differentals
    pub differentals: RefCell<Differentials>,
}

// ---D I F F E R E N T I A L   G E O M E T R Y   I M P L E M E N T A T I O N---
impl DifferentialGeometry {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [DifferentialGeometry] with the given parameters.
    ///
    /// **Note:** The normal does not need to be supplied as it is calculated by
    ///           this constructor.
    pub fn new(
        p: Point,
        u: GFloat,
        v: GFloat,
        dpdu: Vector,
        dpdv: Vector,
        dndu: Normal,
        dndv: Normal,
        shape: &ShapeArc
    ) -> DifferentialGeometry {
        let mut n = Normal::from_vector(&dpdu.cross(&dpdv).normalize());
        if shape.common().reverse_orientation !=
           shape.common().transform_swaps_handedness {
            n *= -1.0;
        }
        DifferentialGeometry{
            p: p,
            n: n,
            u: u,
            v: v,
            dpdu: dpdu,
            dpdv: dpdv,
            dndu: dndu,
            dndv: dndv,
            shape: Arc::clone(shape),
            differentals: RefCell::new(Differentials{
                dpdx: Vector::zero(),
                dpdy: Vector::zero(),
                dudx: 0.0,
                dudy: 0.0,
                dvdx: 0.0,
                dvdy: 0.0
            })
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// TODO: document
    pub fn compute_differentials(&self, ray: &Ray) {
        let mut diff = self.differentals.borrow_mut();
        if let Some(rd) = ray.differentials {
            // TODO: this makes the assumption that the surface is locally flat
            //       with respect to the sampling rate at the point being shaded
            //       While this is a reasonable approximation it can however
            //       breakdown for highly curved surfaces or at silhouette edges
            // estimate screen space change in p and (u, v)
            // compute auxiliary intersection points with plane
            let d = -self.n.dot(&Vector::from_point(&self.p));
            let rxv = Vector::from_point(&rd.rx_o);
            let tx = -(self.n.dot(&rxv) + d) / self.n.dot(&rd.rx_d);
            let px = &rd.rx_o + &(&rd.rx_d * tx);
            let ryv = Vector::from_point(&rd.ry_o);
            let ty = -(self.n.dot(&ryv) + d) / self.n.dot(&rd.ry_d);
            let py = &rd.ry_o + &(&rd.ry_d * ty);
            diff.dpdx = &px - &self.p;
            diff.dpdy = &py - &self.p;
            // initialise a, bx, and by matrices for offset computation
            let mut a: [[GFloat; 2]; 2] = [[0.0, 0.0], [0.0, 0.0]];
            let mut bx: [GFloat; 2] = [0.0, 0.0];
            let mut by: [GFloat; 2] = [0.0, 0.0];
            let axes: [usize; 2] =
                if self.n.x.abs() > self.n.y.abs() &&
                   self.n.x.abs() > self.n.z.abs() {
                    [0, 2]
                }
                else if self.n.y.abs() > self.n.z.abs() {
                    [0, 2]
                }
                else {
                    [0, 1]
                };
            // initialise matrices for chosen projection plane
            a[0][0] = self.dpdu[axes[0]];
            a[0][1] = self.dpdv[axes[0]];
            a[1][0] = self.dpdu[axes[1]];
            a[1][1] = self.dpdv[axes[1]];
            bx[0] = px[axes[0]] - self.p[axes[0]];
            bx[1] = px[axes[1]] - self.p[axes[1]];
            by[0] = py[axes[0]] - self.p[axes[0]];
            by[1] = py[axes[1]] - self.p[axes[1]];
            // compute (u, v) offsets at auxiliary points
            if let Some(s) = Matrix44::solve_linear_system_2x2(&a, &bx) {
                diff.dudx = s.0;
                diff.dvdx = s.1;
            }
            else {
                diff.dudx = 0.0;
                diff.dvdx = 0.0;
            }
            if let Some(s) = Matrix44::solve_linear_system_2x2(&a, &by) {
                diff.dudy = s.0;
                diff.dvdy = s.1;
            }
            else {
                diff.dudy = 0.0;
                diff.dvdy = 0.0;
            }
        }
        else {
            diff.dpdx = Vector::zero();
            diff.dpdy = Vector::zero();
            diff.dudx = 0.0;
            diff.dudy = 0.0;
            diff.dvdx = 0.0;
            diff.dvdy = 0.0;
        }
        panic!("DifferentialGeometry::compute_differentials not yet implemented");
    }
}
