//! # Geometry Math
//!
//! Geometric math structures based on a left-handed co-ordinate system.

pub mod aabb;
pub mod diffgeo;
pub mod normal;
pub mod point;
pub mod ray;
pub mod traits;
pub mod transform;
pub mod vector;

pub use aabb::AABB;
pub use diffgeo::DifferentialGeometry;
pub use normal::Normal;
pub use point::Point;
pub use ray::Ray;
pub use ray::RayDifferentials;
pub use traits::{FaceForward, Eval, EvalAnimated};
pub use transform::{AnimatedTransform, Transform};
pub use vector::Vector;


// -----------------------------------------------------------------------------
//                                     ENUMS
// -----------------------------------------------------------------------------

/// Represents the 3 axis in 3D space
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Axis {
    X = 0,
    Y = 1,
    Z = 2
}
