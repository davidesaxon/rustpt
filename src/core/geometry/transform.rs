use std::ops;
use std::sync::Arc;

use crate::core::math::Lerp;
use crate::core::math::Matrix44;
use crate::core::math::Quaternion;
use crate::core::types::{GFloat, TFloat};
use super::aabb::AABB;
use super::normal::Normal;
use super::point::Point;
use super::ray::Ray;
use super::traits::{Eval, EvalAnimated};
use super::vector::Vector;

// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type TransformArc = Arc<Transform>;

// -----------------------------------------------------------------------------
//                               T R A N S F O R M
// -----------------------------------------------------------------------------

// -----------------------T R A N S F O R M   S T R U C T-----------------------
/// A geometric transform represented by a affine 4x4 matrix - the inverse of
/// the matrix is also stored for performance reasons
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Transform {
    pub m: Matrix44,
    pub m_inv: Matrix44,
}

// ---------------T R A N S F O R M   I M P L E M E N T A T I O N---------------
impl Transform {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [Transform] by copying the given [Matrix44] and the
    /// it's provided inverse
    pub fn new(m: &Matrix44, m_inv: &Matrix44) -> Transform {
        Transform{m: *m, m_inv: *m_inv}
    }

    /// Constructs a new [Transform] with no transformation
    pub fn identity() -> Transform {
        Transform{m: Matrix44::identity(), m_inv: Matrix44::identity()}
    }

    /// Constructs a new [Transform] from the given matrix array values
    pub fn from_array(m: [[GFloat; 4]; 4]) -> Transform {
        let mx = Matrix44::new(m);
        Transform{m: mx, m_inv: mx.inverse()}
    }

    /// Constructs a new [Transform] by using a copy of the given [Matrix44]
    pub fn from_matrix(m: &Matrix44) -> Transform {
        Transform{m: *m, m_inv: m.inverse()}
    }

    /// Constructs a new [Transform] that applies a translation represented by
    /// the given [Vector].
    ///
    /// **Note:** Translation affects [Point]s, but not [Vector]s.
    pub fn translation(v: &Vector) -> Transform {
        Transform{
            m: Matrix44::new([
                [1.0, 0.0, 0.0, v.x],
                [0.0, 1.0, 0.0, v.y],
                [0.0, 0.0, 1.0, v.z],
                [0.0, 0.0, 0.0, 1.0]
            ]),
            m_inv: Matrix44::new([
                [1.0, 0.0, 0.0, -v.x],
                [0.0, 1.0, 0.0, -v.y],
                [0.0, 0.0, 1.0, -v.z],
                [0.0, 0.0, 0.0, 1.0]
            ])
        }
    }

    /// Constructs a new [Transform] that applies a scaling in the given 3
    /// dimensions
    pub fn scaling(x: GFloat, y: GFloat, z: GFloat) -> Transform {
        Transform{
            m: Matrix44::new([
                [  x, 0.0, 0.0, 0.0],
                [0.0,   y, 0.0, 0.0],
                [0.0, 0.0,   z, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ]),
            m_inv: Matrix44::new([
                [1.0 / x,     0.0,     0.0, 0.0],
                [    0.0, 1.0 / y,     0.0, 0.0],
                [    0.0,     0.0, 1.0 / z, 0.0],
                [    0.0,     0.0,     0.0, 1.0]
            ])
        }
    }

    /// Constructs a new [Transform] that applies a rotation around the x-axis
    ///
    /// **Note:** Angle is expected in degrees
    pub fn x_rotation(angle: GFloat) -> Transform {
        let angle_rad = angle.to_radians();
        let s = angle_rad.sin();
        let c = angle_rad.cos();
        let m = Matrix44::new([
            [1.0, 0.0, 0.0, 0.0],
            [0.0,   c,  -s, 0.0],
            [0.0,   s,   c, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]);
        Transform{m: m, m_inv: m.transpose()}
    }

    /// Constructs a new [Transform] that applies a rotation around the y-axis
    ///
    /// **Note:** Angle is expected in degrees
    pub fn y_rotation(angle: GFloat) -> Transform {
        let angle_rad = angle.to_radians();
        let s = angle_rad.sin();
        let c = angle_rad.cos();
        let m = Matrix44::new([
            [  c, 0.0,   s, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [ -s, 0.0,   c, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]);
        Transform{m: m, m_inv: m.transpose()}
    }

    /// Constructs a new [Transform] that applies a rotation around the z-axis
    ///
    /// **Note:** Angle is expected in degrees
    pub fn z_rotation(angle: GFloat) -> Transform {
        let angle_rad = angle.to_radians();
        let s = angle_rad.sin();
        let c = angle_rad.cos();
        let m = Matrix44::new([
            [  c,  -s, 0.0, 0.0],
            [  s,   c, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]);
        Transform{m: m, m_inv: m.transpose()}
    }

    /// Constructs a new [Transform] that applies a rotation around the given
    /// arbitrary axis
    ///
    /// **Note:** Angle is expected in degrees
    pub fn rotation(angle: GFloat, axis: &Vector) -> Transform {
        let a = axis.normalize();
        let angle_rad = angle.to_radians();
        let s = angle_rad.sin();
        let c = angle_rad.cos();

        let xx = a.x * a.x;
        let xy = a.x * a.y;
        let xz = a.x * a.z;
        let yy = a.y * a.y;
        let yz = a.y * a.z;
        let zz = a.z * a.z;
        let inv_c = 1.0 - c;
        let xs = a.x * s;
        let ys = a.y * s;
        let zs = a.z * s;

        let m = Matrix44::new([
            [
                xx + (1.0 - xx) * c,
                xy * inv_c - zs,
                xz * inv_c + ys,
                0.0
            ],
            [
                xy * inv_c + zs,
                yy * (1.0 - yy) * c,
                yz * inv_c - xs,
                0.0
            ],
            [
                xz * inv_c - ys,
                yz * inv_c + xs,
                zz + (1.0 - zz) * c,
                0.0
            ],
            [0.0, 0.0, 0.0, 1.0]
        ]);
        Transform{m: m, m_inv: m.transpose()}
    }

    /// Constructs a [Transform] that can be used to place an object at a
    /// position (pos) that is looking at the [Point] (look) orientated by a
    /// [Vector] (up)
    pub fn look_at(pos: &Point, look: &Point, up: &Vector) -> Transform {
        let dir = (look - pos).normalize();
        let left = up.normalize().cross(&dir).normalize();
        let new_up = dir.cross(&left);

        let cam_to_world = Matrix44::new([
            [left.x, new_up.x, dir.x, pos.x],
            [left.y, new_up.y, dir.y, pos.y],
            [left.z, new_up.z, dir.z, pos.z],
            [   0.0,      0.0,   0.0,   1.0]
        ]);
        Transform{m: cam_to_world.inverse(), m_inv: cam_to_world}
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns the inverse of this [Transform] (m and m_inv swapped)
    pub fn inverse(&self) -> Transform {
        Transform::new(&self.m_inv, &self.m)
    }

    /// Returns whether this [Transform] is identity
    pub fn is_identity(&self) -> bool {
        self.m.m[0][0] == 1.0 && self.m.m[0][1] == 0.0 &&
        self.m.m[0][2] == 0.0 && self.m.m[0][3] == 0.0 &&
        self.m.m[1][0] == 0.0 && self.m.m[1][1] == 1.0 &&
        self.m.m[1][2] == 0.0 && self.m.m[1][3] == 0.0 &&
        self.m.m[2][0] == 0.0 && self.m.m[2][1] == 0.0 &&
        self.m.m[2][2] == 1.0 && self.m.m[2][3] == 0.0 &&
        self.m.m[3][0] == 0.0 && self.m.m[3][1] == 0.0 &&
        self.m.m[3][2] == 0.0 && self.m.m[3][3] == 1.0
    }

    /// Returns whether this [Transform] has a scaling factor or not
    pub fn has_scale(&self) -> bool {
        // TODO: optimise?
        // transform 3 co-ordinate lengths and see if any of their lengths are
        // different
        let la1 = self.eval(&Vector::x_unit()).length_squared();
        let la2 = self.eval(&Vector::y_unit()).length_squared();
        let la3 = self.eval(&Vector::z_unit()).length_squared();
        la1 < 0.999 || la1 > 1.001 ||
        la2 < 0.999 || la2 > 1.001 ||
        la3 < 0.999 || la3 > 1.001
    }

    /// Returns whether this Transform swaps the handness of the co-ordinate
    /// system
    pub fn swaps_handedness(&self) -> bool {
        // check if the the determinant of the upper-left 3x3 matrix is negative
        let det =
            (self.m.m[0][0] *
                (self.m.m[1][1] * self.m.m[2][2] -
                 self.m.m[1][2] * self.m.m[2][1])) -
            (self.m.m[0][1] *
                (self.m.m[1][0] * self.m.m[2][2] -
                 self.m.m[1][2] * self.m.m[2][0])) +
            (self.m.m[0][2] *
                (self.m.m[1][0] * self.m.m[2][1] -
                 self.m.m[1][1] * self.m.m[2][0]));
        det < 0.0
    }

    /// Decomposes this [Transform] into translation, rotation, and scaling
    ///
    /// **Note:** This function assumes the internal matrix of this [Transform]
    ///           is affine.
    pub fn decompose_trs(&self) -> (Vector, Quaternion, Matrix44) {
        // extract translation
        let t = Vector::new(self.m.m[0][3], self.m.m[1][3], self.m.m[2][3]);
        // compute the new transformation matrix without translation
        let mut m = self.m;
        m.m[0][3] = 0.0;
        m.m[3][0] = 0.0;
        m.m[1][3] = 0.0;
        m.m[3][1] = 0.0;
        m.m[2][3] = 0.0;
        m.m[3][2] = 0.0;
        m.m[3][3] = 1.0;
        // TODO: optimise this!
        // extract rotation from the matrix - using polar decomposition
        let mut count: usize = 0;
        let mut norm: GFloat = 1.0;
        let mut rm = m;
        while count < 100 && norm > 0.0001 {
            count += 1;
            // compute next matrix in series
            let mut r_next = Matrix44::zero();
            let r_it = rm.transpose().inverse();
            for i in 0..4 {
                for j in 0..4 {
                    r_next.m[i][j] = 0.5 * (rm.m[i][j] + r_it.m[i][j]);
                }
            }
            // compute norm of difference between r and r_next
            norm = 0.0;
            for i in 0..3 {
                let n =
                    (rm.m[i][0] - r_next.m[i][0]).abs() +
                    (rm.m[i][1] - r_next.m[i][1]).abs() +
                    (rm.m[i][2] - r_next.m[i][2]).abs();
                norm = norm.max(n);
            }
            rm = r_next;
        }
        // TODO: https://github.com/mmp/pbrt-v2/blob/master/src/core/transform.cpp#L363
        // compute scale using rotation and original matrix, and return
        (t, Quaternion::from_matrix44(&rm), &rm.inverse() * &m)
    }
}

// ----------------T R A N S F O R M   M U L   T R A N S F O R M----------------
impl ops::Mul<&Transform> for &Transform {
    type Output = Transform;

    fn mul(self, other: &Transform) -> Transform {
        Transform::new(
            &(&self.m * &other.m),
            &(&other.m_inv * &self.m_inv)
        )
    }
}

// ---------T R A N S F O R M   M U L   A S S I G N   T R A N S F O R M---------
impl ops::MulAssign<&Transform> for Transform {
    fn mul_assign(&mut self, other: &Transform) {
        self.m *= &other.m;
        self.m_inv = &other.m_inv * &self.m_inv;
    }
}

// -------------------T R A N S F O R M   E V A L   P O I N T-------------------
impl Eval<Point> for Transform {
    type Output = Point;

    fn eval(&self, p: &Point) -> Self::Output {
        let xp =
            self.m.m[0][0] * p.x +
            self.m.m[0][1] * p.y +
            self.m.m[0][2] * p.z +
            self.m.m[0][3];
        let yp =
            self.m.m[1][0] * p.x +
            self.m.m[1][1] * p.y +
            self.m.m[1][2] * p.z +
            self.m.m[1][3];
        let zp =
            self.m.m[2][0] * p.x +
            self.m.m[2][1] * p.y +
            self.m.m[2][2] * p.z +
            self.m.m[2][3];
        let wp =
            self.m.m[3][0] * p.x +
            self.m.m[3][1] * p.y +
            self.m.m[3][2] * p.z +
            self.m.m[3][3];

        if wp == 1.0 {
            return Point::new(xp, yp, zp);
        }
        &Point::new(xp, yp, zp) / wp
    }
}

// ------------------T R A N S F O R M   E V A L   V E C T O R------------------
impl Eval<Vector> for Transform {
    type Output = Vector;

    fn eval(&self, v: &Vector) -> Self::Output {
        Vector::new(
            self.m.m[0][0] * v.x + self.m.m[0][1] * v.y + self.m.m[0][2] * v.z,
            self.m.m[1][0] * v.x + self.m.m[1][1] * v.y + self.m.m[1][2] * v.z,
            self.m.m[2][0] * v.x + self.m.m[2][1] * v.y + self.m.m[2][2] * v.z
        )
    }
}

// ------------------T R A N S F O R M   E V A L   N O R M A L------------------
impl Eval<Normal> for Transform {
    type Output = Normal;

    fn eval(&self, n: &Normal) -> Self::Output {
        // Note: this method is the main reason Transform's compute and store
        //       the inverse ahead of time
        // we don't compute the transpose over the inverse matrix - instead we
        // just index into it in a different order
        Normal::new(
            self.m_inv.m[0][0] * n.x +
                self.m_inv.m[1][0] * n.y +
                self.m_inv.m[2][0] * n.z,
            self.m_inv.m[0][1] * n.x +
                self.m_inv.m[1][1] * n.y +
                self.m_inv.m[2][1] * n.z,
            self.m_inv.m[0][2] * n.x +
                self.m_inv.m[1][2] * n.y +
                self.m_inv.m[2][2] * n.z
        )
    }
}

// ---------------------T R A N S F O R M   E V A L   R A Y---------------------
impl Eval<Ray> for Transform {
    type Output = Ray;

    fn eval(&self, r: &Ray) -> Self::Output {
        let mut ret: Ray = *r;
        // call eval for origin and direction
        ret.o = self.eval(&ret.o);
        ret.d = self.eval(&ret.d);
        // transform differentials?
        if let Some(rd) = ret.differentials.as_mut() {
            rd.rx_o = self.eval(&rd.rx_o);
            rd.rx_d = self.eval(&rd.rx_d);
            rd.ry_o = self.eval(&rd.ry_o);
            rd.ry_d = self.eval(&rd.ry_d);
        }
        // return
        ret
    }
}

// --------------------T R A N S F O R M   E V A L   A A B B--------------------
impl Eval<AABB> for Transform {
    type Output = AABB;

    fn eval(&self, bb: &AABB) -> Self::Output {
        // TODO: optimise this!
        // transform all 8 corners of the bounding box and compute the new
        // boundings box that contains them
        let mut ret = AABB::enclose(&self.eval(&Point::new(
            bb.min.x,
            bb.min.y,
            bb.min.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.max.x,
            bb.min.y,
            bb.min.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.min.x,
            bb.max.y,
            bb.min.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.min.x,
            bb.min.y,
            bb.max.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.min.x,
            bb.max.y,
            bb.max.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.max.x,
            bb.max.y,
            bb.min.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.max.x,
            bb.min.y,
            bb.max.z,
        )));
        ret = ret.extend(&self.eval(&Point::new(
            bb.max.x,
            bb.max.y,
            bb.max.z,
        )));
        // return
        ret
    }
}

// -----------------------------------------------------------------------------
//                      A N I M A T E D   T R A N S F O R M
// -----------------------------------------------------------------------------

// TODO: in the future this should be replaced with a proper multi-sample
//       animation system

// --------------A N I M A T E D   T R A N S F O R M   S T R U C T--------------
/// Defines keyframe animation for 2 [Transform]s
#[derive(Debug, Clone, PartialEq)]
pub struct AnimatedTransform {
    /// The start time of the animation
    pub start_time: TFloat,
    /// The [Transform] at start_time
    pub start_transform: TransformArc,
    /// The end time of the animation
    pub end_time: TFloat,
    /// The [Transform] at end_time
    pub end_transform: TransformArc,
    /// Whether this transform is actually animated.
    pub has_animation: bool,

    start_t: Vector,
    start_r: Quaternion,
    start_s: Matrix44,
    end_t: Vector,
    end_r: Quaternion,
    end_s: Matrix44,
}

// ------A N I M A T E D   T R A N S F O R M   I M P L E M E N T A T I O N------
impl AnimatedTransform {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [AnimatedTransform] with the given parameters
    pub fn new(
        start_time: TFloat,
        start_transform: &TransformArc,
        end_time: TFloat,
        end_transform: &TransformArc)
    -> AnimatedTransform {
        // decompose transform components first
        let (start_t, start_r, start_s) = start_transform.decompose_trs();
        let (end_t, end_r, end_s) = end_transform.decompose_trs();
        // construct and return
        AnimatedTransform{
            start_time: start_time,
            start_transform: Arc::clone(start_transform),
            end_time: end_time,
            end_transform: Arc::clone(end_transform),
            has_animation: !Arc::ptr_eq(&start_transform, &end_transform),
            start_t: start_t,
            start_r: start_r,
            start_s: start_s,
            end_t: end_t,
            end_r: end_r,
            end_s: end_s,
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Computes the interpolated [Transform] matrix for the given time in this
    /// [AnimatedTransform]
    ///
    /// **Note:** t is clamped to start_time -> end_time
    pub fn interpolate(&self, t: TFloat) -> TransformArc {
        // handle boundry conditions
        if !self.has_animation || t <= self.start_time {
            return Arc::clone(&self.start_transform);
        }
        if t >= self.end_time {
            return Arc::clone(&self.end_transform);
        }
        // compute delta time
        let dt = (t - self.start_time) / (self.end_time - self.start_time);
        // interpolate translation
        let trans =
            &(&self.start_t * (1.0 - dt as GFloat)) +
            &(&self.end_t * dt as GFloat);
        // interpolate rotation
        let rot = self.start_r.slerp(dt, &self.end_r);
        // interpolate scale
        let mut scale = Matrix44::identity();
        for i in 0..3 {
            for j in 0..3 {
                scale.m[i][j] =
                    GFloat::lerp(dt, self.start_s.m[i][j], self.end_s.m[i][j]);
            }
        }
        // return resulting transform
        Arc::new(Transform::from_matrix(
            &(&(&trans.to_matrix44() *
            &rot.to_matrix44()) *
            &scale)
        ))
    }

    /// Computes the bounding box that contains the given bounding box over
    /// this animation.
    ///
    /// * `use_inverse` Controls whether this function uses the inverse
    //                  [Transform] or not.
    pub fn compute_motion_bounds(
        &self,
        bb: &AABB,
        use_inverse: bool
    ) -> AABB {
        if !self.has_animation {
            if use_inverse {
                return self.start_transform.inverse().eval(bb);
            }
            return self.start_transform.eval(bb);
        }
        let ret = AABB::degenerate();
        let nsteps = 128;
        for i in 0..nsteps {
            let time = TFloat::lerp(
                (i as TFloat) / ((nsteps - 1) as TFloat),
                self.start_time,
                self.end_time
            );
            let mut t = *self.interpolate(time);
            if use_inverse {
                t = t.inverse();
            }
            ret.union(&t.eval(bb));
        }
        // return
        ret
    }
}

// -----------A N I M A T E D T R A N S F O R M   E V A L   P O I N T-----------
impl EvalAnimated<Point> for AnimatedTransform {
    type Output = Point;

    fn eval(&self, time: TFloat, p: &Point) -> Self::Output {
        if !self.has_animation || time <= self.start_time {
            return self.start_transform.eval(p);
        }
        if time >= self.end_time {
            return self.end_transform.eval(p);
        }
        let t = self.interpolate(time);
        t.eval(p)
    }
}

// ----------A N I M A T E D T R A N S F O R M   E V A L   V E C T O R----------
impl EvalAnimated<Vector> for AnimatedTransform {
    type Output = Vector;

    fn eval(&self, time: TFloat, v: &Vector) -> Self::Output {
        if !self.has_animation || time <= self.start_time {
            return self.start_transform.eval(v);
        }
        if time >= self.end_time {
            return self.end_transform.eval(v);
        }
        let t = self.interpolate(time);
        t.eval(v)
    }
}

// ----------A N I M A T E D T R A N S F O R M   E V A L   N O R M A L----------
impl EvalAnimated<Normal> for AnimatedTransform {
    type Output = Normal;

    fn eval(&self, time: TFloat, n: &Normal) -> Self::Output {
        if !self.has_animation || time <= self.start_time {
            return self.start_transform.eval(n);
        }
        if time >= self.end_time {
            return self.end_transform.eval(n);
        }
        let t = self.interpolate(time);
        t.eval(n)
    }
}

// -------------A N I M A T E D T R A N S F O R M   E V A L   R A Y-------------
impl EvalAnimated<Ray> for AnimatedTransform {
    type Output = Ray;

    fn eval(&self, time: TFloat, r: &Ray) -> Self::Output {
        if !self.has_animation || time <= self.start_time {
            return self.start_transform.eval(r);
        }
        if time >= self.end_time {
            return self.end_transform.eval(r);
        }
        let t = self.interpolate(time);
        t.eval(r)
    }
}

// ------------A N I M A T E D T R A N S F O R M   E V A L   A A B B------------
impl EvalAnimated<AABB> for AnimatedTransform {
    type Output = AABB;

    fn eval(&self, time: TFloat, bb: &AABB) -> Self::Output {
        if !self.has_animation || time <= self.start_time {
            return self.start_transform.eval(bb);
        }
        if time >= self.end_time {
            return self.end_transform.eval(bb);
        }
        let t = self.interpolate(time);
        t.eval(bb)
    }
}


// TODO: test
