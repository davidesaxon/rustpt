use crate::core::types::{GFloat, TFloat};
use super::point::Point;
use super::vector::Vector;


// -----------------------------------------------------------------------------
//                       R A Y   D I F F E R E N T I A L S
// -----------------------------------------------------------------------------

/// Represents the optional differentials of a [Ray]: two auxiliary Rays which
/// are offset one sample in the x and y direction respectively from the main
/// [Ray] on the film plane
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct RayDifferentials {
    pub rx_o: Point,
    pub rx_d: Vector,
    pub ry_o: Point,
    pub ry_d: Vector,
}

// -----------------------------------------------------------------------------
//                                     R A Y
// -----------------------------------------------------------------------------

// -----------------------------R A Y   S T R U C T-----------------------------
/// A ray specificed by an origin and direction: r(t) = o + td where t is some
/// scalar value
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Ray {
    /// The origin point of the [Ray]
    pub o: Point,
    /// The direction of the [Ray]
    pub d: Vector,
    // TODO: should we allow these values to be interior mutable?
    /// The minimum limit of t value
    pub min_t: GFloat,
    /// The maximum limit of t value
    pub max_t: GFloat,
    /// The time of the scene this [Ray] is associated with - used for motion
    /// blur
    pub time: TFloat,
    /// The depth of this [Ray] in terms of path tracing - used to track how
    /// many times light has bounced before it reaches the camera
    pub depth: u32,
    /// The optional differentials of this [Ray]
    pub differentials: Option<RayDifferentials>,
}

// ---------------------R A Y   I M P L E M E N T A T I O N---------------------
impl Ray {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Construct a new [Ray] with the given origin, direction, and minimum t
    /// value (should be a small non-zero epsilon value)
    pub fn new(o: &Point, d: &Vector, min_t: GFloat) -> Ray {
        Ray{
            o: *o,
            d: *d,
            min_t: min_t,
            max_t: GFloat::INFINITY,
            time: 0.0,
            depth: 0,
            differentials: None
        }
    }

    /// Construct a new [Ray] with the given origin, direction, minimum t
    /// value, and maximum t value
    pub fn new_max(
            o: &Point,
            d: &Vector,
            min_t: GFloat,
            max_t: GFloat
    ) -> Ray {
        Ray{
            o: *o,
            d: *d,
            min_t: min_t,
            max_t: max_t,
            time: 0.0,
            depth: 0,
            differentials: None
        }
    }

    /// Construct a new [Ray] with the given origin, direction, and minimum t
    /// value, and time
    pub fn new_time(o: &Point, d: &Vector, min_t: GFloat, time: TFloat) -> Ray {
        Ray{
            o: *o,
            d: *d,
            min_t: min_t,
            max_t: GFloat::INFINITY,
            time: time,
            depth: 0,
            differentials: None
        }
    }

    /// Construct a new [Ray] with the given origin, direction, minimum t
    /// value, maximum t value, and time
    pub fn new_max_time(
            o: &Point,
            d: &Vector,
            min_t: GFloat,
            max_t: GFloat,
            time: TFloat
    ) -> Ray {
        Ray{
            o: *o,
            d: *d,
            min_t: min_t,
            max_t: max_t,
            time: time,
            depth: 0,
            differentials: None
        }
    }

    /// Constructs a new  [Ray] with the given origin, direction, minimum t
    /// value, copies the time from the parent [Ray], and increments the depth
    /// from the parent [Ray]
    pub fn new_child(
        o: &Point,
        d: &Vector,
        min_t: GFloat,
        parent: &Ray
    ) -> Ray
    {
        Ray{
            o: *o,
            d: *d,
            min_t: min_t,
            max_t: GFloat::INFINITY,
            time: parent.time,
            depth: parent.depth + 1,
            differentials: None
        }
    }

    /// Constructs a new [Ray] at origin with no direction
    pub fn zero() -> Ray {
        Ray{
            o: Point::origin(),
            d: Vector::zero(),
            min_t: 0.0,
            max_t: GFloat::INFINITY,
            time: 0.0,
            depth: 0,
            differentials: None
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns the [Point] for the given t value using this [Ray] as a function
    /// (i.e. the [Point] at t length along the [Ray])
    pub fn eval(&self, t: GFloat) -> Point {
        &self.o + &(&self.d * t)
    }

    /// Updates the differentials of this [Ray] to for the given sample space s
    pub fn scale_differentials(&mut self, s: GFloat) {
        if let Some(rd) = self.differentials.as_mut() {
            rd.rx_o = &self.o + &(&(&rd.rx_o - &self.o) * s);
            rd.rx_d = &self.d + &(&(&rd.rx_d - &self.d) * s);
            rd.ry_o = &self.o + &(&(&rd.ry_o - &self.o) * s);
            rd.ry_d = &self.d + &(&(&rd.ry_d - &self.d) * s);
        }
    }
}


// -----------------------------------------------------------------------------
//                                     TESTS
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: other test?

    #[test]
    fn scale_differentials() {
        // TODO: this test shoud be better
        let mut r = Ray::new(
            &Point::new(0.5, 0.5, 0.5),
            &Vector::new(1.0, 0.0, 0.0),
            0.0001
        );
        r.differentials = Some(RayDifferentials{
            rx_o: Point::new(1.0, 0.5, 0.5),
            rx_d: Vector::new(1.0, 0.0, 0.0),
            ry_o: Point::new(0.5, 1.0, 0.5),
            ry_d: Vector::new(0.0, 1.0, 0.0)
        });
        r.scale_differentials(0.5);
        let rd = &r.differentials.unwrap();
        assert_eq!(rd.rx_o, Point::new(0.75, 0.5, 0.5));
        assert_eq!(rd.rx_d, Vector::new(1.0, 0.0, 0.0));
        assert_eq!(rd.ry_o, Point::new(0.5, 0.75, 0.5));
        assert_eq!(rd.ry_d, Vector::new(0.5, 0.5, 0.0));
    }
}
