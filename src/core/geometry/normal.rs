use std::ops;

use crate::core::math::Dot;
use crate::core::types::GFloat;
use super::traits::FaceForward;
use super::vector::Vector;


// -----------------------------------------------------------------------------
//                                  N O R M A L
// -----------------------------------------------------------------------------

// -------------------------N O R M A L   S T R U C T---------------------------
/// A vector that is perpendicular to a surface
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Normal {
    pub x: GFloat,
    pub y: GFloat,
    pub z: GFloat,
}

// -----------------N O R M A L   I M P L E M E N T A T I O N-------------------
impl Normal {
    // -----------------------------CONSTRUCTORS--------------------------------
    /// Constructs a new [Normal] with the given 3 components
    ///
    /// **Note:** During debug builds this function will panic if any of the
    ///           components are NaNs.
    pub fn new(x: GFloat, y: GFloat, z: GFloat) -> Normal {
        let n = Normal{x, y, z};
        debug_assert!(
            !n.has_nans(),
            "Attempted to contruct Normal with NaN components: {:?}",
            n
        );
        n
    }

    /// Constructs a new [Normal] with all 3 components set to the given value
    pub fn broadcast(s: GFloat) -> Normal {
        Normal{x: s, y: s, z: s}
    }

    /// Constructs a new [Normal] from the values of the given [Vector]
    pub fn from_vector(v: &Vector) -> Normal {
        Normal{x: v.x, y: v.y, z: v.z}
    }

    // ---------------------------PUBLIC FUNCTIONS------------------------------
    /// Returns whether any of this [Normal]'s 3 components are NaNs
    pub fn has_nans(&self) -> bool {
        self.x.is_nan() || self.y.is_nan() || self.z.is_nan()
    }

    /// Returns the length (magnitude) of this [Normal] squared
    pub fn length_squared(&self) -> GFloat {
        (self.x * self.x) + (self.y * self.y) + (self.z * self.z)
    }

    /// Returns the length (magnitude) of this [Normal]
    pub fn length(&self) -> GFloat {
        self.length_squared().sqrt()
    }

    /// Returns a new normalized version of this [Normal]
    pub fn normalize(&self) -> Normal {
        self / self.length()
    }
}

// ---------------------------N O R M A L   I N D E X---------------------------
impl ops::Index<usize> for Normal {
    type Output = GFloat;

    fn index<'a>(&'a self, i: usize) -> &'a Self::Output {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => panic!("Normal[] index {} out of range", i),
        }
    }
}

// -----------------------N O R M A L   I N D E X   M U T-----------------------
impl ops::IndexMut<usize> for Normal {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => panic!("Normal[] index {} out of range", i),
        }
    }
}

// ----------------------------N O R M A L   N E G------------------------------
impl ops::Neg for &Normal {
    type Output = Normal;

    fn neg(self) -> Normal {
        Normal{x: -self.x, y: -self.y, z: -self.z}
    }
}

// ---------------------N O R M A L   A D D   N O R M A L-----------------------
impl ops::Add<&Normal> for &Normal{
    type Output = Normal;

    fn add(self, v: &Normal) -> Normal {
        Normal{x: self.x + v.x, y: self.y + v.y, z: self.z + v.z}
    }
}

// --------------N O R M A L   A D D   A S S I G N   N O R M A L----------------
impl ops::AddAssign<&Normal> for Normal {
    fn add_assign(&mut self, v: &Normal) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
    }
}

// ---------------------N O R M A L   S U B   N O R M A L-----------------------
impl ops::Sub<&Normal> for &Normal{
    type Output = Normal;

    fn sub(self, v: &Normal) -> Normal {
        Normal{x: self.x - v.x, y: self.y - v.y, z: self.z - v.z}
    }
}

// --------------N O R M A L   S U B   A S S I G N   N O R M A L----------------
impl ops::SubAssign<&Normal> for Normal {
    fn sub_assign(&mut self, v: &Normal) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
    }
}

// ---------------------N O R M A L   M U L   S C A L A R-----------------------
impl ops::Mul<GFloat> for &Normal {
    type Output = Normal;

    fn mul(self, s: GFloat) -> Normal {
        Normal{x: self.x * s, y: self.y * s, z: self.z * s}
    }
}

// --------------N O R M A L   M U L   A S S I G N   S C A L A R----------------
impl ops::MulAssign<GFloat> for Normal {
    fn mul_assign(&mut self, s: GFloat) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
    }
}

// ---------------------N O R M A L   D I V   S C A L A R-----------------------
impl ops::Div<GFloat> for &Normal {
    type Output = Normal;

    fn div(self, s: GFloat) -> Normal {
        debug_assert!(s != 0.0, "Attempted to divide Normal by 0 scalar");
        let inv = 1.0 / s;
        Normal{x: self.x * inv, y: self.y * inv, z: self.z * inv}
    }
}

// --------------N O R M A L   D I V   A S S I G N   S C A L A R----------------
impl ops::DivAssign<GFloat> for Normal {
    fn div_assign(&mut self, s: GFloat) {
        debug_assert!(s != 0.0, "Attempted to divide Normal by 0 scalar");
        let inv = 1.0 / s;
        self.x *= inv;
        self.y *= inv;
        self.z *= inv;
    }
}

// ----------------------N O R M A L   D O T   N O R M A L----------------------
impl Dot<Normal> for Normal {
    fn dot(&self, n: &Normal) -> GFloat {
        (self.x * n.x) + (self.y * n.y) + (self.z * n.z)
    }
}

// ----------------------N O R M A L   D O T   V E C T O R----------------------
impl Dot<Vector> for Normal {
    fn dot(&self, v: &Vector) -> GFloat {
        (self.x * v.x) + (self.y * v.y) + (self.z * v.z)
    }
}

// -------------N O R M A L   F A C E   F O R W A R D   N O R M A L-------------
impl FaceForward<Normal> for Normal {
    fn face_forward(&self, n: &Normal) -> Self {
        if self.dot(n) < 0.0 {
            return -self;
        }
        Normal::clone(self)
    }
}

// -------------N O R M A L   F A C E   F O R W A R D   V E C T O R-------------
impl FaceForward<Vector> for Normal {
    fn face_forward(&self, v: &Vector) -> Self {
        if self.dot(v) < 0.0 {
            return -self;
        }
        Normal::clone(self)
    }
}

// -----------------------------------------------------------------------------
//                                     TESTS
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn equals() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n3 = Normal{x: 4.0, y: 2.0, z: 3.0};
        let n4 = Normal{x: 1.0, y: 5.0, z: 3.0};
        let n5 = Normal{x: 1.0, y: 2.0, z: 6.0};
        let n6 = Normal{x: 0.0, y: 0.0, z: 0.0};
        assert!(n1 == n1);
        assert!(n1 == n2);
        assert!(n2 == n1);
        assert!(n1 != n3);
        assert!(n1 != n4);
        assert!(n1 != n5);
        assert!(n1 != n6);
    }

    #[test]
    fn neg() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 0.0, y: 0.0, z: 0.0};
        let n3 = Normal{x: -1.0, y: -2.0, z: -3.0};
        let n4 = Normal{x: 1.0, y: -2.0, z: 0.0};

        assert_eq!(-&n1, Normal{x: -1.0, y: -2.0, z: -3.0});
        assert_eq!(-&n2, n2);
        assert_eq!(-&n3, Normal{x: 1.0, y: 2.0, z: 3.0});
        assert_eq!(-&n4, Normal{x: -1.0, y: 2.0, z: 0.0});
    }

    #[test]
    fn add() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&n1 + &n2, Normal{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn add_assign() {
        let mut n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: -5.0, z: 6.0};

        n1 += &n2;
        assert_eq!(n1, Normal{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn sub() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&n1 - &n2, Normal{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn sub_assign() {
        let mut n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: -5.0, z: 6.0};
        n1 -= &n2;
        assert_eq!(n1, Normal{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn mul_scalar() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        assert_eq!(&n1 * 4.0, Normal{x: 4.0, y: 8.0, z: 12.0});
    }

    #[test]
    fn mul_assign_scalar() {
        let mut n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        n1 *= -4.0;
        assert_eq!(n1, Normal{x: -4.0, y: -8.0, z: -12.0});
    }

    #[test]
    fn div_scalar() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 4.0};
        assert_eq!(&n1 / 4.0, Normal{x: 0.25, y: 0.5, z: 1.0});
    }

    #[test]
    fn div_assign_scalar() {
        let mut n1 = Normal{x: 1.0, y: 2.0, z: 4.0};
        n1 /= -4.0;
        assert_eq!(n1, Normal{x: -0.25, y: -0.5, z: -1.0});
    }

    #[test]
    fn has_nans() {
        let nan: GFloat = (-1.0 as GFloat).sqrt();
        assert!(!Normal{x: 1.0, y: 2.0, z: 3.0}.has_nans());
        assert!(Normal{x: nan, y: 2.0, z: 3.0}.has_nans());
        assert!(Normal{x: 1.0, y: nan, z: 3.0}.has_nans());
        assert!(Normal{x: 1.0, y: 2.0, z: nan}.has_nans());
        assert!(Normal{x: nan, y: nan, z: nan}.has_nans());
    }

    #[test]
    fn dot() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(n1.dot(&n2), 32.0);
        assert_eq!((-&n1).dot(&n2), -32.0);
    }

    #[test]
    fn dot_vector() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(n1.dot(&v1), 32.0);
        assert_eq!((-&n1).dot(&v1), -32.0);
    }

    #[test]
    fn abs_dot() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let n2 = Normal{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(n1.abs_dot(&n2), 32.0);
        assert_eq!((-&n1).abs_dot(&n2), 32.0);
    }

    #[test]
    fn abs_dot_vector() {
        let n1 = Normal{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(n1.abs_dot(&v1), 32.0);
        assert_eq!((-&n1).abs_dot(&v1), 32.0);
    }

    #[test]
    fn face_forward() {
        let n1 = Normal{x: 1.0, y: 0.0, z: 0.0};
        let v1 = Vector{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(n1.face_forward(&v1), n1);
        assert_eq!(n1.face_forward(&-&v1), Normal{x: -1.0, y: 0.0, z: 0.0});
    }

    #[test]
    fn face_forward_normal() {
        let n1 = Normal{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(n1.face_forward(&n1), n1);
        assert_eq!(
            n1.face_forward(&-&n1),
            Normal{x: -1.0, y: 0.0, z: 0.0}
        );
    }
}
