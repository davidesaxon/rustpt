use std::mem;

use crate::core::math::Lerp;
use crate::core::types::{GFloat, TFloat};
use super::Axis;
use super::point::Point;
use super::ray::Ray;
use super::vector::Vector;


// -----------------------------------------------------------------------------
//                                    A A B B
// -----------------------------------------------------------------------------

// ----------------------------A A B B   S T R U C T----------------------------
/// Axis Aligned Bounding Box represented by a minimum and maximum [Point]
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct AABB {
    /// The minimum corner of this bounding box
    pub min: Point,
    /// The maximum corner of this bounding box
    pub max: Point
}

// --------------------A A B B   I M P L E M E N T A T I O N--------------------
impl AABB {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new bounding box with the given minimum and maximum
    /// [Point]s
    pub fn new(min: &Point, max: &Point) -> AABB {
        AABB{min: *min, max: *max}
    }

    /// Constructs a new bounding box the encloses the given single [Point]
    pub fn enclose(p: &Point) -> AABB {
        AABB{min: *p, max: *p}
    }

    /// Constructs a new bounding box that encapsulates the two given [Point]s
    pub fn encapsulate(p1: &Point, p2: &Point) -> AABB {
        AABB{
            min: Point::new(
                GFloat::min(p1.x, p2.x),
                GFloat::min(p1.y, p2.y),
                GFloat::min(p1.z, p2.z)
            ),
            max: Point::new(
                GFloat::max(p1.x, p2.x),
                GFloat::max(p1.y, p2.y),
                GFloat::max(p1.z, p2.z)
            )
        }
    }

    /// Constructs a new [AABB] that is degenerate, i.e. minimum is positive
    /// infinity while maximum is negative infinity
    pub fn degenerate() -> AABB {
        AABB{
            min: Point::broadcast(GFloat::INFINITY),
            max: Point::broadcast(GFloat::NEG_INFINITY)
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------

    /// Computes and returns a new version of this bounding box the includes
    /// the given [Point]
    pub fn extend(&self, p: &Point) -> AABB {
        AABB {
            min: Point::new(
                GFloat::min(self.min.x, p.x),
                GFloat::min(self.min.y, p.y),
                GFloat::min(self.min.z, p.z),
            ),
            max: Point::new(
                GFloat::max(self.max.x, p.x),
                GFloat::max(self.max.y, p.y),
                GFloat::max(self.max.z, p.z),
            )
        }
    }

    /// Computes and returns a new bounding box which is the union of this
    /// and the given bounding box
    pub fn union(&self, bb: &AABB) -> AABB {
        AABB {
            min: Point::new(
                GFloat::min(self.min.x, bb.min.x),
                GFloat::min(self.min.y, bb.min.y),
                GFloat::min(self.min.z, bb.min.z),
            ),
            max: Point::new(
                GFloat::max(self.max.x, bb.max.x),
                GFloat::max(self.max.y, bb.max.y),
                GFloat::max(self.max.z, bb.max.z),
            )
        }
    }

    /// Returns whether the given bounding box overlaps this bounding box
    pub fn overlaps(&self, bb: &AABB) -> bool {
        let x = (self.max.x >= bb.min.x) && (self.min.x <= bb.max.x);
        let y = (self.max.y >= bb.min.y) && (self.min.y <= bb.max.y);
        let z = (self.max.z >= bb.min.z) && (self.min.z <= bb.max.z);

        x && y && z
    }

    /// Returns whether the given [Point] is contained within this bounding box
    pub fn contains(&self, p: &Point) -> bool {
        (p.x >= self.min.x) && (p.x <= self.max.x) &&
        (p.y >= self.min.y) && (p.y <= self.max.y) &&
        (p.z >= self.min.z) && (p.z <= self.max.z)
    }

    /// Expands this bounding box by the given amount in every direction
    pub fn expand(&mut self, delta: GFloat) {
        let dv = Vector::broadcast(delta);
        self.min -= &dv;
        self.max += &dv;
    }

    /// Returns the surface area of the 6 faces of this bounding box
    pub fn surface_area(&self) -> GFloat {
        let d = &self.max - &self.min;
        2.0 * ((d.x * d.y) + (d.x * d.z) + (d.y * d.z))
    }

    /// Returns the volume of this bounding box
    pub fn volume(&self) -> GFloat {
        let d = &self.max - &self.min;
        d.x * d.y * d.z
    }

    /// Returns which of the axis is the longest
    pub fn maximum_extent(&self) -> Axis {
        let d = &self.max - &self.min;
        if d.x > d.y && d.x > d.z {
            return Axis::X;
        }
        if d.y > d.z {
            return Axis::Y;
        }
        Axis::Z
    }

    /// Linearly interpolates between the corners of the box by the given amount
    pub fn lerp(&self, tx: TFloat, ty: TFloat, tz: TFloat) -> Point {
        Point::new(
            GFloat::lerp(tx, self.min.x, self.max.x),
            GFloat::lerp(ty, self.min.y, self.max.y),
            GFloat::lerp(tz, self.min.z, self.max.z),
        )
    }

    /// Returns the position of the [Point] relative to the corners of the box,
    /// where a [Point] at the minimum corner has offset has offset (0, 0, 0)
    /// and a [Point] at the maximum corner has offset (1, 1, 1), etc
    pub fn offset(&self, p: &Point) -> Vector {
        Vector::new(
            (p.x - self.min.x) / (self.max.x - self.min.x),
            (p.y - self.min.y) / (self.max.y - self.min.y),
            (p.z - self.min.z) / (self.max.z - self.min.z)
        )
    }

    /// Returns the centre [Point] and a radius of a sphere that bounds this
    /// bounding box
    pub fn bounding_sphere(&self) -> (Point, GFloat) {
        let c = &(&self.min * 0.5) + &(&self.max * 0.5);
        let r = if self.contains(&c) {c.distance(&self.max)} else {0.0};
        (c, r)
    }

    /// TODO: document - if min_t is inside the box it is returned as t0
    pub fn intersect_p(&self, ray: &Ray) -> Option<(GFloat, GFloat)> {
        let mut t0 = ray.min_t;
        let mut t1 = ray.max_t;
        for axis in 0..3 {
            // update interval for ith bounding box slab
            let inv_ray_dir = 1.0 / ray.d[axis];
            let mut t_near = (self.min[axis] - ray.o[axis]) * inv_ray_dir;
            let mut t_far  = (self.max[axis] - ray.o[axis]) * inv_ray_dir;
            // update parameteric interval from slab intersection ts
            if t_near > t_far {
                mem::swap(&mut t_near, &mut t_far);
                t0 = t_near.max(t0);
                t1 = t_far.min(t1);
                if t0 > t1 {
                    return None;
                }
            }
        }
        // return
        Some((t0, t1))
    }
}


// -----------------------------------------------------------------------------
//                                     TESTS
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn enclose() {
        let p = Point::new(1.0, 2.0, 3.0);
        let bb = AABB::enclose(&p);
        assert_eq!(bb.min, p);
        assert_eq!(bb.max, p);
    }

    #[test]
    fn encapsulate() {
        let p1 = Point::new(-1.0, -2.0, -3.0);
        let p2 = Point::new(1.0, 2.0, 3.0);

        let bb1 = AABB::encapsulate(&p1, &p2);
        assert_eq!(bb1.min, p1);
        assert_eq!(bb1.max, p2);

        let bb2 = AABB::encapsulate(&p2, &p1);
        assert_eq!(bb2.min, p1);
        assert_eq!(bb2.max, p2);
    }

    #[test]
    fn extend() {
        let p1 = Point::new(1.0, 2.0, 3.0);
        let p2 = Point::new(-1.0, -2.0, -3.0);
        let p3 = Point::new(3.0, 0.0, 0.0);
        let p4 = Point::new(0.5, 4.0, 1.0);
        let p5 = Point::new(0.5, 1.0, -19.0);

        let bb1 = AABB::degenerate().extend(&p1);
        assert_eq!(bb1.min, p1);
        assert_eq!(bb1.max, p1);

        let bb2 = AABB::degenerate().extend(&p2);
        assert_eq!(bb2.min, p2);
        assert_eq!(bb2.max, p2);

        let bb3 = AABB::new(&p2, &p1).extend(&p3);
        assert_eq!(bb3.min, Point::new(-1.0, -2.0, -3.0));
        assert_eq!(bb3.max, Point::new(3.0, 2.0, 3.0));

        let bb4 = AABB::new(&p2, &p1).extend(&p4);
        assert_eq!(bb4.min, Point::new(-1.0, -2.0, -3.0));
        assert_eq!(bb4.max, Point::new(1.0, 4.0, 3.0));

        let bb5 = AABB::new(&p2, &p1).extend(&p5);
        assert_eq!(bb5.min, Point::new(-1.0, -2.0, -19.0));
        assert_eq!(bb5.max, Point::new(1.0, 2.0, 3.0));

        let bb6 = AABB::new(&p2, &p1).extend(&p3).extend(&p4).extend(&p5);
        assert_eq!(bb6.min, Point::new(-1.0, -2.0, -19.0));
        assert_eq!(bb6.max, Point::new(3.0, 4.0, 3.0));
    }

    #[test]
    fn union() {
        let bb1 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );
        let bb2 = AABB::new(
            &Point::new(0.0, -2.5, -19.0),
            &Point::new(3.0,  0.0,  0.0)
        );

        let bb3 = bb1.union(&bb2);
        assert_eq!(bb3.min, Point::new(-1.0, -2.5, -19.0));
        assert_eq!(bb3.max, Point::new(3.0, 2.0, 3.0));
    }

    #[test]
    fn overlaps() {
        let bb1 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );
        let bb2 = AABB::new(
            &Point::new(1.5, 2.5, 5.0),
            &Point::new(4.0, 5.0, 6.0)
        );
        let bb3 = AABB::new(
            &Point::new(-1.5, 2.5, 5.0),
            &Point::new(4.0, 5.0, 6.0)
        );
        let bb4 = AABB::new(
            &Point::new(1.5, -2.5, 5.0),
            &Point::new(4.0, 0.0, 6.0)
        );
        let bb5 = AABB::new(
            &Point::new(1.5, 2.5, -5.0),
            &Point::new(4.0, 5.0, -1.0)
        );
        let bb6 = AABB::new(
            &Point::new(-1.5, -2.5, -5.0),
            &Point::new(4.0, 5.0, -1.0)
        );

        assert!(!bb1.overlaps(&bb2));
        assert!(!bb1.overlaps(&bb3));
        assert!(!bb1.overlaps(&bb4));
        assert!(!bb1.overlaps(&bb5));
        assert!(bb1.overlaps(&bb6));
    }

    #[test]
    fn contains() {
        let bb1 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );
        let bb2 = AABB::new(
            &Point::new(-2.0, -3.0, -4.0),
            &Point::new(-1.0, -2.0, -3.0)
        );

        assert!(bb1.contains(&Point::origin()));
        assert!(!bb2.contains(&Point::origin()));
        assert!(!AABB::degenerate().contains(&Point::origin()));
        assert!(bb1.contains(&Point::new(0.5, -1.5, 2.5)));
        assert!(!bb2.contains(&Point::new(0.5, -1.5, 2.5)));
    }

    #[test]
    fn expand() {
        let mut bb1 = AABB::enclose(&Point::origin());
        let mut bb2 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );

        bb1.expand(0.5);
        assert_eq!(bb1.min, Point::broadcast(-0.5));
        assert_eq!(bb1.max, Point::broadcast(0.5));

        bb1.expand(0.5);
        assert_eq!(bb1.min, Point::broadcast(-1.0));
        assert_eq!(bb1.max, Point::broadcast(1.0));

        bb2.expand(2.0);
        assert_eq!(bb2.min, Point::new(-3.0, -4.0, -5.0));
        assert_eq!(bb2.max, Point::new(3.0, 4.0, 5.0));

    }

    // TODO: test surface area

    // TODO: test volume

    #[test]
    fn maximum_extent() {
        let bb1 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );
        let bb2 = AABB::new(
            &Point::new(-1.0, -2.0, -3.0),
            &Point::new( 10.0,  2.0,  3.0)
        );
        let bb3 = AABB::new(
            &Point::new(-1.0, -20.0, -3.0),
            &Point::new( 1.0,  2.0,  3.0)
        );

        assert_eq!(bb1.maximum_extent(), Axis::Z);
        assert_eq!(bb2.maximum_extent(), Axis::X);
        assert_eq!(bb3.maximum_extent(), Axis::Y);
    }

    // TODO: test lerp

    // TODO: test offset

    // TODO: test bounding sphere
}
