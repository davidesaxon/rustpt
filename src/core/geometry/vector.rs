use std::ops;

use crate::core::math::Dot;
use crate::core::math::Matrix44;
use crate::core::types::{GFloat, G_PI};
use super::normal::Normal;
use super::point::Point;
use super::traits::FaceForward;


// -----------------------------------------------------------------------------
//                                  V E C T O R
// -----------------------------------------------------------------------------

// -------------------------V E C T O R   S T R U C T---------------------------
/// A direction in 3D space
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Vector {
    pub x: GFloat,
    pub y: GFloat,
    pub z: GFloat,
}

// -----------------V E C T O R   I M P L E M E N T A T I O N-------------------
impl Vector {
    // -----------------------------CONSTRUCTORS--------------------------------
    /// Constructs a new [Vector] with the given 3 components
    ///
    /// **Note:** During debug builds this function will panic if any of the
    ///           components are NaNs.
    pub fn new(x: GFloat, y: GFloat, z: GFloat) -> Vector {
        let v = Vector{x, y, z};
        debug_assert!(
            !v.has_nans(),
            "Attempted to contruct Vector with NaN components: {:?}",
            v
        );
        v
    }

    /// Constructs a new [Vector] with all 3 components set to the given value
    pub fn broadcast(s: GFloat) -> Vector {
        Vector{x: s, y: s, z: s}
    }

    /// Constructs a new [Vector] from the values of the given [Point]
    pub fn from_point(p: &Point) -> Vector {
        Vector{x: p.x, y: p.y, z: p.z}
    }

    /// Constructs a new [Vector] from the values of the given [Normal]
    pub fn from_normal(n: &Normal) -> Vector {
        Vector{x: n.x, y: n.y, z: n.z}
    }

    /// Constructs a new [Vector] from the given spherical direction where
    /// sin theta and cos theta are already computed
    pub fn from_spherical_direction(
        sin_theta: GFloat,
        cos_theta: GFloat,
        phi: GFloat
    ) -> Vector {
        Vector{
            x: sin_theta * phi.cos(),
            y: sin_theta * phi.sin(),
            z: cos_theta
        }
    }

    /// Constructs a new [Vector] from the given spherical direction where
    /// sin theta and cos theta are already computed, and x, y, and z are used
    /// as used the basis vectors representing the axis
    pub fn from_spherical_direction_with_basis(
        sin_theta: GFloat,
        cos_theta: GFloat,
        phi: GFloat,
        x: &Vector,
        y: &Vector,
        z: &Vector
    ) -> Vector {
        &(&(x * (sin_theta * phi.cos())) +
          &(y * (sin_theta * phi.sin()))) +
        &(z * cos_theta)
    }

    /// Constructs a [Vector] with the xyz components initialised to 0
    pub fn zero() -> Vector {
        Vector{x: 0.0, y: 0.0, z: 0.0}
    }

    /// Constructs a [Vector] with the xyz components initialised to 1
    pub fn one() -> Vector {
        Vector{x: 1.0, y: 1.0, z: 0.0}
    }

    /// Constructs a [Vector] with the components initialised to the x unit
    /// [Vector] (1, 0, 0)
    pub fn x_unit() -> Vector {
        Vector{x: 1.0, y: 0.0, z: 0.0}
    }

    /// Constructs a [Vector] with the components initialised to the y unit
    /// [Vector] (0, 1, 0)
    pub fn y_unit() -> Vector {
        Vector{x: 0.0, y: 1.0, z: 0.0}
    }

    /// Constructs a [Vector] with the components initialised to the z unit
    /// [Vector] (0, 0, 1)
    pub fn z_unit() -> Vector {
        Vector{x: 0.0, y: 0.0, z: 1.0}
    }

    // ---------------------------PUBLIC FUNCTIONS------------------------------
    /// Returns whether any of this [Vector]'s 3 components are NaNs
    pub fn has_nans(&self) -> bool {
        self.x.is_nan() || self.y.is_nan() || self.z.is_nan()
    }

    /// Returns the length (magnitude) of this [Vector] squared
    pub fn length_squared(&self) -> GFloat {
        (self.x * self.x) + (self.y * self.y) + (self.z * self.z)
    }

    /// Returns the length (magnitude) of this [Vector]
    pub fn length(&self) -> GFloat {
        self.length_squared().sqrt()
    }

    /// Returns a new normalized version of this [Vector]
    pub fn normalize(&self) -> Vector {
        self / self.length()
    }

    /// Returns the left-handed cross product between this [Vector] and the
    /// other given [Vector]
    pub fn cross(&self, v: &Vector) -> Vector {
        Vector{
            x: (self.y * v.z) - (self.z * v.y),
            y: (self.z * v.x) - (self.x * v.z),
            z: (self.x * v.y) - (self.y * v.x)
        }
    }

    /// Constructs a local co-ordinate system for this [Vector] by returning the
    /// other 2 axis [Vector]s
    ///
    /// **Note:** This assumes this [Vector] is normalized.
    pub fn coord_system(&self) -> (Vector, Vector) {
        let mut v2 = Vector::zero();
        if self.x.abs() > self.y.abs()  {
            let inv_len = 1.0 / ((self.x * self.x) + (self.z * self.z)).sqrt();
            v2.x = -self.z * inv_len;
            v2.y = 0.0;
            v2.z = self.x * inv_len;
        }
        else {
            let inv_len = 1.0 / ((self.y * self.y) + (self.z * self.z)).sqrt();
            v2.x = 0.0;
            v2.y = self.z * inv_len;
            v2.z = -self.y * inv_len;
        }
        let v3 = self.cross(&v2);
        (v2, v3)
    }

    /// Returns a [Matrix44] that represents this [Vector] as a translation
    pub fn to_matrix44(&self) -> Matrix44 {
        Matrix44::new([
            [1.0, 0.0, 0.0, self.x],
            [0.0, 1.0, 0.0, self.y],
            [0.0, 0.0, 1.0, self.z],
            [0.0, 0.0, 0.0, 1.0]
        ])
    }

    /// Returns spherical theta angle of this [Vector]
    ///
    /// **Note:** This assumes this [Vector] is normalized.
    pub fn spherical_theta(&self) -> GFloat {
        self.z.clamp(-1.0, 1.0).acos()
    }

    /// Returns spherical phi angle of this [Vector]
    pub fn spherical_phi(&self) -> GFloat {
        let p = self.y.atan2(self.x);
        if p < 0.0 {
            return p + 2.0 * G_PI;
        }
        p
    }
}

// ---------------------------V E C T O R   I N D E X---------------------------
impl ops::Index<usize> for Vector {
    type Output = GFloat;

    fn index<'a>(&'a self, i: usize) -> &'a Self::Output {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => panic!("Vector[] index {} out of range", i),
        }
    }
}

// -----------------------V E C T O R   I N D E X   M U T-----------------------
impl ops::IndexMut<usize> for Vector {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => panic!("Vector[] index {} out of range", i),
        }
    }
}

// ----------------------------V E C T O R   N E G------------------------------
impl ops::Neg for &Vector {
    type Output = Vector;

    fn neg(self) -> Vector {
        Vector{x: -self.x, y: -self.y, z: -self.z}
    }
}

// ---------------------V E C T O R   A D D   V E C T O R-----------------------
impl ops::Add<&Vector> for &Vector{
    type Output = Vector;

    fn add(self, v: &Vector) -> Vector {
        Vector{x: self.x + v.x, y: self.y + v.y, z: self.z + v.z}
    }
}

// --------------V E C T O R   A D D   A S S I G N   V E C T O R----------------
impl ops::AddAssign<&Vector> for Vector {
    fn add_assign(&mut self, v: &Vector) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
    }
}

// ---------------------V E C T O R   S U B   V E C T O R-----------------------
impl ops::Sub<&Vector> for &Vector{
    type Output = Vector;

    fn sub(self, v: &Vector) -> Vector {
        Vector{x: self.x - v.x, y: self.y - v.y, z: self.z - v.z}
    }
}

// --------------V E C T O R   S U B   A S S I G N   V E C T O R----------------
impl ops::SubAssign<&Vector> for Vector {
    fn sub_assign(&mut self, v: &Vector) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
    }
}

// ---------------------V E C T O R   M U L   S C A L A R-----------------------
impl ops::Mul<GFloat> for &Vector {
    type Output = Vector;

    fn mul(self, s: GFloat) -> Vector {
        Vector{x: self.x * s, y: self.y * s, z: self.z * s}
    }
}

// --------------V E C T O R   M U L   A S S I G N   S C A L A R----------------
impl ops::MulAssign<GFloat> for Vector {
    fn mul_assign(&mut self, s: GFloat) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
    }
}

// ---------------------V E C T O R   D I V   S C A L A R-----------------------
impl ops::Div<GFloat> for &Vector {
    type Output = Vector;

    fn div(self, s: GFloat) -> Vector {
        debug_assert!(s != 0.0, "Attempted to divide Vector by 0 scalar");
        let inv = 1.0 / s;
        Vector{x: self.x * inv, y: self.y * inv, z: self.z * inv}
    }
}

// --------------V E C T O R   D I V   A S S I G N   S C A L A R----------------
impl ops::DivAssign<GFloat> for Vector {
    fn div_assign(&mut self, s: GFloat) {
        debug_assert!(s != 0.0, "Attempted to divide Vector by 0 scalar");
        let inv = 1.0 / s;
        self.x *= inv;
        self.y *= inv;
        self.z *= inv;
    }
}

// ----------------------V E C T O R   D O T   V E C T O R----------------------
impl Dot<Vector> for Vector {
    fn dot(&self, v: &Vector) -> GFloat {
        (self.x * v.x) + (self.y * v.y) + (self.z * v.z)
    }
}

// ----------------------V E C T O R   D O T   N O R M A L----------------------
impl Dot<Normal> for Vector {
    fn dot(&self, n: &Normal) -> GFloat {
        (self.x * n.x) + (self.y * n.y) + (self.z * n.z)
    }
}

// -------------V E C T O R   F A C E   F O R W A R D   V E C T O R-------------
impl FaceForward<Vector> for Vector {
    fn face_forward(&self, v: &Vector) -> Self {
        if self.dot(v) < 0.0 {
            return -self;
        }
        Vector::clone(self)
    }
}

// -------------V E C T O R   F A C E   F O R W A R D   N O R M A L-------------
impl FaceForward<Normal> for Vector {
    fn face_forward(&self, n: &Normal) -> Self {
        if self.dot(n) < 0.0 {
            return -self;
        }
        Vector::clone(self)
    }
}

// -----------------------------------------------------------------------------
//                                     TESTS
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn equals() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v3 = Vector{x: 4.0, y: 2.0, z: 3.0};
        let v4 = Vector{x: 1.0, y: 5.0, z: 3.0};
        let v5 = Vector{x: 1.0, y: 2.0, z: 6.0};
        let v6 = Vector{x: 0.0, y: 0.0, z: 0.0};
        assert!(v1 == v1);
        assert!(v1 == v2);
        assert!(v2 == v1);
        assert!(v1 != v3);
        assert!(v1 != v4);
        assert!(v1 != v5);
        assert!(v1 != v6);
    }

    #[test]
    fn neg() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 0.0, y: 0.0, z: 0.0};
        let v3 = Vector{x: -1.0, y: -2.0, z: -3.0};
        let v4 = Vector{x: 1.0, y: -2.0, z: 0.0};

        assert_eq!(-&v1, Vector{x: -1.0, y: -2.0, z: -3.0});
        assert_eq!(-&v2, v2);
        assert_eq!(-&v3, Vector{x: 1.0, y: 2.0, z: 3.0});
        assert_eq!(-&v4, Vector{x: -1.0, y: 2.0, z: 0.0});
    }

    #[test]
    fn add() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&v1 + &v2, Vector{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn add_assign() {
        let mut v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: -5.0, z: 6.0};

        v1 += &v2;
        assert_eq!(v1, Vector{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn sub() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&v1 - &v2, Vector{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn sub_assign() {
        let mut v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: -5.0, z: 6.0};
        v1 -= &v2;
        assert_eq!(v1, Vector{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn mul_scalar() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        assert_eq!(&v1 * 4.0, Vector{x: 4.0, y: 8.0, z: 12.0});
    }

    #[test]
    fn mul_assign_scalar() {
        let mut v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        v1 *= -4.0;
        assert_eq!(v1, Vector{x: -4.0, y: -8.0, z: -12.0});
    }

    #[test]
    fn div_scalar() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 4.0};
        assert_eq!(&v1 / 4.0, Vector{x: 0.25, y: 0.5, z: 1.0});
    }

    #[test]
    fn div_assign_scalar() {
        let mut v1 = Vector{x: 1.0, y: 2.0, z: 4.0};
        v1 /= -4.0;
        assert_eq!(v1, Vector{x: -0.25, y: -0.5, z: -1.0});
    }

    #[test]
    fn has_nans() {
        let nan: GFloat = (-1.0 as GFloat).sqrt();
        assert!(!Vector{x: 1.0, y: 2.0, z: 3.0}.has_nans());
        assert!(Vector{x: nan, y: 2.0, z: 3.0}.has_nans());
        assert!(Vector{x: 1.0, y: nan, z: 3.0}.has_nans());
        assert!(Vector{x: 1.0, y: 2.0, z: nan}.has_nans());
        assert!(Vector{x: nan, y: nan, z: nan}.has_nans());
    }

    #[test]
    fn length_squared() {
        let v1 = Vector{x: 2.0, y: 2.0, z: 1.0};
        let v2 = Vector{x: 3.0, y: 2.0, z: -1.0};
        assert_eq!(v1.length_squared(), 9.0);
        assert_eq!(v2.length_squared(), 14.0);
        assert_eq!(Vector::zero().length_squared(), 0.0);
    }

    #[test]
    fn length() {
        let v1 = Vector{x: 2.0, y: 2.0, z: 1.0};
        assert_eq!(v1.length(), 3.0);
        assert_eq!(Vector::zero().length(), 0.0);
    }

    #[test]
    fn normalize() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0}.normalize();
        assert_relative_eq!(v1.x, 0.2672612419124244);
        assert_relative_eq!(v1.y, 0.5345224838248488);
        assert_relative_eq!(v1.z, 0.8017837257372732);
        let v2 = Vector{x: -4.0, y: 5.0, z: -6.0}.normalize();
        assert_relative_eq!(v2.x, -0.4558423058385518);
        assert_relative_eq!(v2.y,  0.5698028822981898);
        assert_relative_eq!(v2.z, -0.6837634587578276);
        assert_eq!(Vector::x_unit().normalize(), Vector::x_unit());
        assert_eq!(Vector::y_unit().normalize(), Vector::y_unit());
        assert_eq!(Vector::z_unit().normalize(), Vector::z_unit());
    }

    #[test]
    fn dot() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(v1.dot(&v2), 32.0);
        assert_eq!((-&v1).dot(&v2), -32.0);
    }

    #[test]
    fn dot_normal() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let n1 = Normal{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(v1.dot(&n1), 32.0);
        assert_eq!((-&v1).dot(&n1), -32.0);
    }

    #[test]
    fn abs_dot() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(v1.abs_dot(&v2), 32.0);
        assert_eq!((-&v1).abs_dot(&v2), 32.0);
    }

    #[test]
    fn abs_dot_normal() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let n1 = Normal{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(v1.abs_dot(&n1), 32.0);
        assert_eq!((-&v1).abs_dot(&n1), 32.0);
    }

    #[test]
    fn cross() {
        let v1 = Vector{x: 1.0, y: 2.0, z: 3.0};
        let v2 = Vector{x: 4.0, y: 5.0, z: 6.0};
        assert_eq!(v1.cross(&v2), Vector{x: -3.0, y: 6.0, z: -3.0});
        assert_eq!(v1.cross(&v1), Vector::zero());
    }

    #[test]
    fn face_forward() {
        let v1 = Vector{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(v1.face_forward(&v1), v1);
        assert_eq!(v1.face_forward(&-&v1), Vector{x: -1.0, y: 0.0, z: 0.0});
    }

    #[test]
    fn face_forward_normal() {
        let v1 = Vector{x: 1.0, y: 0.0, z: 0.0};
        let n1 = Normal{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(v1.face_forward(&n1), v1);
        assert_eq!(
            v1.face_forward(&-&n1),
            Vector{x: -1.0, y: 0.0, z: 0.0}
        );
    }

    #[test]
    fn coord_system() {
        let v1 = Vector{x: 1.0, y: 0.0, z: 0.0};
        let (v2, v3) = v1.coord_system();
        assert_eq!(v2, Vector{x: 0.0, y: 0.0, z: 1.0});
        assert_eq!(v3, Vector{x: 0.0, y: -1.0, z: 0.0});
    }
}
