use std::ops;

use crate::core::types::GFloat;
use super::vector::Vector;


// -----------------------------------------------------------------------------
//                                   P O I N T
// -----------------------------------------------------------------------------

// --------------------------P O I N T   S T R U C T----------------------------
/// A point in 3D space
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Point {
    pub x: GFloat,
    pub y: GFloat,
    pub z: GFloat,
}

// ------------------P O I N T   I M P L E M E N T A T I O N--------------------
impl Point {
    // -----------------------------CONSTRUCTORS--------------------------------
    /// Constructs a new [Point] with the given 3 components
    ///
    /// **Note:** During debug builds this function will panic if any of the
    ///           Components are NaNs.
    pub fn new(x: GFloat, y: GFloat, z: GFloat) -> Point {
        let p = Point{x, y, z};
        debug_assert!(
            !p.has_nans(),
            "Attempted to contruct Point with NaN components: {:?}",
            p
        );
        p
    }

    /// Constructs a new [Point] with all 3 components set to the given value
    pub fn broadcast(s: GFloat) -> Point {
        Point{x: s, y: s, z: s}
    }

    /// Constructs a new [Point] at the origin
    pub fn origin() -> Point {
        Point{x: 0.0, y: 0.0, z: 0.0}
    }

    // ---------------------------PUBLIC FUNCTIONS------------------------------
    /// Returns whether any of this [Point]'s 3 components are NaNs
    pub fn has_nans(&self) -> bool {
        self.x.is_nan() || self.y.is_nan() || self.z.is_nan()
    }

    /// Returns the distance between this [Point] and the other given [Point]
    pub fn distance(&self, p: &Point) -> GFloat {
        (self - p).length()
    }

    /// Returns the squared distance between this [Point] and the other given
    /// [Point]
    pub fn distance_squared(&self, p: &Point) -> GFloat {
        (self - p).length_squared()
    }
}

// ----------------------------P O I N T   I N D E X----------------------------
impl ops::Index<usize> for Point {
    type Output = GFloat;

    fn index<'a>(&'a self, i: usize) -> &'a Self::Output {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => panic!("Point[] index {} out of range", i),
        }
    }
}

// ------------------------P O I N T   I N D E X   M U T------------------------
impl ops::IndexMut<usize> for Point {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => panic!("Point[] index {} out of range", i),
        }
    }
}

// -----------------------P O I N T   A D D   P O I N T-------------------------
impl ops::Add<&Point> for &Point{
    type Output = Point;

    /// Returns a new [Point] which is the result of the components of the given
    /// [Point] added to this [Point].
    ///
    /// **Note**: This function is for convenience, adding a [Point] to another
    ///           [Point] doesn't make sense from a geometric perspective.
    fn add(self, p: &Point) -> Point {
        Point{x: self.x + p.x, y: self.y + p.y, z: self.z + p.z}
    }
}

// ----------------------P O I N T   A D D   V E C T O R------------------------
impl ops::Add<&Vector> for &Point{
    type Output = Point;

    /// Returns a new [Point] which is this point offset by the given direction
    /// [Vector]
    fn add(self, v: &Vector) -> Point {
        Point{x: self.x + v.x, y: self.y + v.y, z: self.z + v.z}
    }
}

// ---------------P O I N T   A D D   A S S I G N   V E C T O R-----------------
impl ops::AddAssign<&Vector> for Point {
    /// Offsets this [Point] by the given direction [Vector]
    fn add_assign(&mut self, v: &Vector) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
    }
}

// ----------------P O I N T   A D D   A S S I G N   P O I N T------------------
impl ops::AddAssign<&Point> for Point {
    /// Adds the components of the given [Point] to this [Point]
    ///
    /// **Note**: This function is for convenience, adding a [Point] to another
    ///           [Point] doesn't make sense from a geometric perspective.
    fn add_assign(&mut self, p: &Point) {
        self.x += p.x;
        self.y += p.y;
        self.z += p.z;
    }
}

// -----------------------P O I N T   S U B   P O I N T-------------------------
impl ops::Sub<&Point> for &Point{
    type Output = Vector;

    /// Subtracts another [Point] from this [Point] to produce the direction
    /// [Vector] between the 2 [Point]s
    fn sub(self, p: &Point) -> Vector {
        Vector{x: self.x - p.x, y: self.y - p.y, z: self.z - p.z}
    }
}

// ----------------------P O I N T   S U B   V E C T O R------------------------
impl ops::Sub<&Vector> for &Point{
    type Output = Point;

    /// Returns a new [Point] which is this [Point] offset by the negative of the
    /// given direction [Vector]
    fn sub(self, v: &Vector) -> Point {
        Point{x: self.x - v.x, y: self.y - v.y, z: self.z - v.z}
    }
}

// ---------------P O I N T   S U B   A S S I G N   V E C T O R-----------------
impl ops::SubAssign<&Vector> for Point {
    /// Offsets this [Point] by the negative of the given direction [Vector]
    fn sub_assign(&mut self, v: &Vector) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
    }
}

// ----------------------P O I N T   M U L   S C A L A R------------------------
impl ops::Mul<GFloat> for &Point {
    type Output = Point;

    /// Returns a new [Point] which is the components of this [Point] weighted
    /// by the given scalar.
    ///
    /// **Note**: This function is for convenience, multiplying a [Point] by a
    ///           scalar doesn't make sense from a geometric perspective.
    fn mul(self, s: GFloat) -> Point {
        Point{x: self.x * s, y: self.y * s, z: self.z * s}
    }
}

// ----------------------P O I N T   M U L   S C A L A R------------------------
impl ops::MulAssign<GFloat> for Point {
    /// Returns a new [Point] which is the components of this [Point] weighted
    /// by the given scalar.
    ///
    /// **Note**: This function is for convenience, multiplying a [Point] by a
    ///           scalar doesn't make sense from a geometric perspective.
    fn mul_assign(&mut self, s: GFloat) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
    }
}

// -----------------------P O I N T   D I V   S C A L A R-----------------------
impl ops::Div<GFloat> for &Point {
    type Output = Point;

    fn div(self, s: GFloat) -> Point {
        debug_assert!(s != 0.0, "Attempted to divide Point by 0 scalar");
        let inv = 1.0 / s;
        Point{x: self.x * inv, y: self.y * inv, z: self.z * inv}
    }
}

// ----------------P O I N T   D I V   A S S I G N   S C A L A R----------------
impl ops::DivAssign<GFloat> for Point {
    fn div_assign(&mut self, s: GFloat) {
        debug_assert!(s != 0.0, "Attempted to divide Point by 0 scalar");
        let inv = 1.0 / s;
        self.x *= inv;
        self.y *= inv;
        self.z *= inv;
    }
}


// -----------------------------------------------------------------------------
//                                     TESTS
// -----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn equals() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p2 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p3 = Point{x: 4.0, y: 2.0, z: 3.0};
        let p4 = Point{x: 1.0, y: 5.0, z: 3.0};
        let p5 = Point{x: 1.0, y: 2.0, z: 6.0};
        let p6 = Point{x: 0.0, y: 0.0, z: 0.0};
        assert!(p1 == p1);
        assert!(p1 == p2);
        assert!(p2 == p1);
        assert!(p1 != p3);
        assert!(p1 != p4);
        assert!(p1 != p5);
        assert!(p1 != p6);
    }

    #[test]
    fn add_point() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p2 = Point{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&p1 + &p2, Point{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn add_vector() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&p1 + &v1, Point{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn add_assign_point() {
        let mut p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p2 = Point{x: 4.0, y: -5.0, z: 6.0};

        p1 += &p2;
        assert_eq!(p1, Point{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn add_assign_vector() {
        let mut p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: -5.0, z: 6.0};

        p1 += &v1;
        assert_eq!(p1, Point{x: 5.0, y: -3.0, z: 9.0});
    }

    #[test]
    fn sub_point() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p2 = Point{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&p1 - &p2, Vector{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn sub_vector() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: -5.0, z: 6.0};
        assert_eq!(&p1 - &v1, Point{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn sub_assign_vector() {
        let mut p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        let v1 = Vector{x: 4.0, y: -5.0, z: 6.0};
        p1 -= &v1;
        assert_eq!(p1, Point{x: -3.0, y: 7.0, z: -3.0});
    }

    #[test]
    fn mul_scalar() {
        let p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        assert_eq!(&p1 * 4.0, Point{x: 4.0, y: 8.0, z: 12.0});
    }

    #[test]
    fn mul_assign_scalar() {
        let mut p1 = Point{x: 1.0, y: 2.0, z: 3.0};
        p1 *= -4.0;
        assert_eq!(p1, Point{x: -4.0, y: -8.0, z: -12.0});
    }

    #[test]
    fn has_nans() {
        let nan: GFloat = (-1.0 as GFloat).sqrt();
        assert!(!Point{x: 1.0, y: 2.0, z: 3.0}.has_nans());
        assert!(Point{x: nan, y: 2.0, z: 3.0}.has_nans());
        assert!(Point{x: 1.0, y: nan, z: 3.0}.has_nans());
        assert!(Point{x: 1.0, y: 2.0, z: nan}.has_nans());
        assert!(Point{x: nan, y: nan, z: nan}.has_nans());
    }

    #[test]
    fn distance() {
        let p1 = Point{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(p1.distance(&Point::origin()), 1.0);
        let p2 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p3 = Point{x: 4.0, y: 5.0, z: 6.0};
        assert_relative_eq!(p2.distance(&p3), 5.196152422706632);
    }

    #[test]
    fn distance_squared() {
        let p1 = Point{x: 1.0, y: 0.0, z: 0.0};
        assert_eq!(p1.distance_squared(&Point::origin()), 1.0);
        let p2 = Point{x: 1.0, y: 2.0, z: 3.0};
        let p3 = Point{x: 4.0, y: 5.0, z: 6.0};
        assert_relative_eq!(p2.distance_squared(&p3), 27.0);
    }
}
