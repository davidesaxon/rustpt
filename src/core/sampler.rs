use std::sync::Arc;

use crate::core::geometry::Ray;
use crate::core::integrator::*;
use crate::core::intersection::Intersection;
use crate::core::math::Lerp;
use crate::core::rng::RNG;
use crate::core::scene::Scene;
use crate::core::spectrum::Spectrum;
use crate::core::types::*;

// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type SamplerArc = Arc<dyn Sampler + Send + Sync>;

// -----------------------------------------------------------------------------
//                                 S A M P L E R
// -----------------------------------------------------------------------------

// ------------------S A M P L E R   C O M M O N   S T R U C T------------------
/// Data that is common to all samplers
#[derive(Debug, Clone, PartialEq)]
pub struct SamplerCommon {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The minimum x and y pixels of the sampler
    pub pixels_min: (i32, i32),
    /// The maximum (non-inclusive) x and y pixels of the sampler
    pub pixels_max: (i32, i32),
    /// The number of samples the Sampler expects to generate per pixel in
    /// the image
    pub samples_per_pixel: usize,
    /// The time at which the shutter opens
    pub shutter_open: TFloat,
    /// The time at which the shutter closes
    pub shutter_close: TFloat,
}

// ----------S A M P L E R   C O M M O N   I M P L E M E N T A T I O N----------
impl SamplerCommon {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs new [SamplerCommon] data from the given parameters
    pub fn new(
        pixels_min: (i32, i32),
        pixels_max: (i32, i32),
        samples_per_pixel: usize,
        shutter_open: TFloat,
        shutter_close: TFloat
    ) -> SamplerCommon {
        SamplerCommon{
            pixels_min,
            pixels_max,
            samples_per_pixel,
            shutter_open,
            shutter_close
        }
    }
}

// --------------------------S A M P L E R   T R A I T--------------------------
pub trait Sampler {
    // ----------------------------F U N C T I O N S----------------------------
    /// Returns the common data of this [Sampler]
    fn common(&self) -> &SamplerCommon;

    /// Generates one or more samples - which are returned via the `samples`
    /// vector (this is done so the `samples` vector only needs to be allocated
    /// once).
    /// Returns the number of samples that have been generated, when 0 is
    /// returned, all samples have been generated.
    fn get_more_samples(
        &mut self,
        samples: &mut Vec<Sample>,
        rng: &mut RNG
    ) -> usize;

    /// Returns the maximum number of sample values that the [Sampler] will ever
    /// return from `get_more_samples`
    fn max_sample_count(&self) -> usize;

    /// Allows the renderer to report back to the [Sampler] which [Ray]s were
    /// generated, what radiance values were computed, and the intersection
    /// points found for a collection of samples originally from
    /// `get_more_samples`.
    /// Returns a tuple whether the first value is whether or not the sample
    /// values should be added to the image, and the second value is an
    /// estimated percentage (0.0->1.0) of how complete this [Sampler] is.
    fn report_results(
        &self,
        _samples: &Vec<Sample>,
        _rays: &Vec<Ray>,
        _ls: &Vec<Spectrum>,
        _intersections: &Vec<Option<Intersection>>,
        // TODO: could be removed if we don't prefil samples?
        _sample_count: usize
    ) -> (bool, f32) {
        (true, 0.0)
    }

    /// Returns a new [Sampler] that is responsible for generating samples for a
    /// subset of the image.
    ///
    /// * `num` The number of the sub [Sampler] to generate (between 0 and
    ///         count - 1).
    /// * `count` The total number of sub [Sampler]s that are being used.
    fn get_sub_sampler(
        &self,
        num: usize,
        count: usize
    ) -> Option<SamplerArc>;

    /// Integrators should call this method to determine how many samples they
    /// should actually request vs the integrator's requried amount of samples
    fn round_size(&self, size: usize) -> usize;

    /// Utility function that computes a pixel sampling window for a given tile
    /// number, and total number of tiles.
    ///
    /// Returns the sub window as a tuple containing the a tuple of the start x
    /// and y pixels, and a tuple of the end x and y pixels
    fn compute_sub_window(
        &self,
        num: usize,
        count: usize
    ) -> ((i32, i32), (i32, i32)) {
        let c = self.common();
        // determine how many tiles to use in each dimension
        let dx = c.pixels_max.0 - c.pixels_min.0;
        let dy = c.pixels_max.1 - c.pixels_min.1;
        let mut nx = count as i32;
        let mut ny = 1;
        while (nx & 0x1) == 0 && (2 * dx * ny) < (dy * nx) {
            nx >>= 1;
            ny <<= 1;
        }
        // compute x and y pixel sample range for sub window
        let xo = (num as i32) % nx;
        let yo = (num as i32) / nx;
        let tx0 = (xo as TFloat) / (nx as TFloat);
        let ty0 = (yo as TFloat) / (ny as TFloat);
        let tx1 = ((xo + 1) as TFloat) / (nx as TFloat);
        let ty1 = ((yo + 1) as TFloat) / (ny as TFloat);
        // return
        (
            (
                i32::lerp(tx0, c.pixels_min.0, c.pixels_max.0),
                i32::lerp(ty0, c.pixels_min.1, c.pixels_max.1)
            ),
            (
                i32::lerp(tx1, c.pixels_min.0, c.pixels_max.0),
                i32::lerp(ty1, c.pixels_min.1, c.pixels_max.1)
            )
        )
    }
}

// -----------------------------------------------------------------------------
//                           C A M E R A   S A M P L E
// -----------------------------------------------------------------------------

#[derive(Debug, Copy, Clone, PartialEq)]
/// Struct that represents just the sample values that are needed for
/// generating camera rays
pub struct CameraSample {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The position on the image of this sample
    pub image_pos: (GFloat, GFloat),
    /// The position on the lens of this sample
    pub lens_pos: (GFloat, GFloat),
    /// The time of this sample
    pub time: TFloat,
}

// -----------------------------------------------------------------------------
//                                  S A M P L E
// -----------------------------------------------------------------------------

// --------------------------S A M P L E   S T R U C T--------------------------
#[derive(Debug, Clone, PartialEq)]
/// TODO: document
pub struct Sample {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The camera sample values
    pub camera_sample: CameraSample,
    // list of the number of 1 dimensional samples
    pub n_1d: Vec<usize>,
    // list of the number of 2 dimensional samples
    pub n_2d: Vec<usize>,
    // TODO: test alignment?
    // raw sample data
    pub data: Vec<f32>,
    pub data_1d_i: Vec<usize>,
    pub data_2d_i: Vec<usize>,
}

// ------------------S A M P L E   I M P L E M E N T A T I O N------------------
impl Sample {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new sample for the given [SurfaceIntegrator] and
    /// [VolumeIntegrator].
    pub fn new(
        sampler: &SamplerArc,
        surf: Option<&SurfaceIntegratorArc>,
        vol: Option<&VolumeIntegratorArc>,
        scene: &Arc<Scene>
    ) -> Sample {
        // construct
        let mut ret = Sample{
            camera_sample: CameraSample {
                image_pos: (0.0, 0.0),
                lens_pos: (0.0, 0.0),
                time: 0.0
            },
            n_1d: Vec::new(),
            n_2d: Vec::new(),
            data: Vec::new(),
            data_1d_i: Vec::new(),
            data_2d_i: Vec::new()
        };
        // request samples?
        if let Some(s) = surf {
            s.request_samples(sampler, &mut ret, scene);
        }
        if let Some(v) = vol {
            v.request_samples(sampler, &mut ret, scene);
        }
        ret.allocate_sample_memory();
        // return
        ret
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Called by integrators to request a number of 1 dimensional samples.
    /// Returns the index that can be used by the integrator to access the
    /// generated samples.
    pub fn add_1d(&mut self, num: usize) -> usize {
        self.n_1d.push(num);
        self.n_1d.len() - 1
    }

    /// Called by integrators to request a number of 2 dimensional samples.
    /// Returns the index that can be used by the integrator to access the
    /// generated samples.
    pub fn add_2d(&mut self, num: usize) -> usize {
        self.n_2d.push(num);
        self.n_2d.len() - 1
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    fn allocate_sample_memory(&mut self) {
        // allocate storage for sample indices
        self.data_1d_i.reserve(self.n_1d.len());
        self.data_2d_i.reserve(self.n_2d.len());
        // compute total number of sample values needed and set indices
        let mut total_samples: usize = 0;
        for c in &self.n_1d {
            self.data_1d_i.push(total_samples);
            total_samples += c;
        }
        for c in &self.n_2d {
            self.data_2d_i.push(total_samples);
            total_samples += 2 * c;
        }
        // allocate storage for sample values
        self.data = vec![0.0; total_samples];
    }
}
