use std::rc::Rc;
use std::sync::Arc;

use crate::core::geometry::{DifferentialGeometry, Ray, Transform};
use crate::core::primitive::PrimitiveArc;
use crate::core::reflection::BSDF;
use crate::core::types::GFloat;

// -----------------------------------------------------------------------------
//                            I N T E R S E C T I O N
// -----------------------------------------------------------------------------

// --------------------I N T E R S E C T I O N   S T R U C T--------------------
/// Stuct that holds information about a [Ray]-[Primitive] intersection.
pub struct Intersection {
    /// The [DifferentialGeometry] where the [Ray] hit the [Primitive]'s
    /// surface.
    pub dg: Rc<DifferentialGeometry>,
    /// The [Primitive] that the [Ray] intersected with.
    pub primitive: PrimitiveArc,
    /// Transforms from world space to the [Primitive]'s object space.
    pub world_to_object: Transform,
    /// Transforms from the [Primitive]'s object space to world space.
    pub object_to_world: Transform,
    /// The unique identifier of the [Shape] that the [Ray] intersected with.
    pub shape_id: u64,
    /// The unique identifier of the [Primitive] that the [Ray] intersected
    /// with.
    pub primitive_id: u64,
    /// the maximum numeric error in the intersection caculation.
    pub ray_epsilon: GFloat,
}

// ------------I N T E R S E C T I O N   I M P L E M E N T A T I O N------------
impl Intersection {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [Intersection] from the given parameters
    pub fn new(
        dg: &Rc<DifferentialGeometry>,
        primitive: &PrimitiveArc,
        world_to_object: &Transform,
        object_to_world: &Transform,
        shape_id: u64,
        primitive_id: u64,
        ray_epsilon: GFloat
    ) -> Intersection {
        Intersection {
            dg: Rc::clone(dg),
            primitive: Arc::clone(&primitive),
            world_to_object: *world_to_object,
            object_to_world: *object_to_world,
            shape_id: shape_id,
            primitive_id: primitive_id,
            ray_epsilon: ray_epsilon
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    pub fn get_bsdf(&self, ray: &Ray) -> BSDF {
        self.dg.compute_differentials(ray);
        // TODO: defer to primitive that needs to defer to material
        panic!("Intersection::get_bsdf implementation unfinished");
    }
}
