use std::env;

use log;


// -----------------------------------------------------------------------------
//                                  C O N F I G
// -----------------------------------------------------------------------------

/// Stores the program's configuration settings
pub struct Config {
    // TODO: actually use a path here?
    pub scene_description_path: String,
    /// logging verbosity
    pub verbosity: log::LevelFilter,
    // TODO: options
}

impl Config {
    // ------------------------C O N S T R U C T O R S--------------------------

    /// Initialise program configuration from command line arguments
    pub fn new(mut args: env::Args) -> Result<Config, String> {
        // set up defaults
        let mut verbosity = log::LevelFilter::Warn;

        // skip over the program name
        args.next();

        // TODO:
        // let scene_description_path = match args.next() {
        //     Some(arg) => arg,
        //     None => return Err("No scene description file path provided"),
        // };

        // process flags
        while let Some(arg) = args.next() {
            if arg == "--verbosity" || arg == "-v" {
                if let Some(v) = args.next() {
                    let v_s = v.to_lowercase();
                    match v_s.as_str() {
                        "off" => verbosity = log::LevelFilter::Off,
                        "error" => verbosity = log::LevelFilter::Error,
                        "warn" => verbosity = log::LevelFilter::Warn,
                        "info" => verbosity = log::LevelFilter::Info,
                        "debug" => verbosity = log::LevelFilter::Debug,
                        "trace" => verbosity = log::LevelFilter::Trace,
                        _ => {
                            return Err(
                                format!("Unrecognised verbosity: {}", v_s)
                            );
                        },
                    }
                }
            }
            else {
                return Err(
                    format!("Unrecognised argument: {}", arg)
                );
            }
        }

        // TODO: process arguments
        Ok(Config{
            scene_description_path: String::from(""),
            verbosity: verbosity
        })
    }
}
