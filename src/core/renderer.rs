use std::sync::Arc;

use crate::core::geometry::Ray;
use crate::core::integrator::*;
use crate::core::intersection::Intersection;
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::scene::*;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type RendererInterfaceArc = Arc<dyn RendererInterface + Send + Sync>;

// -----------------------------------------------------------------------------
//                      R E N D E R E R   I N T E R F A C E
// -----------------------------------------------------------------------------

// ---------------R E N D E R E R   I N T E R F A C E   T R A I T---------------
/// Specifies an interface that Renderer's can share across multiple threads
pub trait RendererInterface {
    /// Returns the [SurfaceIntegrator] of this renderer
    fn surface_integrator(&self) -> &SurfaceIntegratorArc;

    /// Returns the [VolumeIntegrator] of this renderer
    fn volume_integrator(&self) -> &VolumeIntegratorArc;

    /// Calculates the incident radiance along a [Ray] and Returns a tuple
    /// contains a [Spectrum] representing the incident radiance, a Spectrum
    /// representing the volumetric transmittance along the [Ray], and a
    /// [Intersection] describing the [Ray]'s geometric intersection if it
    /// intersected.
    ///
    /// * `scene` The scene being rendered
    /// * `ray` The [Ray] to compute incident radiance for
    /// * `sample` Optionally provides sample values for Monte Carlo integration
    /// * `rng` Random number generator
    fn li(
        &self,
        scene: &Arc<Scene>,
        ray: &mut Ray,
        sample: Option<&Sample>,
        rng: &mut RNG
        // TODO: memory pool?
    ) -> (Spectrum, Spectrum, Option<Intersection>);

    /// Returns the fraction of light that is attenuated by volumetric
    /// scattering along the given [Ray]
    fn transmittance(
        &self,
        scene: &Arc<Scene>,
        ray: &mut Ray,
        sample: Option<&Sample>,
        rng: &mut RNG
        // TODO: memory pool?
    ) -> Spectrum;
}
