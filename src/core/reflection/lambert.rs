use crate::core::geometry::Vector;
use crate::core::reflection::{BxDF, BxDFType};
use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                              L A M B E R T I A N
// -----------------------------------------------------------------------------

// TODO: rename to LambertianReflection?

// ----------------------L A M B E R T I A N   S T R U C T----------------------
/// BRDF that describes a Lambertian perfectly diffuse surface that scatters
/// incident illumination equally in all directions
pub struct Lambertian {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    bxdf_type: BxDFType,
    // [Spectrum] used to scale the reflected colour
    r: Spectrum,
}

// --------------L A M B E R T I A N   I M P L E M E N T A T I O N--------------
impl Lambertian {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [Lambertian] BRDF from the given parameters
    ///
    /// * `r` [Spectrum] used to scale the reflected colour
    pub fn new(r: &Spectrum) -> Lambertian {
        Lambertian{
            bxdf_type: (BxDFType::REFLECTION | BxDFType::DIFFUSE),
            r: *r
        }
    }
}

// --------------------B X D F   I M P L E M E N T A T I O N--------------------
impl BxDF for Lambertian {
    fn get_type(&self) -> BxDFType {
        self.bxdf_type
    }

    fn f(&self, _wo: &Vector, _wi: &Vector) -> Spectrum {
        &self.r * G_INV_PI
    }

    fn rho(
        &self,
        _wo: &Vector,
        _n_samples: usize,
        _samples: &[GFloat]
    ) -> Spectrum {
        self.r
    }

    fn rhh(
        &self,
        _n_samples: usize,
        _samples1: &[GFloat],
        _samples2: &[GFloat]
    ) -> Spectrum {
        self.r
    }
}
