use crate::core::geometry::Vector;
use crate::core::reflection::*;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                            B R D F   T O   B T D F
// -----------------------------------------------------------------------------

// --------------------B R D F   T O   B T D F   S T R U C T--------------------
/// Adapter that can reuse an already defined BRDF as BTDF
pub struct BRDFToBTDF {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    bxdf_type: BxDFType,
    brdf: Box<dyn BxDF>,
}

// ------------B R D F   T O   B T D F   I M P L E M E N T A T I O N------------
impl BRDFToBTDF {
    // -------------------------------CONSTRUCTORS------------------------------
    // Constructs a new [BRDFToBTDF] from the given BRDF
    pub fn new(brdf: Box<dyn BxDF>) -> BRDFToBTDF {
        BRDFToBTDF{
            bxdf_type:
                brdf.get_type() ^ (BxDFType::REFLECTION | BxDFType::TRANSMISSION),
            brdf: brdf
        }
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    /// Converts the given [Vector] in the reflection coordinate system to a
    /// [Vector] in the opposite hemisphere.
    fn other_hemisphere(w: &Vector) -> Vector {
        Vector::new(w.x, w.y, -w.z)
    }
}

// --------------------B X D F   I M P L E M E N T A T I O N--------------------
impl BxDF for BRDFToBTDF {
    fn get_type(&self) -> BxDFType {
        self.bxdf_type
    }

    fn f(&self, wo: &Vector, wi: &Vector) -> Spectrum {
        self.brdf.f(wo, &BRDFToBTDF::other_hemisphere(wi))
    }

    fn sample_f(
        &self,
        wo: &Vector,
        u1: GFloat,
        u2: GFloat
    ) -> (Spectrum, Vector, GFloat) {
        let f = self.brdf.sample_f(wo, u1, u2);
        (f.0, BRDFToBTDF::other_hemisphere(&f.1), f.2)
    }

    fn rho(
        &self,
        wo: &Vector,
        n_samples: usize,
        samples: &[GFloat]
    ) -> Spectrum {
        self.brdf.rho(&BRDFToBTDF::other_hemisphere(wo), n_samples, samples)
    }

    fn rhh(
        &self,
        n_samples: usize,
        samples1: &[GFloat],
        samples2: &[GFloat]
    ) -> Spectrum {
        self.brdf.rhh(n_samples, samples1, samples2)
    }
}
