//! # Reflection models and utils

pub mod brdf_to_btdf;
pub mod bsdf;
pub mod bxdf;
pub mod fresnel;
pub mod lambert;
pub mod scaled_bxdf;
pub mod specular;

pub use brdf_to_btdf::BRDFToBTDF;
pub use bsdf::BSDF;
pub use bxdf::{BxDFType, BxDF};
pub use lambert::Lambertian;
pub use scaled_bxdf::ScaledBxDF;
pub use specular::{SpecularReflection, SpecularTransmission};

use crate::core::geometry::Vector;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                               F U N C T I O N S
// -----------------------------------------------------------------------------

/// Returns the cos of the spherical theta angle for the given [Vector] in the
/// reflection coordinate system
#[inline]
pub fn cos_theta(w: &Vector) -> GFloat {
    w.z
}

/// Returns the absolute cos of the sphericaltheta angle for the given [Vector]
/// in the reflection coordinate system
#[inline]
pub fn abs_cos_theta(w: &Vector) -> GFloat {
    w.z.abs()
}

/// Returns the sin squared of the spherical theta angle for the given [Vector]
/// in the reflection coordinate system
#[inline]
pub fn sin_theta2(w: &Vector) -> GFloat {
    (1.0 - cos_theta(w) * cos_theta(w)).max(0.0)
}

/// Returns the sin of the spherical theta angle for the given [Vector] in the
/// reflection coordinate system
#[inline]
pub fn sin_theta(w: &Vector) -> GFloat {
    sin_theta2(w).sqrt()
}

/// Returns the cos of the spherical phi angle for the given [Vector] in the
/// reflection coordinate system
#[inline]
pub fn cos_phi(w: &Vector) -> GFloat {
    let sin_theta = sin_theta(w);
    if sin_theta == 0.0 {
        return 1.0;
    }
    (w.x / sin_theta).clamp(-1.0, 1.0)
}

/// Returns the sin of the spherical phi angle for the given [Vector] in the
/// reflection coordinate system
#[inline]
pub fn sin_phi(w: &Vector) -> GFloat {
    let sin_theta = sin_theta(w);
    if sin_theta == 0.0 {
        return 0.0;
    }
    (w.y / sin_theta).clamp(-1.0, 1.0)
}
