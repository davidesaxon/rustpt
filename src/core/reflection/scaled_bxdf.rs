use crate::core::geometry::Vector;
use crate::core::reflection::*;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                             S C A L E D   B X D F
// -----------------------------------------------------------------------------

// ---------------------S C A L E D   B X D F   S T R U C T---------------------
/// [BxDF] that takes a [BxDF] and scales it's contribution with a [Spectrum]
/// value
pub struct ScaledBxDF {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    bxdf: Box<dyn BxDF>,
    s: Spectrum
}

// -------------S C A L E D   B X D F   I M P L E M E N T A T I O N-------------
impl ScaledBxDF {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new(bxdf: Box<dyn BxDF>, s: &Spectrum) -> ScaledBxDF {
        ScaledBxDF{bxdf: bxdf, s: *s}
    }
}

// --------------------B X D F   I M P L E M E N T A T I O N--------------------
impl BxDF for ScaledBxDF {
    fn get_type(&self) -> BxDFType {
        self.bxdf.get_type()
    }

    fn f(&self, wo: &Vector, wi: &Vector) -> Spectrum {
        &self.s * &self.bxdf.f(wo, wi)
    }

    fn sample_f(
        &self,
        wo: &Vector,
        u1: GFloat,
        u2: GFloat
    ) -> (Spectrum, Vector, GFloat) {
        let f = self.bxdf.sample_f(wo, u1, u2);
        (&self.s * &f.0, f.1, f.2)
    }

    fn rho(
        &self,
        wo: &Vector,
        n_samples: usize,
        samples: &[GFloat]
    ) -> Spectrum {
        &self.s * &self.bxdf.rho(wo, n_samples, samples)
    }

    fn rhh(
        &self,
        n_samples: usize,
        samples1: &[GFloat],
        samples2: &[GFloat]
    ) -> Spectrum {
        &self.s * &self.bxdf.rhh(n_samples, samples1, samples2)
    }
}
