use crate::core::geometry::Vector;
use crate::core::reflection::{self, BxDF, BxDFType, fresnel::*};
use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                     S P E C U L A R   R E F L E C T I O N
// -----------------------------------------------------------------------------

// -------------S P E C U L A R   R E F L E C T I O N   S T R U C T-------------
/// BRDF that describes physically plausible specular reflection
pub struct SpecularReflection {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    bxdf_type: BxDFType,
    // [Spectrum] used to scale the reflected colour
    r: Spectrum,
    // Describes fresnel properties
    fresnel: Box<dyn Fresnel>,
}

// -----S P E C U L A R   R E F L E C T I O N   I M P L E M E N T A T I O N-----
impl SpecularReflection {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [SpecularReflection] from the given parameters
    ///
    /// * `r` [Spectrum] used to scale the reflected colour
    /// * `fresnel` Describes the fresnel properties of this [BRDF]
    pub fn new(r: &Spectrum, fresnel: Box<dyn Fresnel>) -> SpecularReflection {
        SpecularReflection{
            bxdf_type: (BxDFType::REFLECTION | BxDFType::SPECULAR),
            r: *r,
            fresnel: fresnel
        }
    }
}

// --------------------B X D F   I M P L E M E N T A T I O N--------------------
impl BxDF for SpecularReflection {
    fn get_type(&self) -> BxDFType {
        self.bxdf_type
    }

    fn f(&self, _wo: &Vector, _wi: &Vector) -> Spectrum {
        // no scattering
        // TODO: change this?
        // Even if the wi was the perfect mirror of wo this function still
        // returns 0.0. Perfect reflection is instead handled by sample_f
        // (delta function)
        Spectrum::new(0.0)
    }

    fn sample_f(
        &self,
        wo: &Vector,
        _u1: GFloat,
        _u2: GFloat
    ) -> (Spectrum, Vector, GFloat) {
        // compute perfect specular reflection direction
        let wi = Vector::new(-wo.x, -wo.y, wo.z);
        // return
        (
            &self.fresnel.evaluate(reflection::cos_theta(wo)) *
                &(&self.r / reflection::abs_cos_theta(&wi)),
            wi,
            1.0,
        )
    }
}


// -----------------------------------------------------------------------------
//                   S P E C U L A R   T R A N S M I S S I O N
// -----------------------------------------------------------------------------

// -----------S P E C U L A R   T R A N S M I S S I O N   S T R U C T-----------
/// BRDF that describes physically plausible specular transmission
pub struct SpecularTransmission {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    bxdf_type: BxDFType,
    // transmission scale factor
    t: Spectrum,
    // the index of refraction for the incident side of the surface
    eta_i: SFloat,
    // the index of refraction for the transmitted side of the surface
    eta_t: SFloat,
    // fresnel dielectric is always used since conductors don't transmit light
    fresnel: FresnelDielectric,
}

// ---S P E C U L A R   T R A N S M I S S I O N   I M P L E M E N T A T I O N---
impl SpecularTransmission {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [SpecularTransmission] from the given parameters.
    ///
    /// * `t` Transmission scale factor.
    /// * `eta_i` The index of refraction for the incident side of the surface
    /// * `eta_t` The index of refraction for the transmitted side of the
    ///           surface.
    pub fn new(
        t: &Spectrum,
        eta_i: SFloat,
        eta_t: SFloat
    ) -> SpecularTransmission {
        SpecularTransmission{
            bxdf_type: (BxDFType::TRANSMISSION | BxDFType::SPECULAR),
            t: *t,
            eta_i: eta_i,
            eta_t: eta_t,
            fresnel: FresnelDielectric::new(eta_i, eta_t)
        }
    }
}

// --------------------B X D F   I M P L E M E N T A T I O N--------------------
impl BxDF for SpecularTransmission {
    fn get_type(&self) -> BxDFType {
        self.bxdf_type
    }

    fn f(&self, _wo: &Vector, _wi: &Vector) -> Spectrum {
        // no scattering
        // TODO: change this?
        // Even if the wi was the perfect mirror of wo this function still
        // returns 0.0. Perfect reflection is instead handled by sample_f
        // (delta function)
        Spectrum::new(0.0)
    }

    fn sample_f(
        &self,
        wo: &Vector,
        _u1: GFloat,
        _u2: GFloat
    ) -> (Spectrum, Vector, GFloat) {
        // determine which index of refraction is incident and which is
        // transmitted
        let entering = reflection::cos_theta(wo) > 0.0;
        let (ei, et) = if entering {
            (self.eta_i, self.eta_t)
        }
        else {
            (self.eta_t, self.eta_i)
        };
        // compute transmitted ray direction
        let sin_i2 = reflection::sin_theta2(wo);
        let eta = ei / et;
        let sin_t2 = eta * eta * sin_i2;
        // handle total internal reflection for transmission (no transmission
        // possible so return black)
        if sin_t2 >= 1.0 {
            return (Spectrum::black(), *wo, 1.0);
        }
        let mut cos_t = (1.0 - sin_t2).max(0.0).sqrt();
        if entering {
            cos_t = -cos_t;
        }
        let wi = Vector::new(
            eta * -wo.x,
            eta * -wo.y,
            cos_t
        );
        let f = self.fresnel.evaluate(reflection::cos_theta(wo));
        let l =
            &(&(&Spectrum::new(1.0) - &f) * ((et * et) / (ei * ei))) *
            &(&self.t / reflection::abs_cos_theta(&wi));
        // return
        (l, wi, 1.0)
    }
}
