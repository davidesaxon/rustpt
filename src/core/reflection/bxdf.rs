use crate::core::geometry::Vector;
use crate::core::reflection;
use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                                    B X D F
// -----------------------------------------------------------------------------

// ------------------------------B I T   F L A G S------------------------------

bitflags!{
    /// Describes the type of a BxDF. All BxDFs should have exactly one of the
    /// `REFLECTION` or `TRANSMISSION` flags set and exactly one of the
    /// `DIFFUSE`, `GLOSSY`, or `SPECULAR` flags set
    pub struct BxDFType: u8 {
        const REFLECTION   = 0b00000001;
        const TRANSMISSION = 0b00000010;
        const DIFFUSE      = 0b00000100;
        const GLOSSY       = 0b00001000;
        const SPECULAR     = 0b00010000;
        const ALL_TYPES =
            Self::DIFFUSE.bits | Self::GLOSSY.bits | Self::SPECULAR.bits;
        const ALL_REFLECTION = Self::REFLECTION.bits | Self::ALL_TYPES.bits;
        const ALL_TRANSMISSION = Self::TRANSMISSION.bits | Self::ALL_TYPES.bits;
        const ALL = Self::ALL_REFLECTION.bits | Self::ALL_TRANSMISSION.bits;
    }
}

// -----------------------------B X D F   T R A I T-----------------------------
/// Common interface for [BRDF]s and [BTDF]s
pub trait BxDF {
    /// Returns the type of this [BxDF]
    fn get_type(&self) -> BxDFType;

    /// Returns the value of the distribution function for the given pair of
    /// directions.
    ///
    /// This interface assumes that light in different wave lengths is decoupled
    /// i.e. light from one wavelength will not be reflected in a different
    ///      wavelength. This means reflection models like fluorescent are not
    ///      supported
    fn f(&self, wo: &Vector, wi: &Vector) -> Spectrum;

    /// Used for describing scattering by delta functions and for randomly
    /// sampling directions from BxDFs that scatter lights along multiple
    /// directions
    ///
    /// TODO: document parameters and return value
    fn sample_f(
        &self,
        _wo: &Vector,
        _u1: GFloat,
        _u2: GFloat
    ) -> (Spectrum, Vector, GFloat) {
        panic!("BxDF::sample_f not yet implemented");
    }

    /// Computes the hemispherical-directional reflectance of a surface.
    ///
    /// This gives the total reflection in a given direction due to constant
    /// illumination over the hemisphere
    fn rho(
        &self,
        wo: &Vector,
        n_samples: usize,
        samples: &[GFloat]
    ) -> Spectrum {
        let mut r = Spectrum::new(0.0);
        for i in 0..n_samples {
            let f_r = self.sample_f(wo, samples[2 * i], samples[2 * i + 1]);
            if f_r.2 > 0.0 {
                r += &(&f_r.0 * (reflection::abs_cos_theta(&f_r.1) / f_r.2));
            }
        }
        // return
        &r / (n_samples as SFloat)
    }

    /// Computes the hemispherical-hemispherical reflectance of a surface.
    ///
    /// This gives a constant spectral value that gives the fraction of incident
    /// light reflected by a surface when the incident light is the same from
    /// all direcitons.
    fn rhh(
        &self,
        _n_samples: usize,
        _samples1: &[GFloat],
        _samples2: &[GFloat]
    ) -> Spectrum {
        panic!("BxDF::rhh implementation unfinished!");
    }
}
