use std::rc::Rc;

use crate::core::geometry::{DifferentialGeometry, Normal, Vector};
use crate::core::math::Dot;
use crate::core::monte_carlo;
use crate::core::reflection::*;
use crate::core::rng::RNG;
use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                                    B S D F
// -----------------------------------------------------------------------------

// ------------------------------C O N S T A N T S------------------------------

const MAX_BXDFS: usize = 8;

// ----------------------------B S D F   S T R U C T----------------------------
/// Represents a collection of BRDFs and BTDFs
pub struct BSDF {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The shading [DifferentialGeometry]
    pub dg: DifferentialGeometry,
    /// The index of refraction of the medium inclosed by the surface
    pub eta: GFloat,
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // the normal of the surface this BSDF is associated with
    n: Normal,
    /// the normal of the surface in geometry space // TODO:
    ng: Normal,
    // the primary tanget // TODO:
    sn: Vector,
    // the seconday tanget // TODO:
    tn: Vector,
    // the BxDF components assoicated with this BSDF
    bxdfs: Vec<Rc<dyn BxDF>>,
}

// --------------------B S D F   I M P L E M E N T A T I O N--------------------
impl BSDF {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [BSDF] from the given parameters.
    ///
    /// * `dg` The shading [DifferentialGeometry]
    /// * `ng` The true surface normal
    /// * `eta` The index of refraction of the medium inclosed by the surface
    pub fn new(dg: &DifferentialGeometry, ng: &Normal, eta: GFloat) -> BSDF {
        let sn = dg.dpdu.normalize();
        let bxdfs = Vec::with_capacity(MAX_BXDFS);
        BSDF{
            dg: dg.clone(),
            eta: eta,
            n: dg.n,
            ng: *ng,
            sn: sn,
            tn: Vector::from_normal(&dg.n).cross(&sn),
            bxdfs: bxdfs
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Adds a [BxDF] to this [BSDF]
    pub fn add(&mut self, b: &Rc<dyn BxDF>) {
        debug_assert!(self.bxdfs.len() < MAX_BXDFS);
        self.bxdfs.push(Rc::clone(b));
    }

    /// Returns the number of [BxDF] components of this [BSDF]
    pub fn num_components(&self) -> usize {
        self.bxdfs.len()
    }

    /// Returns the number of [BxDF] components of this [BSDF] that match the
    /// given [BxDFType] flags
    pub fn num_components_of_type(&self, flags: BxDFType) -> usize {
        let mut num = 0;
        for bxdf in &self.bxdfs {
            if !(bxdf.get_type() & flags).is_empty() {
                num += 1;
            }
        }
        // return
        num
    }

    /// Transforms the given [Vector] from the world coordinate system to this
    /// [BSDF]'s local coordinate system.
    ///
    /// In the [BSDF]'s local coordinate system, the surface normal is along the
    /// z axis, the primary tangent is along the x axis, and the seconday
    /// tangent is along the y axis
    pub fn world_to_local(&self, v: &Vector) -> Vector {
        Vector::new(
            v.dot(&self.sn),
            v.dot(&self.tn),
            v.dot(&Vector::from_normal(&self.n))
        )
    }

    /// Transforms the given [Vector] from the local coordinate system of this
    /// [BSDF] to the world coordinate system
    pub fn local_to_world(&self, v: &Vector) -> Vector {
        Vector::new(
            (self.sn.x * v.x) + (self.tn.x * v.y) + (self.n.x * v.z),
            (self.sn.y * v.x) + (self.tn.y * v.y) + (self.n.y * v.z),
            (self.sn.z * v.x) + (self.tn.z * v.y) + (self.n.z * v.z)
        )
    }

    /// TODO: document
    pub fn f(&self, wo_w: &Vector, wi_w: &Vector, flags: BxDFType) -> Spectrum {
        let mut flags = flags;
        // transform to local space
        let wo = self.world_to_local(wo_w);
        let wi = self.world_to_local(wi_w);
        // handle differences between surface normal and geometry normal (light
        // leaks and dark spots)
        // should we ignore BTDFs?
        if (wi_w.dot(&self.ng) * wo_w.dot(&self.ng)) > 0.0 {
            flags = flags & (!BxDFType::TRANSMISSION);
        }
        // should we ignore BRDFs?
        else {
            flags = flags & (!BxDFType::REFLECTION);
        }
        // sum the Spectrum to return
        let mut ret = Spectrum::black();
        for bxdf in &self.bxdfs {
            if !(bxdf.get_type() & flags).is_empty() {
                ret += &bxdf.f(&wo, &wi);
            }
        }
        // return
        ret
    }

    /// TODO: document
    pub fn rho(
        &self,
        wo: &Vector,
        flags: BxDFType,
        rng: &mut RNG
    ) -> Spectrum {
        self.rho_sqrt_samples(wo, 6, flags, rng)
    }

    /// TODO: document
    pub fn rho_sqrt_samples(
        &self,
        wo: &Vector,
        sqrt_samples: usize,
        flags: BxDFType,
        rng: &mut RNG
    ) -> Spectrum {
        let n_samples = sqrt_samples * sqrt_samples;
        // gen samples
        let mut s1: Vec<GFloat> = vec![0.0; n_samples * 2];
        monte_carlo::stratified_sample_2d(
            &mut s1,
            (sqrt_samples, sqrt_samples),
            true,
            rng
        );
        // sum the Spectrum to return
        let mut ret = Spectrum::black();
        for bxdf in &self.bxdfs {
            if !(bxdf.get_type() & flags).is_empty() {
                ret += &bxdf.rho(wo, n_samples, &s1);
            }
        }
        // return
        ret
    }

    /// TODO: document
    pub fn rhh(&self, flags: BxDFType, rng: &mut RNG) -> Spectrum {
        self.rhh_sqrt_samples(6, flags, rng)
    }

    /// TODO: document
    pub fn rhh_sqrt_samples(
        &self,
        sqrt_samples: usize,
        flags: BxDFType,
        rng: &mut RNG
    ) -> Spectrum {
        let n_samples = sqrt_samples * sqrt_samples;
        // gen samples
        let mut s1: Vec<GFloat> = vec![0.0; n_samples * 2];
        monte_carlo::stratified_sample_2d(
            &mut s1,
            (sqrt_samples, sqrt_samples),
            true,
            rng
        );
        let mut s2: Vec<GFloat> = vec![0.0; n_samples * 2];
        monte_carlo::stratified_sample_2d(
            &mut s2,
            (sqrt_samples, sqrt_samples),
            true,
            rng
        );
        // sum the Spectrum to return
        let mut ret = Spectrum::black();
        for bxdf in &self.bxdfs {
            if !(bxdf.get_type() & flags).is_empty() {
                ret += &bxdf.rhh(n_samples, &s1, &s2);
            }
        }
        // return
        ret
    }
}
