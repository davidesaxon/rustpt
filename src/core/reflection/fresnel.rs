use crate::core::spectrum::Spectrum;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                               F U N C T I O N S
// -----------------------------------------------------------------------------

/// Computes the fresnel reflection formula for dielectric materials and
/// circularly polarized light
pub fn fr_diel(
    cos_i: GFloat,
    cos_t: GFloat,
    eta_i: &Spectrum,
    eta_t: &Spectrum
) -> Spectrum {
    let r_parl =
        &(&(eta_t * cos_i) - &(eta_i * cos_t)) /
        &(&(eta_t * cos_i) + &(eta_i * cos_t));
    let r_perp =
        &(&(eta_i * cos_i) - &(eta_t * cos_t)) /
        &(&(eta_i * cos_i) + &(eta_t * cos_t));
    // return
    &(&(&r_parl * &r_parl) + &(&r_perp * &r_perp)) / 2.0
}

/// Computes the fresnel reflection formula for conductive materials and
/// circularly polarized light
pub fn fr_cond(cos_i: GFloat, eta: &Spectrum, k: &Spectrum) -> Spectrum {
    let tmp = &(&(eta * eta) + &(k * k)) * (cos_i * cos_i);
    let s_one = Spectrum::new(1.0);
    let r_parl2 =
        &(&(&tmp - &(eta * (cos_i * 2.0))) + &s_one) /
        &(&(&tmp + &(eta * (cos_i * 2.0))) + &s_one);
    let tmp_f = &(eta * eta) + &(k * k);
    let s_cos_i2 = Spectrum::new(cos_i * cos_i);
    let r_perp2 =
        &(&(&tmp_f - &(eta * (cos_i * 2.0))) + &s_cos_i2) /
        &(&(&tmp_f + &(eta * (cos_i * 2.0))) + &s_cos_i2);
    // return
    &(&r_parl2 + &r_perp2) / 2.0
}

// -----------------------------------------------------------------------------
//                                 F R E S N E L
// -----------------------------------------------------------------------------

// --------------------------F R E S N E L   T R A I T--------------------------
/// Object that can compute fresnel reflection coefficients
pub trait Fresnel {
    /// Returns the amount of light reflected by the surface given the cosine of
    /// the angle made by the incoming direction and the surface normal.
    fn evaluate(&self, cos_i: GFloat) -> Spectrum;
}

// -----------------------------------------------------------------------------
//                       F R E S N E L   C O N D U C T O R
// -----------------------------------------------------------------------------

// ---------------F R E S N E L   C O N D U C T O R   S T R U C T---------------
/// [Fresnel] implementation for conductors
pub struct FresnelConductor {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // Index of refraction
    eta: Spectrum,
    // Absorption coefficient
    k: Spectrum,
}

// -------F R E S N E L   C O N D U C T O R   I M P L E M E N T A T I O N-------
impl FresnelConductor {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [FresnelConductor] with the given index of refraction
    /// and absorption coefficient
    pub fn new(eta: &Spectrum, k: &Spectrum) -> FresnelConductor {
        FresnelConductor{eta: *eta, k: *k}
    }
}

// -----------------F R E S N E L   I M P L E M E N T A T I O N-----------------
impl Fresnel for FresnelConductor {
    fn evaluate(&self, cos_i: GFloat) -> Spectrum {
        // we take the absolute value of the cosine to ensure its on the same
        // side as the normal
        fr_cond(cos_i.abs(), &self.eta, &self.k)
    }
}

// -----------------------------------------------------------------------------
//                      F R E S N E L   D I E L E C T R I C
// -----------------------------------------------------------------------------

// --------------F R E S N E L   D I E L E C T R I C   S T R U C T--------------
/// [Fresnel] implementation for dielectrics
pub struct FresnelDielectric {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // Index of refraction for the incident side of the surface
    eta_i: SFloat,
    // Index of refraction for the transmitted side of the surface
    eta_t: SFloat,
}

// -------F R E S N E L   D I E L E C T R I C   I M P L E M E N A T I O N-------
impl FresnelDielectric {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [FresnelDielectric] with the given indices of
    /// refraction for the 2 sides of the surface
    pub fn new(eta_i: SFloat, eta_t: SFloat) -> FresnelDielectric {
        FresnelDielectric{eta_i, eta_t}
    }
}

// -----------------F R E S N E L   I M P L E M E N T A T I O N-----------------
impl Fresnel for FresnelDielectric {
    fn evaluate(&self, cos_i: GFloat) -> Spectrum {
        let cos_i = cos_i.clamp(-1.0, 1.0);
        // compute indices of refraction for dielectric by determing which side
        // of the medium the incident ray lies on
        let ei;
        let et;
        if cos_i < 0.0 {
            ei = self.eta_i;
            et = self.eta_t;
        }
        else {
            ei = self.eta_t;
            et = self.eta_i;
        }
        // compute sin_t using Snell's law
        let sin_t = (ei / et) * (1.0 - (cos_i * cos_i)).max(0.0).sqrt();
        // total internal reflection?
        if sin_t >= 1.0 {
            return Spectrum::new(1.0);
        }
        let cos_t = (1.0 - (sin_t * sin_t)).max(0.0).sqrt();
        // return
        fr_diel(cos_i.abs(), cos_t, &Spectrum::new(ei), &Spectrum::new(et))
    }
}

// -----------------------------------------------------------------------------
//                           F R E S N E L   N O   O P
// -----------------------------------------------------------------------------

// -------------------F R E S N E L   N O   O P   S T R U C T-------------------
pub struct FresnelNoOp {}

// -----------F R E S N E L   N O   O P   I M P L E M E N T A T I O N-----------
impl FresnelNoOp {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new() -> FresnelNoOp {
        FresnelNoOp{}
    }
}

// -----------------F R E S N E L   I M P L E M E N T A T I O N-----------------
impl Fresnel for FresnelNoOp {
    fn evaluate(&self, _cos_i: GFloat) -> Spectrum {
        Spectrum::new(1.0)
    }
}
