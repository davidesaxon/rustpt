//! # core
//!
//! The core components of the Riptide path tracer.

pub mod camera;
pub mod config;
pub mod film;
pub mod filter;
pub mod integrator;
pub mod intersection;
pub mod geometry;
pub mod material;
pub mod math;
pub mod monte_carlo;
pub mod primitive;
pub mod reflection;
pub mod renderer;
pub mod rng;
pub mod sampler;
pub mod scene;
pub mod shape;
pub mod spectrum;
pub mod types;
