use std::sync::Arc;

use crate::core::film::*;
use crate::core::geometry::{AnimatedTransform, Ray, RayDifferentials};
use crate::core::sampler::CameraSample;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type CameraArc = Arc<dyn Camera + Send + Sync>;

// -----------------------------------------------------------------------------
//                                  C A M E R A
// -----------------------------------------------------------------------------

// -------------------C A M E R A   C O M M O N   S T R U C T-------------------
/// Data that is common to all cameras
#[derive(Clone)]
pub struct CameraCommon {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// Transformation that places the camera in the scene
    pub camera_to_world: AnimatedTransform,
    /// The time at which the camera shutter opens
    pub shutter_open: TFloat,
    /// The time at which the camera shutter closes
    pub shutter_close: TFloat,
    /// TODO: document
    pub film: FilmArc,
}

// -----------C A M E R A   C O M M O N   I M P L E M E N T A T I O N-----------
impl CameraCommon {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs new [CameraCommon] data from the given parameters
    pub fn new(
        camera_to_world: &AnimatedTransform,
        shutter_open: TFloat,
        shutter_close: TFloat,
        film: &FilmArc
    ) -> CameraCommon {
        CameraCommon {
            camera_to_world: camera_to_world.clone(),
            shutter_open: shutter_open,
            shutter_close: shutter_close,
            film: Arc::clone(film),
        }
    }
}

// ---------------------------C A M E R A   T R A I T---------------------------
pub trait Camera {
    // --------------------------------FUNCTIONS--------------------------------
    /// Returns the common data of this [Camera]
    fn common(&self) -> &CameraCommon;

    /// Generates a [Ray] for the given [Sample] and returns the generated [Ray]
    /// and a weight for how much light arriving at the film plane will
    /// contribute to the final image
    fn generate_ray(&self, sample: &CameraSample) -> (Ray, GFloat);

    /// Generates a [Ray] with differentials for the given [Sample] and returns
    /// the generated [Ray] and a weight for how much light arriving at the film
    /// plane will contribute to the final image
    fn generate_ray_differential(
        &self,
        sample: &CameraSample
    ) -> (Ray, GFloat) {
        let (mut ray, wt) = self.generate_ray(sample);
        // find ray after shifting one pixel in the x direction
        let mut x_shift = *sample;
        x_shift.image_pos.0 += 1.0;
        let (ray_x, wt_x) = self.generate_ray(&x_shift);
        // find ray after shift one pixel in the y direction
        let mut y_shift = *sample;
        y_shift.image_pos.1 += 1.0;
        let (ray_y, wt_y) = self.generate_ray(&y_shift);
        // no weight?
        if wt_x == 0.0 || wt_y == 0.0 {
            return (ray, 0.0);
        }
        // set differentials
        ray.differentials = Some(RayDifferentials{
            rx_o: ray_x.o,
            rx_d: ray_x.d,
            ry_o: ray_y.o,
            ry_d: ray_y.d
        });
        (ray, wt)
    }
}
