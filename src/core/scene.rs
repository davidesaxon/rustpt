use std::sync::Arc;

use log::warn;

use crate::core::geometry::{AABB, Ray};
use crate::core::intersection::Intersection;
use crate::core::primitive::PrimitiveArc;


// -----------------------------------------------------------------------------
//                                   S C E N E
// -----------------------------------------------------------------------------

// ---------------------------S C E N E   S T R U C T---------------------------
/// Represents all of the elements (shapes, lights, volumes, etc) that make up the scene
pub struct Scene {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The root [Primitive] of the scene
    pub aggregrate: PrimitiveArc,
    // TODO: vector of lights
        // -> may need to support lights per geometric object in the future
    // TODO: aggregrate volume region
    /// The world bounds of the scene
    pub world_bound: AABB
}

// -------------------S C E N E   I M P L E M E N T A T I O N-------------------
impl Scene {
    // -------------------------------CONSTRUCTOR-------------------------------
    /// Constructs a new [Scene] from the given parameters
    pub fn new(aggregrate: &PrimitiveArc) -> Scene {
        warn!("Scene::new has an unfinished implementation");
        let world_bound = aggregrate.world_bound();
        // TODO
        // if let Some(v) = volume_region {
        //     world_bound = world_bound.union(v.world_bound());
        // }
        Scene{
            aggregrate: Arc::clone(aggregrate),
            world_bound: world_bound
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Checks whether the given [Ray] intersects with any primitives in the
    /// scene. If an intersection occurs a [Intersection] object is returned
    pub fn intersect(&self, ray: &mut Ray) -> Option<Intersection> {
        self.aggregrate.intersect(ray, &self.aggregrate)
    }

    /// Faster version of `intersect` that only checks whether an intersection
    /// occurs and does't return any data about the intersection. Used for
    /// shadow rays
    pub fn intersect_p(&self, ray: &Ray) -> bool {
        self.aggregrate.intersect_p(ray)
    }

    /// Returns the bounding box the contains all the geometry in the scene.
    pub fn world_bound(&self) -> &AABB {
        &self.world_bound
    }
}
