use std::ops;

use crate::core::math::Lerp;
use crate::core::spectrum::{self, CoefficientSpectrum, constants, SpectrumType};
use crate::core::types::{SFloat, TFloat};

// -----------------------------------------------------------------------------
//                            R G B   S P E C T R U M
// -----------------------------------------------------------------------------

// --------------------R G B   S P E C T R U M   S T R U C T--------------------
/// A Spectrum that is represented by 3 coefficients for red, green, and blue.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct RGBSpectrum {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The samples
    pub c: [SFloat; 3],
}

// ------------R G B   S P E C T R U M   I M P L E M E N T A T I O N------------
impl RGBSpectrum {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs and returns a new [RGBSpectrum] with the RGB components all
    /// initialised to the given value `v`
    pub fn new(v: SFloat) -> RGBSpectrum {
        RGBSpectrum{c: [v; 3]}
    }

    /// Constructs a new [RGBSpectrum] with all components initialised to 0.0
    pub fn black() -> RGBSpectrum {
        RGBSpectrum{c: [0.0; 3]}
    }

    /// Constructs a new [RGBSpectrum] with all components initialised to 1.0
    pub fn white() -> RGBSpectrum {
        RGBSpectrum{c: [1.0; 3]}
    }

    /// Constructs a new [RGBSpectrum] from the given arbitrary SPD
    /// (spectral power distribution) samples
    pub fn from_samples(samples: &Vec<(SFloat, SFloat)>) -> RGBSpectrum {
        // sort samples if unordered
        if !spectrum::spectrum_samples_sorted(samples) {
            return RGBSpectrum::from_samples(
                &spectrum::sort_spectrum_samples(samples)
            );
        }
        // convert to XYZ
        let mut xyz: (SFloat, SFloat, SFloat) = (0.0, 0.0, 0.0);
        let mut y_int = 0.0;
        for i in 0..constants::CIE_SAMPLES_COUNT {
            y_int += constants::CIE_Y[i];
            let val = spectrum::interpolate_spectrum_samples(
                samples,
                constants::CIE_LAMBDA[i]
            );
            xyz.0 += val * constants::CIE_X[i];
            xyz.1 += val * constants::CIE_Y[i];
            xyz.2 += val * constants::CIE_Z[i];
        }
        xyz.0 /= y_int;
        xyz.1 /= y_int;
        xyz.2 /= y_int;
        // return as RGB
        RGBSpectrum::from_xyz(&xyz)
    }

    /// Constructs a new [RGBSpectrum] from the given RGB coefficients
    pub fn from_rgb(rgb: &(SFloat, SFloat, SFloat)) -> RGBSpectrum {
        RGBSpectrum{c: [rgb.0, rgb.1, rgb.2]}
    }

    /// Constructs a new [RGBSpectrum] from the given RGB coefficients
    ///
    /// **Note**: This function only exists to match
    ///           `SampledSpectrum::from_rgb_type` so that these types can be
    ///           interchangeably type aliased
    pub fn from_rgb_type(
        rgb: &(SFloat, SFloat, SFloat),
        _spectrum_type: SpectrumType
    ) -> RGBSpectrum {
        RGBSpectrum::from_rgb(rgb)
    }

    /// Constructs a new [RGBSpectrum] from the given CIE XYZ coefficients
    pub fn from_xyz(xyz: &(SFloat, SFloat, SFloat)) -> RGBSpectrum {
        let rgb = spectrum::xyz_to_rgb(xyz);
        RGBSpectrum{c: [rgb.0, rgb.1, rgb.2]}
    }

    /// Constructs a new [RGBSpectrum] from the given CIE XYZ coefficients
    ///
    /// **Note**: This function only exists to match
    ///           `SampledSpectrum::from_xyz_type` so that these types can be
    ///           interchangeably type aliased
    pub fn from_xyz_type(
        xyz: &(SFloat, SFloat, SFloat),
        _spectrum_type: SpectrumType
    ) -> RGBSpectrum {
        RGBSpectrum::from_xyz(xyz)
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns this [RGBSpectrum] represented as CIE XYZ coefficients
    pub fn to_xyz(&self) -> (SFloat, SFloat, SFloat) {
        spectrum::rgb_to_xyz(&(self.c[0], self.c[1], self.c[2]))
    }

    /// Return this [RGBSpectrum] represented as the single CIE Y
    /// coefficient. This is useful since Y is closely related to luminance
    pub fn to_y(&self) -> SFloat {
        constants::RGB_Y_WEIGHTS[0] * self.c[0] +
        constants::RGB_Y_WEIGHTS[1] * self.c[1] +
        constants::RGB_Y_WEIGHTS[2] * self.c[2]
    }

    /// Returns the RGB coefficients of this [RGBSpectrum]
    pub fn to_rgb(&self) -> (SFloat, SFloat, SFloat) {
        (self.c[0], self.c[1], self.c[2])
    }

    /// Returns of a copy of this [RGBSpectrum].
    ///
    /// **Note**: This function only exists to match
    ///           `SampledSpectrum::to_rgbspectrum` so that these types can be
    ///           interchangeably type aliased
    pub fn to_rgbspectrum(&self) -> RGBSpectrum {
        RGBSpectrum{c: [self.c[0], self.c[1], self.c[2]]}
    }
}

// ---------------R G B   S P E C T R U M   C O E F F I C I E N T---------------
impl CoefficientSpectrum for RGBSpectrum {
    fn is_black(&self) -> bool {
        for i in 0..3 {
            if self.c[i] != 0.0 {
                return false;
            }
        }
        true
    }

    fn srqt(&self) -> Self {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i].sqrt();
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::srqt has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn pow(&self, e: SFloat) -> Self {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i].powf(e);
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::pow has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn exp(&self) -> Self {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i].exp();
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::exp has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn clamp(&self, low: SFloat, high: SFloat) -> Self {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i].clamp(low, high);
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::clamp has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn has_nans(&self) -> bool {
        for i in 0..3 {
            if self.c[i].is_nan() {
                return true;
            }
        }
        false
    }
}

// ------------------------------------N E G------------------------------------
impl ops::Neg for &RGBSpectrum {
    type Output = RGBSpectrum;

    fn neg(self) -> RGBSpectrum {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = -self.c[i];
        }
        RGBSpectrum{c}
    }
}

// -------------------A D D   S A M P L E D   S P E C T R U M-------------------
impl ops::Add<&RGBSpectrum> for &RGBSpectrum{

    type Output = RGBSpectrum;

    fn add(self, other: &RGBSpectrum) -> RGBSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::add has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] + other.c[i];
        }
        RGBSpectrum{c}
    }
}

// ------------A D D   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::AddAssign<&RGBSpectrum> for RGBSpectrum {
    fn add_assign(&mut self, other: &RGBSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::add_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..3 {
            self.c[i] += other.c[i];
        }
    }
}

// -------------------S U B   S A M P L E D   S P E C T R U M-------------------
impl ops::Sub<&RGBSpectrum> for &RGBSpectrum{
    type Output = RGBSpectrum;

    fn sub(self, other: &RGBSpectrum) -> RGBSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::sub has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] - other.c[i];
        }
        RGBSpectrum{c}
    }
}

// ------------S U B   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::SubAssign<&RGBSpectrum> for RGBSpectrum {
    fn sub_assign(&mut self, other: &RGBSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::sub_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..3 {
            self.c[i] -= other.c[i];
        }
    }
}

// -----------------------------M U L   S C A L A R-----------------------------
impl ops::Mul<SFloat> for &RGBSpectrum {
    type Output = RGBSpectrum;

    fn mul(self, s: SFloat) -> RGBSpectrum {
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] * s;
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::mul scalar has NaN components: {:?}",
            ret.c
        );
        ret
    }
}

// ----------------------M U L   A S S I G N   S C A L A R----------------------
impl ops::MulAssign<SFloat> for RGBSpectrum {
    fn mul_assign(&mut self, s: SFloat) {
        for i in 0..3 {
            self.c[i] *= s;
        }
        debug_assert!(
            !self.has_nans(),
            "Result of RGBSpectrum::mul_assign scalar has NaN components:
            {:?}",
            self.c
        );
    }
}

// -------------------M U L   S A M P L E D   S P E C T R U M-------------------
impl ops::Mul<&RGBSpectrum> for &RGBSpectrum{
    type Output = RGBSpectrum;

    fn mul(self, other: &RGBSpectrum) -> RGBSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::mul has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] * other.c[i];
        }
        RGBSpectrum{c}
    }
}

// ------------M U L   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::MulAssign<&RGBSpectrum> for RGBSpectrum {
    fn mul_assign(&mut self, other: &RGBSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::mul_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..3 {
            self.c[i] *= other.c[i];
        }
    }
}

// -----------------------------D I V   S C A L A R-----------------------------
impl ops::Div<SFloat> for &RGBSpectrum {
    type Output = RGBSpectrum;

    fn div(self, s: SFloat) -> RGBSpectrum {
        debug_assert!(
            !s.is_nan(),
            "Scalar in RGBSpectrum::div scalar is NaN: {}",
            s
        );
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] / s;
        }
        let ret = RGBSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of RGBSpectrum::div scalar has NaN components: {:?}",
            ret.c
        );
        ret
    }
}

// ----------------------D I V   A S S I G N   S C A L A R----------------------
impl ops::DivAssign<SFloat> for RGBSpectrum {
    fn div_assign(&mut self, s: SFloat) {
        debug_assert!(
            !s.is_nan(),
            "Scalar in RGBSpectrum::div_assign scalar is NaN: {}",
            s
        );
        for i in 0..3 {
            self.c[i] /= s;
        }
        debug_assert!(
            !self.has_nans(),
            "Result of RGBSpectrum::div_assign scalar has NaN components:
            {:?}",
            self.c
        );
    }
}

// -------------------D I V   S A M P L E D   S P E C T R U M-------------------
impl ops::Div<&RGBSpectrum> for &RGBSpectrum{
    type Output = RGBSpectrum;

    fn div(self, other: &RGBSpectrum) -> RGBSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::div has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; 3];
        for i in 0..3 {
            c[i] = self.c[i] / other.c[i];
        }
        RGBSpectrum{c}
    }
}

// ------------D I V   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::DivAssign<&RGBSpectrum> for RGBSpectrum {
    fn div_assign(&mut self, other: &RGBSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other RGBSpectrum in RGBSpectrum::div_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..3 {
            self.c[i] /= other.c[i];
        }
    }
}

// -----------------------------------L E R P-----------------------------------
impl Lerp for RGBSpectrum {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        &(&v1 * ((1.0 - t) as SFloat)) + &(&v2 * (t as SFloat))
    }
}
