//! # Spectrum representations
//!
//! Various ways spectra can be represented in Riptide

pub mod constants;
pub mod rgb;
pub mod sampled;

pub use rgb::RGBSpectrum;
pub use sampled::SampledSpectrum;

use crate::core::math::Lerp;
use crate::core::types::{SFloat, TFloat};


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

/// The default Spectrum type which will be used by Riptide
pub type Spectrum = RGBSpectrum;

// -----------------------------------------------------------------------------
//                                   E N U M S
// -----------------------------------------------------------------------------

/// Defines whether a spectrum is representing reflectance or illumination
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SpectrumType {
    Reflectance,
    Illuminant
}

// -----------------------------------------------------------------------------
//                    C O E F F I C I E N T   S P E C T R U M
// -----------------------------------------------------------------------------

/// Trait for spectrum types that represent a spectrum by a particular number of
/// given samples
pub trait CoefficientSpectrum {
    /// Returns whether this spectrum is represented by 0 for all samples
    fn is_black(&self) -> bool;

    /// Takes the square root of each component and returns the result as a new
    /// spectrum
    fn srqt(&self) -> Self;

    /// Raises each component to the power of e and returns the result as a new
    /// spectrum
    fn pow(&self, e: SFloat) -> Self;

    /// Computes Euler's number raised to the power of each component and
    /// returns the result as a new spectrum
    fn exp(&self) -> Self;

    /// Clamps the components of this spectrum between (inclusive) the given
    /// `low` and `high` values
    fn clamp(&self, low: SFloat, high: SFloat) -> Self;

    /// Returns whether any of the components of this spectrum are NaNs
    fn has_nans(&self) -> bool;
}

// -----------------------------------------------------------------------------
//                               F U N C T I O N S
// -----------------------------------------------------------------------------

/// Converts CIE XYZ coefficients to RGB coefficients
#[inline]
pub fn xyz_to_rgb(xyz: &(SFloat, SFloat, SFloat)) -> (SFloat, SFloat, SFloat) {
    let mut rgb = (0.0, 0.0, 0.0);
    rgb.0 =  3.240479 * xyz.0 - 1.537150 * xyz.1 - 0.498535 * xyz.2;
    rgb.1 = -0.969256 * xyz.0 + 1.875991 * xyz.1 + 0.041556 * xyz.2;
    rgb.2 =  0.055648 * xyz.0 - 0.204043 * xyz.1 + 1.057311 * xyz.2;
    // return
    rgb
}

/// Converts RGB coefficients to CIE XYZ coefficients
#[inline]
pub fn rgb_to_xyz(rgb: &(SFloat, SFloat, SFloat)) -> (SFloat, SFloat, SFloat) {
    let mut xyz = (0.0, 0.0, 0.0);
    xyz.0 = 0.412453 * rgb.0 + 0.357580 * rgb.1 + 0.180423 * rgb.2;
    xyz.1 = 0.212671 * rgb.0 + 0.715160 * rgb.1 + 0.072169 * rgb.2;
    xyz.2 = 0.019334 * rgb.0 + 0.119193 * rgb.1 + 0.950227 * rgb.2;
    // return
    xyz
}

/// Returns whether the given samples are sorted
pub fn spectrum_samples_sorted(samples: &Vec<(SFloat, SFloat)>) -> bool {
    for i in 0..(samples.len() - 1) {
        if samples[i].0 > samples[i + 1].0 {
            return false;
        }
    }
    true
}

/// Returns a new version of the given samples as sorted
pub fn sort_spectrum_samples(
    samples: &Vec<(SFloat, SFloat)>
) -> Vec<(SFloat, SFloat)> {
    let mut ret = Vec::with_capacity(samples.len());
    for s in samples {
        ret.push(*s);
    }
    ret.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());
    // return
    ret
}

/// Returns the average of the samples within the given wavelength range
pub fn average_spectrum_samples(
    samples: &Vec<(SFloat, SFloat)>,
    lambda_start: SFloat,
    lambda_end: SFloat
) -> SFloat {
    // handle out of bound range and single sample. we assume SPD has a constant
    // value outside of the sample range, if this is the case for a particular
    // set of data it should have explicit values for 0.0 (for example) at the
    // end points
    if lambda_end <= samples[0].0 || samples.len() == 1 {
        return samples[0].1;
    }
    if lambda_start >= samples.last().unwrap().0 {
        return samples.last().unwrap().1;
    }
    let mut sum = 0.0;
    // add contributions of constant segments before/after samples
    if lambda_start < samples[0].0 {
        sum += samples[0].1 * (samples[0].0 - lambda_start);
    }
    if lambda_end > samples[samples.len() - 1].0 {
        sum +=
            samples.last().unwrap().1 *
            (lambda_end - samples.last().unwrap().0);
    }
    // advance to first relevant wavelength segment
    let mut i = 0;
    while lambda_start > samples[i + 1].0 {
        i += 1;
    }
    // loop over wavelength sample segments and add contributions
    while i + 1 < samples.len() && lambda_end >= samples[i].0 {
        let seg_start = lambda_start.max(samples[i].0);
        let seg_end = lambda_end.min(samples[i + 1].0);
        let lerp1 = SFloat::lerp(
            ((seg_start - samples[i].0) /
             (samples[i + 1].0 - samples[i].0)) as TFloat,
            samples[i].1,
            samples[i + 1].1
        );
        let lerp2 = SFloat::lerp(
            ((seg_end - samples[i].0) /
             (samples[i + 1].0 - samples[i].0)) as TFloat,
            samples[i].1,
            samples[i + 1].1
        );
        sum += ((lerp1 + lerp2) * 0.5) * (seg_end - seg_start);
        i += 1;
    }
    return sum / ((lambda_end - lambda_start) as SFloat);
}

/// Takes a possibly irregularly sammpled set of SPD values and returns the
/// interpolated values of the SPD at the given wavelength `l`
pub fn interpolate_spectrum_samples(
    samples: &Vec<(SFloat, SFloat)>,
    l: SFloat
) -> SFloat {
    if l <= samples[0].0 {
        return samples[0].1;
    }
    if l >= samples.last().unwrap().0 {
        return samples.last().unwrap().1;
    }
    for i in 0..(samples.len() - 1) {
        if l >= samples[i].0 && l <= samples[i + 1].0 {
            let t = (
                (l - samples[i].0) / (samples[i + 1].0 - samples[i].0)
            ) as TFloat;
            return SFloat::lerp(t, samples[i].1, samples[i + 1].1);
        }
    }
    panic!(
        "Code in spectrum::interpolate_spectrum_samples should never be
        reached!"
    );
}
