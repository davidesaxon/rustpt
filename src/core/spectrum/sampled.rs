use std::ops;

use log::debug;

use crate::core::math::Lerp;
use crate::core::spectrum::{
    self,
    CoefficientSpectrum,
    constants,
    RGBSpectrum,
    SpectrumType
};
use crate::core::types::{SFloat, TFloat};


// -----------------------------------------------------------------------------
//                        S A M P L E D   S P E C T R U M
// -----------------------------------------------------------------------------

// ----------------S A M P L E D   S P E C T R U M   S T R U C T----------------
/// A Spectrum that is represented by `constants::SAMPLED_LAMBDA_COUNT` number
/// of samples between wavelength `constants::SAMPLED_LAMBDA_START` nm and
/// wavelength `constants::SAMPLED_LAMBDA_END` nm
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SampledSpectrum {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The samples
    pub c: [SFloat; constants::SAMPLED_LAMBDA_COUNT],
}

// --------S A M P L E D   S P E C T R U M   I M P L E M E N T A T I O N--------
impl SampledSpectrum {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs and returns a new [SampledSpectrum] with the components all
    /// initialised to the given value `v`
    pub fn new(v: SFloat) -> SampledSpectrum {
        SampledSpectrum{c: [v; constants::SAMPLED_LAMBDA_COUNT]}
    }

    /// Constructs a new [SampledSpectrum] with all components initialised to
    /// 0.0
    pub fn black() -> SampledSpectrum {
        SampledSpectrum{c: [0.0; constants::SAMPLED_LAMBDA_COUNT]}
    }

    /// Constructs a new [SampledSpectrum] with all components initialised to
    /// 1.0
    pub fn white() -> SampledSpectrum {
        SampledSpectrum{c: [1.0; constants::SAMPLED_LAMBDA_COUNT]}
    }

    /// Constructs a new [SampledSpectrum] from the given arbitrary SPD
    /// (spectral power distribution) samples
    pub fn from_samples(samples: &Vec<(SFloat, SFloat)>) -> SampledSpectrum {
        // sort samples if unordered
        if !spectrum::spectrum_samples_sorted(samples) {
            return SampledSpectrum::from_samples(
                &spectrum::sort_spectrum_samples(samples)
            );
        }
        // compute the average value of the given SPD (spectral power
        // distribution) for each sample's range
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            let lambda0 = SFloat::lerp(
                (i as TFloat) / (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            let lambda1 = SFloat::lerp(
                ((i + 1) as TFloat) /
                 (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            c[i] = spectrum::average_spectrum_samples(
                samples,
                lambda0,
                lambda1
            );
        }
        // return
        SampledSpectrum{c}
    }

    /// Same as from_rgb_type but the [SpectrumType] is assumed to be
    /// [SpectrumType::Reflectance]
    pub fn from_rgb(rgb: &(SFloat, SFloat, SFloat)) -> SampledSpectrum {
        SampledSpectrum::from_rgb_type(rgb, SpectrumType::Reflectance)
    }

    /// Converts the given RGB coefficients to a full SPD (spectral power
    /// distribution) represented by a [SampledSpectrum]
    ///
    /// * `rgb`: The RGB coefficients to convert to SPD
    /// * `spectrum_type`: The type of spectrum the RGB coefficients represent
    pub fn from_rgb_type(
        rgb: &(SFloat, SFloat, SFloat),
        spectrum_type: SpectrumType
    ) -> SampledSpectrum {
        let mut ret = SampledSpectrum::black();
        unsafe {
            match spectrum_type {
                SpectrumType::Reflectance => {
                    // compute reflectance based on the minimum RGB value
                    if rgb.0 <= rgb.1 && rgb.0 <= rgb.2 {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_REF_WHITE * rgb.0
                        );
                        if rgb.1 <= rgb.2 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_CYAN *
                                (rgb.1 - rgb.0)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_BLUE *
                                (rgb.2 - rgb.1)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_CYAN *
                                (rgb.2 - rgb.0)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_GREEN *
                                (rgb.1 - rgb.2)
                            );
                        }
                    }
                    else if rgb.1 <= rgb.0 && rgb.1 <= rgb.2 {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_REF_WHITE * rgb.1
                        );
                        if rgb.0 <= rgb.2 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_MAGENTA *
                                (rgb.0 - rgb.1)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_BLUE *
                                (rgb.2 - rgb.0)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_MAGENTA *
                                (rgb.2 - rgb.1)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_RED *
                                (rgb.0 - rgb.2)
                            );
                        }
                    }
                    else {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_REF_WHITE * rgb.2
                        );
                        if rgb.0 <= rgb.1 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_YELLOW *
                                (rgb.0 - rgb.2)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_GREEN *
                                (rgb.1 - rgb.0)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_YELLOW *
                                (rgb.1 - rgb.2)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_REF_RED *
                                (rgb.0 - rgb.1)
                            );
                        }
                    }
                    // TODO: understand this?
                    ret *= 0.94;
                },
                SpectrumType::Illuminant => {
                    // compute illumination based on the minimum RGB value
                    if rgb.0 <= rgb.1 && rgb.0 <= rgb.2 {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_ILLUM_WHITE * rgb.0
                        );
                        if rgb.1 <= rgb.2 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_CYAN *
                                (rgb.1 - rgb.0)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_BLUE *
                                (rgb.2 - rgb.1)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_CYAN *
                                (rgb.2 - rgb.0)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_GREEN *
                                (rgb.1 - rgb.2)
                            );
                        }
                    }
                    else if rgb.1 <= rgb.0 && rgb.1 <= rgb.2 {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_ILLUM_WHITE * rgb.1
                        );
                        if rgb.0 <= rgb.2 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_MAGENTA *
                                (rgb.0 - rgb.1)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_BLUE *
                                (rgb.2 - rgb.0)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_MAGENTA *
                                (rgb.2 - rgb.1)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_RED *
                                (rgb.0 - rgb.2)
                            );
                        }
                    }
                    else {
                        ret += &(
                            &constants::SAMPLED_RGB_TO_SPECT_ILLUM_WHITE * rgb.2
                        );
                        if rgb.0 <= rgb.1 {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_YELLOW *
                                (rgb.0 - rgb.2)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_GREEN *
                                (rgb.1 - rgb.0)
                            );
                        }
                        else {
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_YELLOW *
                                (rgb.1 - rgb.2)
                            );
                            ret += &(
                                &constants::SAMPLED_RGB_TO_SPECT_ILLUM_RED *
                                (rgb.0 - rgb.1)
                            );
                        }
                    }
                    // TODO: understand this?
                    ret *= 0.86445;
                }
            }
        }
        // return clamped
        ret.clamp(0.0, SFloat::INFINITY)
    }

    /// Same as from_xyz_type but the [SpectrumType] is assumed to be
    /// [SpectrumType::Reflectance]
    pub fn from_xyz(xyz: &(SFloat, SFloat, SFloat)) -> SampledSpectrum {
        SampledSpectrum::from_xyz_type(xyz, SpectrumType::Reflectance)
    }

    /// Converts the given XYZ coefficients to a full SPD (spectral power
    /// distribution) represented by a [SampledSpectrum]
    ///
    /// * `xyz`: The XYZ coefficients to convert to SPD
    /// * `spectrum_type`: The type of spectrum the XYZ coefficients represent
    pub fn from_xyz_type(
        xyz: &(SFloat, SFloat, SFloat),
        spectrum_type: SpectrumType
    ) -> SampledSpectrum {
        SampledSpectrum::from_rgb_type(
            &spectrum::xyz_to_rgb(xyz),
            spectrum_type
        )
    }

    // TODO: from_RGBSpectrum - page 279

    // -----------------------------STRUCT FUNCTIONS----------------------------
    /// initialises data relating to [SampledSpectrum]s. Called by Riptide
    /// startup
    pub fn init() {
        debug!("Initialising sampled spectrum data...");

        // compute XYZ matching functions
        let cie_x_samples =
            constants::CIE_LAMBDA.iter().cloned()
            .zip(constants::CIE_X.iter().cloned()).collect();
        let cie_y_samples =
            constants::CIE_LAMBDA.iter().cloned()
            .zip(constants::CIE_Y.iter().cloned()).collect();
        let cie_z_samples =
            constants::CIE_LAMBDA.iter().cloned()
            .zip(constants::CIE_Z.iter().cloned()).collect();
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            let lerp1 = SFloat::lerp(
                (i as TFloat) / (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            let lerp2 = SFloat::lerp(
                ((i + 1) as TFloat) / (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            unsafe {
                constants::SAMPLED_X.c[i] = spectrum::average_spectrum_samples(
                    &cie_x_samples,
                    lerp1,
                    lerp2
                );
                constants::SAMPLED_Y.c[i] = spectrum::average_spectrum_samples(
                    &cie_y_samples,
                    lerp1,
                    lerp2
                );
                constants::SAMPLED_Z.c[i] = spectrum::average_spectrum_samples(
                    &cie_z_samples,
                    lerp1,
                    lerp2
                );
                constants::SAMPLED_Y_INT += constants::SAMPLED_Y.c[i];
            }
        }
        // compute RGB to spectrum functions
        let ref_white_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_WHITE.iter().cloned()).collect();
        let ref_cyan_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_CYAN.iter().cloned()).collect();
        let ref_magenta_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_MAGENTA.iter().cloned()).collect();
        let ref_yellow_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_YELLOW.iter().cloned()).collect();
        let ref_red_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_RED.iter().cloned()).collect();
        let ref_green_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_GREEN.iter().cloned()).collect();
        let ref_blue_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_REF_BLUE.iter().cloned()).collect();
        let illum_white_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_WHITE.iter().cloned()).collect();
        let illum_cyan_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_CYAN.iter().cloned()).collect();
        let illum_magenta_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_MAGENTA.iter().cloned())
            .collect();
        let illum_yellow_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_YELLOW.iter().cloned())
            .collect();
        let illum_red_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_RED.iter().cloned()).collect();
        let illum_green_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_GREEN.iter().cloned()).collect();
        let illum_blue_samples =
            constants::RGB_TO_SPECT_LAMBDA.iter().cloned()
            .zip(constants::RGB_TO_SPECT_ILLUM_BLUE.iter().cloned()).collect();
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            let lerp1 = SFloat::lerp(
                (i as TFloat) / (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            let lerp2 = SFloat::lerp(
                ((i + 1) as TFloat) / (constants::SAMPLED_LAMBDA_COUNT as TFloat),
                constants::SAMPLED_LAMBDA_START as SFloat,
                constants::SAMPLED_LAMBDA_END as SFloat
            );
            unsafe {
                constants::SAMPLED_RGB_TO_SPECT_REF_WHITE.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_white_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_CYAN.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_cyan_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_MAGENTA.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_magenta_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_YELLOW.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_yellow_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_RED.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_red_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_GREEN.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_green_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_REF_BLUE.c[i] =
                    spectrum::average_spectrum_samples(
                        &ref_blue_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_WHITE.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_white_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_CYAN.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_cyan_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_MAGENTA.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_magenta_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_YELLOW.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_yellow_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_RED.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_red_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_GREEN.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_green_samples,
                        lerp1,
                        lerp2
                    );
                constants::SAMPLED_RGB_TO_SPECT_ILLUM_BLUE.c[i] =
                    spectrum::average_spectrum_samples(
                        &illum_blue_samples,
                        lerp1,
                        lerp2
                    );
            }
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns this [SampledSpectrum] represented as CIE XYZ coefficients
    pub fn to_xyz(&self) -> (SFloat, SFloat, SFloat) {
        let mut xyz = (0.0, 0.0, 0.0);
        unsafe {
            for i in 0..constants::SAMPLED_LAMBDA_COUNT {
                xyz.0 += constants::SAMPLED_X.c[i] * self.c[i];
                xyz.1 += constants::SAMPLED_Y.c[i] * self.c[i];
                xyz.2 += constants::SAMPLED_Z.c[i] * self.c[i];
            }
            xyz.0 /= constants::SAMPLED_Y_INT;
            xyz.1 /= constants::SAMPLED_Y_INT;
            xyz.2 /= constants::SAMPLED_Y_INT;
        }
        // return
        xyz
    }

    /// Return this [SampledSpectrum] represented as the single CIE Y
    /// coefficient. This is useful since Y is closely related to luminance
    pub fn to_y(&self) -> SFloat {
        let mut y = 0.0;
        unsafe {
            for i in 0..constants::SAMPLED_LAMBDA_COUNT {
                y += constants::SAMPLED_Y.c[i] * self.c[i];
            }
            y /= constants::SAMPLED_Y_INT;
        }
        // return
        y
    }

    /// Returns this [SampledSpectrum] represented as RGB coefficients
    pub fn to_rgb(&self) -> (SFloat, SFloat, SFloat) {
        spectrum::xyz_to_rgb(&self.to_xyz())
    }

    /// Returns this [SampledSpectrum] represented as a RGBSpectrum
    pub fn to_rgbspectrum(&self) -> RGBSpectrum {
        RGBSpectrum::from_rgb(&self.to_rgb())
    }
}

// -----------S A M P L E D   S P E C T R U M   C O E F F I C I E N T-----------
impl CoefficientSpectrum for SampledSpectrum {
    fn is_black(&self) -> bool {
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            if self.c[i] != 0.0 {
                return false;
            }
        }
        true
    }

    fn srqt(&self) -> Self {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i].sqrt();
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::srqt has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn pow(&self, e: SFloat) -> Self {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i].powf(e);
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::pow has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn exp(&self) -> Self {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i].exp();
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::exp has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn clamp(&self, low: SFloat, high: SFloat) -> Self {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i].clamp(low, high);
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::clamp has NaN components: {:?}",
            ret.c
        );
        ret
    }

    fn has_nans(&self) -> bool {
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            if self.c[i].is_nan() {
                return true;
            }
        }
        false
    }
}

// ------------------------------------N E G------------------------------------
impl ops::Neg for &SampledSpectrum {
    type Output = SampledSpectrum;

    fn neg(self) -> SampledSpectrum {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = -self.c[i];
        }
        SampledSpectrum{c}
    }
}

// -------------------A D D   S A M P L E D   S P E C T R U M-------------------
impl ops::Add<&SampledSpectrum> for &SampledSpectrum{

    type Output = SampledSpectrum;

    fn add(self, other: &SampledSpectrum) -> SampledSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::add has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] + other.c[i];
        }
        SampledSpectrum{c}
    }
}

// ------------A D D   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::AddAssign<&SampledSpectrum> for SampledSpectrum {
    fn add_assign(&mut self, other: &SampledSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::add_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] += other.c[i];
        }
    }
}

// -------------------S U B   S A M P L E D   S P E C T R U M-------------------
impl ops::Sub<&SampledSpectrum> for &SampledSpectrum{
    type Output = SampledSpectrum;

    fn sub(self, other: &SampledSpectrum) -> SampledSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::sub has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] - other.c[i];
        }
        SampledSpectrum{c}
    }
}

// ------------S U B   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::SubAssign<&SampledSpectrum> for SampledSpectrum {
    fn sub_assign(&mut self, other: &SampledSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::sub_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] -= other.c[i];
        }
    }
}

// -----------------------------M U L   S C A L A R-----------------------------
impl ops::Mul<SFloat> for &SampledSpectrum {
    type Output = SampledSpectrum;

    fn mul(self, s: SFloat) -> SampledSpectrum {
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] * s;
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::mul scalar has NaN components: {:?}",
            ret.c
        );
        ret
    }
}

// ----------------------M U L   A S S I G N   S C A L A R----------------------
impl ops::MulAssign<SFloat> for SampledSpectrum {
    fn mul_assign(&mut self, s: SFloat) {
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] *= s;
        }
        debug_assert!(
            !self.has_nans(),
            "Result of SampledSpectrum::mul_assign scalar has NaN components:
            {:?}",
            self.c
        );
    }
}

// -------------------M U L   S A M P L E D   S P E C T R U M-------------------
impl ops::Mul<&SampledSpectrum> for &SampledSpectrum{
    type Output = SampledSpectrum;

    fn mul(self, other: &SampledSpectrum) -> SampledSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::mul has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] * other.c[i];
        }
        SampledSpectrum{c}
    }
}

// ------------M U L   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::MulAssign<&SampledSpectrum> for SampledSpectrum {
    fn mul_assign(&mut self, other: &SampledSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::mul_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] *= other.c[i];
        }
    }
}

// -----------------------------D I V   S C A L A R-----------------------------
impl ops::Div<SFloat> for &SampledSpectrum {
    type Output = SampledSpectrum;

    fn div(self, s: SFloat) -> SampledSpectrum {
        debug_assert!(
            !s.is_nan(),
            "Scalar in SampledSpectrum::div scalar is NaN: {}",
            s
        );
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] / s;
        }
        let ret = SampledSpectrum{c};
        debug_assert!(
            !ret.has_nans(),
            "Result of SampledSpectrum::div scalar has NaN components: {:?}",
            ret.c
        );
        ret
    }
}

// ----------------------D I V   A S S I G N   S C A L A R----------------------
impl ops::DivAssign<SFloat> for SampledSpectrum {
    fn div_assign(&mut self, s: SFloat) {
        debug_assert!(
            !s.is_nan(),
            "Scalar in SampledSpectrum::div_assign scalar is NaN: {}",
            s
        );
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] /= s;
        }
        debug_assert!(
            !self.has_nans(),
            "Result of SampledSpectrum::div_assign scalar has NaN components:
            {:?}",
            self.c
        );
    }
}

// -------------------D I V   S A M P L E D   S P E C T R U M-------------------
impl ops::Div<&SampledSpectrum> for &SampledSpectrum{
    type Output = SampledSpectrum;

    fn div(self, other: &SampledSpectrum) -> SampledSpectrum {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::div has NaN components:
            {:?}",
            other.c
        );
        let mut c = [0.0; constants::SAMPLED_LAMBDA_COUNT];
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            c[i] = self.c[i] / other.c[i];
        }
        SampledSpectrum{c}
    }
}

// ------------D I V   A S S I G N   S A M P L E D   S P E C T R U M------------
impl ops::DivAssign<&SampledSpectrum> for SampledSpectrum {
    fn div_assign(&mut self, other: &SampledSpectrum) {
        debug_assert!(
            !other.has_nans(),
            "Other SampledSpectrum in SampledSpectrum::div_assign has NaN
            components: {:?}",
            other.c
        );
        for i in 0..constants::SAMPLED_LAMBDA_COUNT {
            self.c[i] /= other.c[i];
        }
    }
}

// -----------------------------------L E R P-----------------------------------
impl Lerp for SampledSpectrum {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        &(&v1 * ((1.0 - t) as SFloat)) + &(&v2 * (t as SFloat))
    }
}
