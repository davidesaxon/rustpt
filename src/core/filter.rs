use std::sync::Arc;

use crate::core::types::*;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type FilterArc = Arc<dyn Filter + Send + Sync>;

// -----------------------------------------------------------------------------
//                                  F I L T E R
// -----------------------------------------------------------------------------

// --------------------------F I L T E R   C O M M O N--------------------------
/// Common data for [Filter]s
pub struct FilterCommon {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The width at which beyond the filter has a value of 0
    pub width: (GFloat, GFloat),
    /// The reciprocal of `width`
    pub inv_width: (GFloat, GFloat),
}

// -----------F I L T E R   C O M M O N   I M P L E M E N T A T I O N-----------
impl FilterCommon {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs new [FilterCommon] data from the given parameters
    pub fn new(width: (GFloat, GFloat)) -> FilterCommon {
        FilterCommon{
            width: width,
            inv_width: (1.0 / width.0, 1.0 / width.1)
        }
    }
}

// ---------------------------F I L T E R   T R A I T---------------------------
/// Image reconstruction filter
pub trait Filter {
    /// Returns the [FilterCommon] data of this [Filter]
    fn common(&self) -> &FilterCommon;

    /// Returns the weight of a sample at the given position with relation to
    /// the centre of this [Filter]
    ///
    /// **Note:** It is assumed that `pos` is not outside of the extents of this
    ///           filter.
    fn evalulate(&self, pos: (GFloat, GFloat)) -> GFloat;
}
