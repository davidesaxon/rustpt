use std::sync::Arc;

use crate::core::camera::CameraArc;
use crate::core::geometry::Ray;
use crate::core::intersection::Intersection;
use crate::core::renderer::{RendererInterface, RendererInterfaceArc};
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::scene::Scene;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type SurfaceIntegratorArc = Arc<dyn SurfaceIntegrator + Send + Sync>;
pub type VolumeIntegratorArc = Arc<dyn VolumeIntegrator + Send + Sync>;

// -----------------------------------------------------------------------------
//                      S U R F A C E   I N T E G R A T O R
// -----------------------------------------------------------------------------

// ---------------S U R F A C E   I N T E G R A T O R   T R A I T---------------
// TODO: document
pub trait SurfaceIntegrator {
    /// Calculates the incident radiance along the [Ray] for the given
    /// [Intersection] data
    fn li(
        &self,
        scene: &Arc<Scene>,
        renderer: &dyn RendererInterface,
        ray: &mut Ray,
        intersection: &Intersection,
        sample: Option<&Sample>,
        rng: &mut RNG,
        // TODO: memory pool
    ) -> Spectrum;

    // TODO: place holder
    fn request_samples(
        &self,
        _sampler: &SamplerArc,
        _sample: &mut Sample,
        _scene: &Arc<Scene>
    ) {
        // TODO:
    }

    // TODO: place holder
    fn preprocess(
        &self,
        _camera: &CameraArc,
        _scene:&Arc<Scene>,
        _renderer: &RendererInterfaceArc
    ) {
        // TODO:
    }
}

// -----------------------------------------------------------------------------
//                                   FUNCTIONS
// -----------------------------------------------------------------------------

// TODO: document: computes the specular reflection for the bsdf at the intersection
// TODO:
// pub fn specular_reflect(
//     scene: &Scene,
//     renderer: Box<dyn Renderer>,
//     ray: &RayDifferential,
//     intersection: &Intersection,
//     bsdf: &BSDF,
//     sample: &Sample,
//     rng: &mut Rng,
//     arena: &mut MemoryArena
// ) -> Spectrum {
//     let wo = -ray.d;
//     p = &bsdf.dg_shading.p;
//     n = &bsdf.dg_shading.nn;
//     // ask the bsdf to compute incident ray direction for the reflection
//     let (f, wi, pdf) = bsdf.sample_f(
//         wo,
//         BSDFSample::new(rng),
//         // TODO: use the bitflags crate for this
//         bxdf::BSDF_REFLECTION | bxdf::BSDF_SPECULAR
//     );
//     // TODO: should investigate what it means if f is black and if the consine term is 0.0 and
//     //       comment here why
//     let cosine_term = wi.abs().dot(n.abs());
//     if pdf > 0.0f32 && !f.is_black() && cosine_term != 0.0f32 {
//         // TODO: compute ray differental rd for specular reflection - 512
//         let (li, ..) = renderer.li(scene, rd, sample, rng , arena);
//         f * li * (cosine_term / pdf)
//     }
//     return Spectrum::new(0.0f32);
// }

// TODO: document:  computes the specular transmission for the bsdf at the intersection
// TODO
// pub fn specular_transmit(
//     scene: &Scene,
//     renderer: Box<dyn Renderer>,
//     ray: &RayDifferential,
//     intersection: &Intersection,
//     bsdf: &BSDF,
//     sample: &Sample,
//     rng: &mut Rng,
//     arena: &mut MemoryArena
// ) -> Spectrum {
//     let wo = -ray.d;
//     p = &bsdf.dg_shading.p;
//     n = &bsdf.dg_shading.nn;
//     // ask the bsdf to compute incident ray direction for the reflection
//     let (f, wi, pdf) = bsdf.sample_f(
//         wo,
//         BSDFSample::new(rng),
//         // TODO: use the bitflags crate for this
//         bxdf::BSDF_TRANSMISSION  | bxdf::BSDF_SPECULAR
//     );
//     // TODO: should investigate what it means if f is black and if the consine term is 0.0 and
//     //       comment here why
//     let cosine_term = wi.abs().dot(n.abs());
//     if pdf > 0.0f32 && !f.is_black() && cosine_term != 0.0f32 {
//         // TODO: compute ray differental rd for specular reflection - 512
//         let (li, _, _) = renderer.li(scene, rd, sample, rng , arena);
//         f * li * (cosine_term / pdf)
//     }
//     return Spectrum::new(0.0f32);
// }

// -----------------------------------------------------------------------------
//                                   P R O T O
// -----------------------------------------------------------------------------

// TODO: place holder
pub trait VolumeIntegrator {
    // TODO:
    fn li(
        &self,
        scene: &Arc<Scene>,
        renderer: &dyn RendererInterface,
        ray: &mut Ray,
        sample: Option<&Sample>,
        rng: &mut RNG,
        // TODO: memory pool
    ) -> (Spectrum, Spectrum);

    // TODO: place holder
    fn request_samples(
        &self,
        _sampler: &SamplerArc,
        _sample: &mut Sample,
        _scene: &Arc<Scene>
    ) {
        // TODO:
    }

    // TODO: place holder
    fn preprocess(
        &self,
        _camera: &CameraArc,
        _scene:&Arc<Scene>,
        _renderer: &RendererInterfaceArc
    ) {
        // TODO:
    }
}
