use std::num::Wrapping;

// -----------------------------------------------------------------------------
//                                     R N G
// -----------------------------------------------------------------------------

// ------------------------------C O N S T A N T S------------------------------

/// The default seed that will be used for Random Number Generators if one is
/// not supplied
pub const DEFAULT_SEED: u64 = 19650218;

const N: usize = 312;
const M: usize = 156;
const MATRIX_A: u64 = 0xB5026F5AA96619E9;
const UPPER_MASK: u64 = 0xFFFFFFFF80000000;
const LOWER_MASK: u64 = 0x7FFFFFFF;
const MAG01: [u64; 2] = [0, MATRIX_A];

// -----------------------------R N G   S T R U C T-----------------------------
/// Non-thread safe Pesudo Random Number Generator.
///
/// Random numbers are generated using a 64-bit mersenne twister implementation
pub struct RNG {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    mt: [u64; N],
    mti: usize,
}

// ---------------------R N G   I M P L E M E N T A T I O N---------------------
impl RNG {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [RNG] using `DEFAULT_SEED`
    pub fn new() -> RNG {
        let mut ret = RNG{mt: [0; N], mti: N + 1};
        ret.seed(DEFAULT_SEED);
        ret
    }

    /// Constructs a new [RNG] using the given seed
    pub fn new_seed(s: u64) -> RNG {
        let mut ret = RNG{mt: [0; N], mti: N + 1};
        ret.seed(s);
        ret
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Seeds this [RNG]
    pub fn seed(&mut self, s: u64) {
        self.mt[0] = s;
        self.mti = 1;

        while self.mti < N {
            let x = Wrapping(6364136223846793005u64);
            let y = Wrapping(self.mt[self.mti - 1] ^ (self.mt[self.mti - 1] >> 62));
            let z = Wrapping(self.mti as u64);
            self.mt[self.mti] = (x * y + z).0;
            self.mti += 1;
        }
    }

    /// Generates a new random u64
    pub fn gen_u64(&mut self) -> u64 {
        let mut x: u64;

        // generate N words at one time
        if self.mti >= N {
            // use default seed if one has not been set
            if self.mti == N + 1 {
                self.seed(DEFAULT_SEED);
            }

            let mut i: usize = 0;
            while i < N - M {
                x = (self.mt[i] & UPPER_MASK) | (self.mt[i + 1] & LOWER_MASK);
                self.mt[i] = self.mt[i + M] ^ (x >> 1) ^ MAG01[x as usize & 1];
                i += 1;
            }
            while i < N - 1 {
                x = (self.mt[i] & UPPER_MASK) | (self.mt[i + 1] & LOWER_MASK);
                self.mt[i] =
                    self.mt[(i as i64 + (M as i64 - N as i64)) as usize] ^
                    (x >> 1) ^ MAG01[x as usize & 1];
                i += 1;
            }
            x = (self.mt[N - 1] & UPPER_MASK) | (self.mt[0] & LOWER_MASK);
            self.mt[N - 1] = self.mt[M - 1] ^ (x >> 1) ^ MAG01[x as usize & 1];

            self.mti = 0;
        }

        x = self.mt[self.mti];
        self.mti += 1;

        x ^= (x >> 29) & 0x5555555555555555;
        x ^= (x << 17) & 0x71D67FFFEDA60000;
        x ^= (x << 37) & 0xFFF7EEE000000000;
        x ^= x >> 43;

        // return
        x
    }

    /// Generates a new random u32
    pub fn gen_u32(&mut self) -> u32 {
        self.gen_u64() as u32
    }

    /// Generates a new random f32 between 0.0 and 1.0
    pub fn gen_f32(&mut self) -> f32 {
        ((self.gen_u64() & 0xFFFFFF) as f32 / ((1 << 24) as f32)) as f32
    }
}
