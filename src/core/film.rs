use std::sync::Arc;

use crate::core::sampler::CameraSample;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type FilmArc = Arc<dyn Film + Send + Sync>;

// -----------------------------------------------------------------------------
//                                    F I L M
// -----------------------------------------------------------------------------

// -----------------------------F I L M   T R A I T-----------------------------
/// Represents a camera film plane
pub trait Film {
    /// Returns pixel resolution of this [Film]
    fn resolution(&self) -> &(usize, usize);

    /// Returns the range `((x_min, y_min), (x_max-1, y_max-1))` in pixel values
    /// for which samples should be generated for to cover this [Film] plane.
    fn sample_extent(&self) -> ((i32, i32), (i32, i32));

    /// Returns the range `((x_min, y_min), (x_max-1, y_max-1))` in pixel values
    /// of this [Film]'s image
    fn pixel_extent(&self) -> ((i32, i32), (i32, i32));

    /// Adds the radiance associated with the given sample to this [Film] by
    /// assuming that sampling density around a pixel is not related to the
    /// radiance of that pixel (i.e. takes a weighted average of samples at each
    /// pixel)
    fn add_sample(&self, sample: &CameraSample, s: &Spectrum);

    /// Adds the radiance associated with the given sample to this [Film] by
    /// summing the samples at pixel to give the final value
    fn splat(&self, sample: &CameraSample, s: &Spectrum);

    /// Writes the image associated with this [Film]
    fn write_image(&self, splat_scale: f32);

    /// Notifies that a region `((x_min, y_min), (x_max-1, y_max-1))` of pixels
    /// has recently been updated
    fn update_display(
        &self,
        _region: &((i32, i32), (i32, i32)),
        _splat_scale: f32
    ) {
    }
}
