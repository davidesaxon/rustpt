use crate::core::types::GFloat;


// -----------------------------------------------------------------------------
//                               D O T   T R A I T
// -----------------------------------------------------------------------------

/// Trait that signifies the type can perform the dot product
pub trait Dot<Rhs=Self> {
    /// Returns the dot product of this and the rhs parameter
    fn dot(&self, rhs: &Rhs) -> GFloat;

    /// Returns the absolute value of the dot product of this and the rhs
    /// parameter
    fn abs_dot(&self, rhs: &Rhs) -> GFloat {
        self.dot(rhs).abs()
    }
}
