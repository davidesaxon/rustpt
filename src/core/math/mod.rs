//! # Math Utilities
//!
//! Miscellaneous useful math functions

pub mod dot;
pub mod lerp;
pub mod matrix;
pub mod quaternion;

pub use dot::Dot;
pub use lerp::Lerp;
pub use matrix::Matrix44;
pub use quaternion::Quaternion;

use std::mem;

use num::{Float, NumCast};


// -----------------------------------------------------------------------------
//                                   FUNCTIONS
// -----------------------------------------------------------------------------

/// Rounds the given number up to the nearest power of 2
/// Implemented from:
/// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
#[inline]
pub fn round_up_pow2(v: i32) -> i32 {
    let mut r = v - 1;
    r |= r >> 1;
    r |= r >> 2;
    r |= r >> 4;
    r |= r >> 8;
    r |= r >> 16;
    r += 1;
    // return
    r
}


/// Solves a quadratic equation for a, b, and c.
///
/// Returns `None` if there are no real soltuions, otherwise returns a tuple
/// of t0 and t1
#[inline]
pub fn quadratic<T: Float>(a: T, b: T, c: T) -> Option<(T, T)> {
    // setup constants
    let c_0: T = NumCast::from(0.0).unwrap();
    let c_0_5: T = NumCast::from(0.5).unwrap();
    let c_4: T = NumCast::from(4.0).unwrap();
    // find quadratic discriminant
    let discrim: T = (b * b) - (c_4 * a * c);
    if discrim < c_0 {
        return None;
    }
    let root_discrim = discrim.sqrt();

    // compute quadratic t values
    let q = if b < c_0 {
        (-c_0_5) * (b - root_discrim)
    }
    else {
        (-c_0_5) * (b + root_discrim)
    };
    let mut t0 = q / a;
    let mut t1 = c / q;
    if t0 > t1 {
        mem::swap(&mut t0, &mut t1);
    }
    Some((t0, t1))
}
