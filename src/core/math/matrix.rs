use std::mem;
use std::ops;
use std::slice;

use crate::core::types::GFloat;


// -----------------------------------------------------------------------------
//                                M A T R I X 4 4
// -----------------------------------------------------------------------------

// ------------------------M A T R I X 4 4   S T R U C T------------------------
/// a 4x4 floating point matrix
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Matrix44 {
    pub m: [[GFloat; 4]; 4]
}

// ----------------M A T R I X 4 4   I M P L E M E N T A T I O N----------------
impl Matrix44 {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new 4x4 matrix with the given data
    pub fn new(m: [[GFloat; 4]; 4]) -> Matrix44 {
        Matrix44{m}
    }

    /// Constructs a new 4x4 identity matrix
    pub fn identity() -> Matrix44 {
        Matrix44{m: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0]
        ]}
    }

    /// Constructs a new 4x4 zero'd matrix
    pub fn zero() -> Matrix44 {
        Matrix44{m: [
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0]
        ]}
    }

    // ---------------------------ASSOCIATED FUNCTIONS--------------------------

    pub fn solve_linear_system_2x2(
        a: &[[GFloat; 2]; 2],
        b: &[GFloat; 2]
    ) -> Option<(GFloat, GFloat)> {
        let det = (a[0][0] * a[1][1]) - (a[0][1] * a[1][0]);
        if det.abs() < 1e-10 {
            return None;
        }
        let x0 = ((a[1][1] * b[0]) - (a[0][1] * b[1])) / det;
        let x1 = ((a[0][0] * b[1]) - (a[1][0] * b[0])) / det;
        if x0.is_nan() || x1.is_nan() {
            return None;
        }
        // return
        Some((x0, x1))
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns the transpose of this [Matrix44]
    pub fn transpose(&self) -> Matrix44 {
        Matrix44{m: [
            [self.m[0][0], self.m[1][0], self.m[2][0], self.m[3][0]],
            [self.m[0][1], self.m[1][1], self.m[2][1], self.m[3][1]],
            [self.m[0][2], self.m[1][2], self.m[2][2], self.m[3][2]],
            [self.m[0][3], self.m[1][3], self.m[2][3], self.m[3][3]]
        ]}
    }

    /// Returns the inverse of this [Matrix44]
    pub fn inverse(&self) -> Matrix44 {
        let mut indxc: [usize; 4] = [0, 0, 0, 0];
        let mut indxr: [usize; 4] = [0, 0, 0, 0];
        let mut ipiv = [0, 0, 0, 0];
        let mut minv = self.m;
        for i in 0..4 {
            let mut irow: i32 = -1;
            let mut icol : i32 = -1;
            let mut big: GFloat = 0.0;
            // chose pivot
            for j in 0..4 {
                if ipiv[j] != 1 {
                    for k in 0..4 {
                        if ipiv[k] == 0 {
                            if minv[j][k].abs()  >= big {
                                irow = j as i32;
                                icol = k as i32;
                                big = minv[j][k].abs();
                            }
                        }
                        else if ipiv[k] > 1 {
                            // TODO: handle better? - at least only do this
                            // at debug time?
                            panic!(
                                "Singular matrix in Matrix44::inverse: {:?}",
                                self
                            );
                        }
                    }
                }
            }
            ipiv[icol as usize] += 1;
            // swap irow and icol for pivot
            if irow != icol {
                for k in 0..4 {
                    let ptr = minv.as_mut_ptr();
                    unsafe {
                        let mut swap_a = slice::from_raw_parts_mut(
                            ptr,
                            minv.len()
                        )[irow as usize];
                        let mut swap_b = slice::from_raw_parts_mut(
                            ptr,
                            minv.len()
                        )[icol as usize];
                        mem::swap(&mut swap_a[k], &mut swap_b[k]);
                    }
                }
            }
            indxr[i] = irow as usize;
            indxc[i] = icol as usize;
            if minv[icol as usize][icol as usize] == 0.0 {
                // TODO: handle better? - at least only do this
                // at debug time?
                panic!(
                    "Singular matrix in Matrix44::inverse: {:?}",
                    self
                );
            }

            // set m[icol][icol] to one by scaling row icol appropriately
            let pivinv = 1.0 / minv[icol as usize][icol as usize];
            minv[icol as usize][icol as usize] = 1.0;
            for j in 0..4 {
                minv[icol as usize][j] *= pivinv;
            }

            // subtract this row from others to zero out their columns
            for j in 0_usize..4 {
                if j != icol as usize {
                    let save = minv[j][icol as usize];
                    minv[j][icol as usize] = 0.0;
                    for k in 0..4 {
                        minv[j][k] -= minv[icol as usize][k] * save;
                    }
                }
            }

            // swap columns to reflect to permutation
            for j in (0_usize..4).rev() {
                if indxr[j] != indxc[j] {
                    for k in 0_usize..4 {
                        // TODO: fix swap
                        // let a = minv[k][indxr[j]];
                        // let b = minv[k][indxc[j]];
                        // minv[k][indxr[j]] = b;
                        // minv[k][indxc[j]] = a;
                        let ptr = minv.as_mut_ptr();
                        unsafe {
                            let mut swap_a =
                                slice::from_raw_parts_mut(ptr, minv.len())[k];
                            let mut swap_b =
                                slice::from_raw_parts_mut(ptr, minv.len())[k];
                            mem::swap(&mut swap_a[indxr[j]], &mut swap_b[indxc[j]]);
                        }
                    }
                }
            }
        }

        Matrix44::new(minv)
    }
}

// ------------------M A T R I X 4 4   M U L   M A T R I X 4 4------------------
impl ops::Mul<&Matrix44> for &Matrix44 {
    type Output = Matrix44;

    fn mul(self, other: &Matrix44) -> Matrix44 {
        let mut r = Matrix44::identity();
        // TODO: does unrolling this optimtise?
        for i in 0..4 {
            for j in 0..4 {
                r.m[i][j] =
                    (self.m[i][0] * other.m[0][j]) +
                    (self.m[i][1] * other.m[1][j]) +
                    (self.m[i][2] * other.m[2][j]) +
                    (self.m[i][3] * other.m[3][j]);
            }
        }
        r
    }
}

// -----------M A T R I X 4 4   M U L   A S S I G N   M A T R I X 4 4-----------
impl ops::MulAssign<&Matrix44> for Matrix44 {
    fn mul_assign(&mut self, other: &Matrix44) {
        // TODO: does unrolling this optimtise?
        for i in 0..4 {
            for j in 0..4 {
                self.m[i][j] =
                    (self.m[i][0] * other.m[0][j]) +
                    (self.m[i][1] * other.m[1][j]) +
                    (self.m[i][2] * other.m[2][j]) +
                    (self.m[i][3] * other.m[3][j]);
            }
        }
    }
}


// TODO: test
