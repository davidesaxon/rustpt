use crate::core::types::TFloat;


// -----------------------------------------------------------------------------
//                              L E R P   T R A I T
// -----------------------------------------------------------------------------

/// Trait that signifies the type can be linearly interpolated
pub trait Lerp {
    /// linearly interpolates at t between v1 and v2
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self;
}

// -----------------------------U S I Z E   L E R P-----------------------------
impl Lerp for usize {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        (((1.0 - t) * (v1 as TFloat)) + (t * (v2 as TFloat))) as Self
    }
}

// -------------------------------I 3 2   L E R P-------------------------------
impl Lerp for i32 {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        (((1.0 - t) * (v1 as TFloat)) + (t * (v2 as TFloat))) as Self
    }
}

// -------------------------------F 3 2   L E R P-------------------------------
impl Lerp for f32 {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        ((1.0 - t as Self) * v1) + (t as Self * v2)
    }
}

// -------------------------------F 6 4   L E R P-------------------------------
impl Lerp for f64 {
    fn lerp(t: TFloat, v1: Self, v2: Self) -> Self {
        ((1.0 - t as Self) * v1) + (t as Self * v2)
    }
}

// TODO: other types


// TODO: test
