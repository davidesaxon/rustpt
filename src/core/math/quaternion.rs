use std::ops;

use crate::core::geometry::Transform;
use crate::core::geometry::Vector;
use crate::core::types::GFloat;
use crate::core::types::TFloat;
use super::dot::Dot;
use super::matrix::Matrix44;


// -----------------------------------------------------------------------------
//                              Q U A T E R N I O N
// -----------------------------------------------------------------------------

// ----------------------Q U A T E R N I O N   S T R U C T----------------------
/// Quaternion reprsented by a [Vector] and a float
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Quaternion {
    pub v: Vector,
    pub w: GFloat,
}

// --------------Q U A T E R N I O N   I M P L E M E N T A T I O N--------------
impl Quaternion {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [Quaternion] with the given [Vector] and w components
    pub fn new(v: &Vector, w: GFloat) -> Quaternion {
        Quaternion{v: *v, w: w}
    }

    /// Constructs a new unit [Quaternion]
    pub fn unit() -> Quaternion {
        Quaternion{v: Vector::zero(), w: 0.0}
    }

    /// Constructs a new [Quaternion] that represents the rotation of the given
    /// [Matrix44]
    pub fn from_matrix44(m: &Matrix44) -> Quaternion {
        let trace = m.m[0][0] + m.m[1][1] + m.m[2][2];
        if trace > 0.0 {
            // compute w from matrix trace, then xyz
            let mut s = (trace + 1.0).sqrt();
            let w = s / 2.0;
            s = 0.5 / s;
            return Quaternion{
                v: Vector::new(
                    (m.m[2][1] - m.m[1][2]) * s,
                    (m.m[0][2] - m.m[2][0]) * s,
                    (m.m[1][0] - m.m[0][1]) * s
                ),
                w: w
            };
        }
        // compute largest of x, y, or z, and then remaining components
        let nxt = [1, 2, 0];
        let mut q = [0.0, 0.0, 0.0];
        let mut i = 0;
        if m.m[1][1] > m.m[0][0] {
            i = 1;
        }
        if m.m[2][2] > m.m[i][i] {
            i = 2;
        }
        let j = nxt[i];
        let k = nxt[j];
        let mut s =
            ((m.m[i][i] - (m.m[j][j] + m.m[k][k])) + 1.0).sqrt();
        q[i] = s * 0.5;
        if s != 0.0 {
            s = 0.5 / s;
        }
        q[j] = (m.m[j][i] + m.m[i][j]) * s;
        q[k] = (m.m[k][i] + m.m[i][k]) * s;
        // return
        Quaternion{
            v: Vector::new(q[0], q[1], q[2]),
            w: (m.m[k][j] - m.m[j][k]) * s
        }
    }

    /// Constructs a new [Quaternion] that represents the rotation of the given
    /// [Transform]
    pub fn from_transform(t: &Transform) -> Quaternion {
        Quaternion::from_matrix44(&t.m)
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Returns a new normalized version of this [Quaternion]
    pub fn normalize(&self) -> Quaternion {
        self / self.dot(self).sqrt()
    }

    /// Returns the resulting [Quaternion] from performing spherical linear
    /// interpolation between this [Quaternion] and the other [Quaternion] by t
    pub fn slerp(&self, t: TFloat, q: &Quaternion) -> Quaternion {
        let cos_theta = self.dot(q);
        if cos_theta > 0.9995 {
            // if the Quaternions are nearly parallel use linearl interpolation
            // to avoid numerical instability
            return (
                &(self * (1.0 - (t as GFloat))) +
                &(q * (t as GFloat))
            ).normalize();
        }
        let theta = cos_theta.clamp(-1.0, 1.0).acos();
        let theta_p = theta * (t as GFloat);
        let q_perp = (q - &(self * cos_theta)).normalize();
        // return
        &(self * theta_p.cos()) + &(&q_perp * theta_p.sin())
    }

    /// Returns a [Matrix44] that represents this [Quaternion]
    pub fn to_matrix44(&self) -> Matrix44 {
        let xx = self.v.x * self.v.x;
        let yy = self.v.y * self.v.y;
        let zz = self.v.z * self.v.z;
        let xy = self.v.x * self.v.y;
        let xz = self.v.x * self.v.z;
        let yz = self.v.y * self.v.z;
        let wx = self.v.x * self.w;
        let wy = self.v.y * self.w;
        let wz = self.v.z * self.w;
        let m = Matrix44::new([
            [
                1.0 - 2.0 * (yy + zz),
                2.0 * (xy + wz),
                2.0 * (xz - wy),
                0.0
            ],
            [
                2.0 * (xy - wz),
                1.0 - 2.0 * (xx + zz),
                2.0 * (yz + wx),
                0.0
            ],
            [
                2.0 * (xz + wy),
                2.0 * (yz - wx),
                1.0 - 2.0 * (xx + yy),
                0.0
            ],
            [0.0, 0.0, 0.0, 1.0]
        ]);
        // transpose since we're left handed
        m.transpose()
    }

    /// Returns a [Transform] that represents this [Quaternion]
    pub fn to_transform(&self) -> Transform {
        let m = self.to_matrix44();
        Transform::new(&m, &m.inverse())
    }
}

// --------------Q U A T E R N I O N   A D D   Q U A T E R N I O N--------------
impl ops::Add<&Quaternion> for &Quaternion{
    type Output = Quaternion;

    fn add(self, q: &Quaternion) -> Quaternion {
        Quaternion::new(&(&self.v + &q.v), self.w + q.w)
    }
}

// -------Q U A T E R N I O N   A D D   A S S I G N   Q U A T E R N I O N-------
impl ops::AddAssign<&Quaternion> for Quaternion {
    fn add_assign(&mut self, q: &Quaternion) {
        self.v += &q.v;
        self.w += q.w;
    }
}

// --------------Q U A T E R N I O N   S U B   Q U A T E R N I O N--------------
impl ops::Sub<&Quaternion> for &Quaternion{
    type Output = Quaternion;

    fn sub(self, q: &Quaternion) -> Quaternion {
        Quaternion::new(&(&self.v - &q.v), self.w - q.w)
    }
}

// -------Q U A T E R N I O N   S U B   A S S I G N   Q U A T E R N I O N-------
impl ops::SubAssign<&Quaternion> for Quaternion {
    fn sub_assign(&mut self, q: &Quaternion) {
        self.v -= &q.v;
        self.w -= q.w;
    }
}

// ------------------Q U A T E R N I O N   M U L   S C A L A R------------------
impl ops::Mul<GFloat> for &Quaternion {
    type Output = Quaternion;

    fn mul(self, s: GFloat) -> Quaternion {
        Quaternion::new(&(&self.v * s), self.w * s)
    }
}

// -----------Q U A T E R N I O N   M U L   A S S I G N   S C A L A R-----------
impl ops::MulAssign<GFloat> for Quaternion {
    fn mul_assign(&mut self, s: GFloat) {
        self.v *= s;
        self.w *= s;
    }
}

// ------------------Q U A T E R N I O N   D I V   S C A L A R------------------
impl ops::Div<GFloat> for &Quaternion {
    type Output = Quaternion;

    fn div(self, s: GFloat) -> Quaternion {
        debug_assert!(s != 0.0, "Attempted to divide Quaternion by 0 scalar");
        let inv = 1.0 / s;
        Quaternion::new(&(&self.v * inv), self.w * inv)
    }
}

// -----------Q U A T E R N I O N   D I V   A S S I G N   S C A L A R-----------
impl ops::DivAssign<GFloat> for Quaternion {
    fn div_assign(&mut self, s: GFloat) {
        debug_assert!(s != 0.0, "Attempted to divide Quaternion by 0 scalar");
        let inv = 1.0 / s;
        self.v *= inv;
        self.w *= inv;
    }
}

// --------------Q U A T E R N I O N   D O T   Q U A T E R N I O N--------------
impl Dot<Quaternion> for Quaternion {
    fn dot(&self, q: &Quaternion) -> GFloat {
        self.v.dot(&q.v) + (self.w * q.w)
    }
}


// TODO: test
