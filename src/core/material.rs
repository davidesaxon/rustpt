use std::sync::Arc;

use crate::core::geometry::DifferentialGeometry;
use crate::core::reflection::BSDF;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type MaterialArc = Arc<dyn Material + Send + Sync>;

// -----------------------------------------------------------------------------
//                                M A T E R I A L
// -----------------------------------------------------------------------------

// -------------------------M A T E R I A L   T R A I T-------------------------
/// Trait that represents a material within Riptide
pub trait Material {
    // --------------------------------FUNCTIONS--------------------------------
    /// Returns a [BSDF] that holds information about the final shading geometry
    /// at the point and the BRDF and BTDF components.
    ///
    /// *`dg_geom` The actual [DifferentialGeometry] at the ray intersection
    ///            point.
    /// *`dg_shading` The shading [DifferentialGeometry] this may be different
    ///               to `dg_geom` due to things such as per-vertex normals. The
    ///               shading [DifferentialGeometry] may be futther modified due
    ///               to bump mapping.
    fn get_bsdf(
        &self,
        dg_geom: &DifferentialGeometry,
        dg_shading: &DifferentialGeometry
    ) -> BSDF;

    // TODO:
    // /// Returns a [BSSRDF] that represnets the object's subsurface scattering
    // /// properties
    // ///
    // /// *`dg_geom` The actual [DifferentialGeometry] at the ray intersection
    // ///            point.
    // /// *`dg_shading` The shading [DifferentialGeometry] this may be different
    // ///               to `dg_geom` due to things such as per-vertex normals.
    // fn get_bssrdf(
    //     &self,
    //     dg_geom: &DifferentialGeometry,
    //     dg_shading: &DifferentialGeometry
    // ) -> Option<BSSRDF> {
    //     None
    // }
}
