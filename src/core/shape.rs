use std::rc::Rc;
use std::sync::Arc;

use log::error;

use crate::core::geometry::traits::Eval;
use crate::core::geometry::{AABB, DifferentialGeometry, Ray, Transform};
use crate::core::types::GFloat;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type ShapeArc = Arc<dyn Shape + Send + Sync>;

// -----------------------------------------------------------------------------
//                                   S H A P E
// -----------------------------------------------------------------------------

static mut NEXT_SHAPE_ID: u64 = 1;

// --------------------S H A P E   C O M M O N   S T R U C T--------------------
/// Data that is common to all shapes
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct ShapeCommon {
    /// The unique identifier of this [Shape]
    pub shape_id: u64,
    /// [Transform] that maps from world space to this [Shape]'s object space.
    pub object_to_world: Transform,
    /// [Transform] that maps from this [Shape]'s object space to world space.
    pub world_to_object: Transform,
    /// Whether this orientation of this [Shape]'s geometry should be reversed
    pub reverse_orientation: bool,
    /// Whther this [Shape]'s [Transform]s swap the co-ordinate system
    /// handedness
    pub transform_swaps_handedness: bool,
}

// ------------S H A P E   C O M M O N   I M P L E M E N T A T I O N------------
impl ShapeCommon {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs new [ShapeCommon] data from the given parameters
    pub fn new(
        object_to_world: &Transform,
        world_to_object: &Transform,
        reverse_orientation: bool
    ) -> ShapeCommon {
        // TODO: shape_id is not thread safe!
        ShapeCommon {
            shape_id: unsafe{let t = NEXT_SHAPE_ID; NEXT_SHAPE_ID += 1; t},
            object_to_world: *object_to_world,
            world_to_object: *world_to_object,
            reverse_orientation: reverse_orientation,
            transform_swaps_handedness: object_to_world.swaps_handedness()
        }
    }
}

// ----------------------------S H A P E   T R A I T----------------------------
/// Specifies a geometric shape to be rendered
pub trait Shape {
    // --------------------------------FUNCTIONS--------------------------------
    /// Returns the [ShapeCommon] data associated with this [Shape]
    fn common(&self) -> &ShapeCommon;

    /// Returns the bounds of this [Shape] in the [Shape]'s object space
    fn object_bound(&self) -> AABB;

    /// Returns the bounds of this [Shape] in world space
    /// **Note:** This default implementation should be overridden for [Shape]s
    ///           that can compute a tighter bound rather than directly
    ///           transform the object bound into world space.
    fn world_bound(&self) -> AABB {
        // TODO: should this cache and return a reference instead?
        self.common().object_to_world.eval(&self.object_bound())
    }

    /// Returns whether this [Shape] can compute ray intersections
    /// **Note:** Since this defaults to returning `true`, only [Shape]s that
    ///           are non-intersectable need to override this function.
    fn can_intersect(&self) -> bool {
        true
    }

    /// Returns whether the given [Ray] intersects with this [Shape] and
    /// information about the intersection if it occured.
    ///
    /// **Note**: Only intersectable [Shape] implmenetations need to override
    ///           this function.
    ///
    /// **Note**: implementations of this function must only check for
    ///           intersection with respect to the [Ray]'s min_t and max_t
    ///           attributes.
    ///
    /// * `ray` The [Ray] to check intersections against.
    /// * `this` The shared pointer to this [Shaoe] so it can be used to
    ///          construct the returned [DifferentialGeometry] object.
    ///
    /// If the [Ray] does not intersect then `None` will be returned, otherwise
    /// a tuple will be returned with information about the closest intersection
    /// where the first value is the t value on the [Ray] where the intersection
    /// occured, the second value is the maximum numeric error in the
    /// intersection caculation, and the third value is the
    /// [DifferentialGeometry] for the [Ray] intersection point on this
    /// [Shape]'s surface.
    fn intersect(
        &self,
        _ray: &Ray,
        _this: &ShapeArc
    ) -> Option<(GFloat, GFloat, Rc<DifferentialGeometry>)> {
        error!("Unimplemented Shape::intersect() function called");
        None
    }

    /// Returns whether the given [Ray] intersects with this [Shape]
    ///
    /// **Note**: Only intersectable [Shape] implmenetations need to override
    ///           this function.
    ///
    /// **Note**: implementations of this function must only check for
    ///         intersection with respect to the [Ray]'s min_t and max_t
    ///         attributes.
    fn intersect_p(&self, _ray: &Ray) -> bool {
        error!("Unimplemented Shape::intersect_p() function called");
        false
    }

    /// If can_intersect() returns `false` this function is called to refine
    /// this [Shape] into a group of a new [Shape]s. The resulting [Shape]s can
    /// be intersectable or non-intersectable and require further refinement.
    fn refine(&self) -> Vec<ShapeArc> {
        error!("Unimplemented Shape::refine() function called");
        Vec::new()
    }

    /// Returns the shading geometry for this [Shape] corresponding to the
    /// [DifferentialGeometry] returned by `Shape::intersect()``.
    ///
    /// The object_to_world [Transform] must be passed to this function rather
    /// than using object_to_world from [ShapeCommon] to allow for instancing
    /// implementations.
    ///
    /// Only certian [Shape]s have shading geometry that is different to the
    /// true geometry, so by default this function just returns the
    /// [DifferentialGeometry] passed in from `Shape::intersect()`.
    fn get_shading_geometry(
            &self,
            _object_to_world: &Transform,
            dg: &Rc<DifferentialGeometry>
    ) -> Rc<DifferentialGeometry> {
        Rc::clone(dg)
    }

    /// Returns the surface area of this [Shape] in object space.
    ///
    /// **Note:** Only intersectable [Shape] implementations need to override
    ///           this function.
    fn surface_area(&self) -> GFloat {
        error!("Unimplemented Shape::surface_area() function called");
        0.0
    }
}
