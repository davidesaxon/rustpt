//! Various primitive types of Riptide - typedef'ed so they can be modified if
//! needed
use std::f64;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

/// the floating point type used for geometric operations
pub type GFloat = f32;
/// the floating point type used for spectrum based operations
pub type SFloat = f32;
/// the floating point type used for time based operations
pub type TFloat = f64;

// -----------------------------------------------------------------------------
//                               C O N S T A N T S
// -----------------------------------------------------------------------------

pub const G_PI: GFloat = f64::consts::PI as GFloat;
pub const G_INV_PI: GFloat = 1.0 / G_PI;
