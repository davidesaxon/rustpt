use std::mem;
use std::rc::Rc;
use std::sync::{Arc, RwLock};

use log::warn;

use crate::core::geometry::{AABB, DifferentialGeometry, Ray, Transform};
use crate::core::intersection::Intersection;
use crate::core::primitive::{self, Primitive, PrimitiveArc};
use crate::core::reflection::BSDF;
use crate::core::shape::*;


// -----------------------------------------------------------------------------
//                     G E O M E T R I C   P R I M I T I V E
// -----------------------------------------------------------------------------

// -------------G E O M E T R I C   P R I M I T I V E   S T R U C T-------------
/// Primitive that represents a single [Shape] in the scene
pub struct GeometricPrimitive {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    /// The unique identifier of this [Primitive]
    pub primitive_id: u64,
    /// The [Shape] of this primitive
    pub shape: ShapeArc,
    // TODO:
    /// The [Material] of this primitive
    // pub material: Rc<dyn Material>,
    // /// The [AreaLight] that describes this primitive's emission if it is
    // /// emissive
    // pub area_light: Option<Rc<AreaLight>>,
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    // the list of primitives this primitive has refined into
    refined: RwLock<Option<Vec<PrimitiveArc>>>
}

// -----G E O M E T R I C   P R I M I T I V E   I M P L E M E N T A T I O N-----
impl GeometricPrimitive {
    // -------------------------------CONSTRUCTORS------------------------------
    // TODO:
    // /// Constructs a new [GeometricPrimitive] from the given parameters.
    // pub fn new(
    //     shape: &ShapeArc,
    //     material: &Rc<dyn Material>,
    //     area_light: &Option<Rc<AreaLight>>
    // ) -> GeometricPrimitive {
    //     GeometricPrimitive {
    //         primitive_id: primitive::get_next_primitive_id(),
    //         shape: Rc::clone(shape),
    //         material: Rc::clone(material),
    //         area_light: *area_light,
    //         refined: RwLock::new(None)
    //     }
    // }

    // TODO: REMOVE ME
    pub fn proto_new(shape: &ShapeArc) -> GeometricPrimitive {
        GeometricPrimitive {
            primitive_id: primitive::get_next_primitive_id(),
            shape: Arc::clone(shape),
            refined: RwLock::new(None)
        }
    }
}

// ------------------------P R I M I T I V E   T R A I T------------------------
impl Primitive for GeometricPrimitive {
    fn id(&self) -> u64 {
        self.primitive_id
    }

    fn world_bound(&self) -> AABB {
        self.shape.world_bound()
    }

    fn can_intersect(&self) -> bool {
        self.shape.can_intersect()
    }

    fn intersect(
        &self,
        ray: &mut Ray,
        this: &PrimitiveArc
    ) -> Option<Intersection> {
        let isect =  self.shape.intersect(ray, &self.shape);
        let (t, ray_epsilon, dg) = if let Some(i) = isect {
            i
        }
        else {
            return None;
        };
        // update the maximum limit of the ray to t so that intersections behind
        // this primitive are not tested
        ray.max_t = t;
        // construct and return the Intersection object
        let shape_common = self.shape.common();
        Some(Intersection::new(
            &dg,
            this,
            &shape_common.world_to_object,
            &shape_common.object_to_world,
            shape_common.shape_id,
            self.primitive_id,
            ray_epsilon
        ))
    }

    fn intersect_p(&self, ray: &Ray) -> bool {
        self.shape.intersect_p(ray)
    }

    fn refine(&self) -> Vec<PrimitiveArc> {
        warn!("GeometricPrimitive::refine has an unfinished implementation");
        // refine?
        let refined = self.refined.read().unwrap();
        if refined.is_none() {
            let mut ret:  Vec<PrimitiveArc> = Vec::new();
            for shape in self.shape.refine() {
                // TODO:
                // ret.push(GeometricPrimitive::new(
                //     &shape,
                //     &self.material
                //     &self.area_light
                // ));
                ret.push(Arc::new(GeometricPrimitive::proto_new(&shape)));
            }
            // TODO: REMOVE ME
            if !ret.is_empty() {
                println!("Prim {} refined new prims:", self.id());
                for p in &ret {
                    println!("\t-> {}", p.id());
                }
            }
            mem::drop(refined);
            *self.refined.write().unwrap() = Some(ret);
        }
        let refined = self.refined.read().unwrap();
        // return new list
        let mut ret: Vec<PrimitiveArc> = Vec::new();
        for prim in refined.as_ref().unwrap() {
            ret.push(Arc::clone(prim));
        }
        // return
        ret
    }

    // TODO:
    // fn get_area_light(&self) -> Option<Rc<AreaLight>> {
    //     self.area_light
    // }

    fn get_bsdf(
        &self,
        dg: &Rc<DifferentialGeometry>,
        object_to_world: &Transform
    ) -> BSDF {
        let _dgs = self.shape.get_shading_geometry(object_to_world, dg);
        // TODO: need material
        panic!("GeometricPrimitive::get_bsdf implementation unfinished!");
    }

    // TODO:
    // fn get_bsdf(
    //     &self,
    //     dg: &Rc<DifferentialGeometry>,
    //     object_to_world: &Transform,
    //     arena: &MemoryArena
    // ) -> BSDF {
    //     let dgs = self.shape.get_shading_geometry(self.object_to_world, dg);
    //     // return
    //     material.get_bsdf(dg, &dgs, arena)
    // }

    // TODO:
    // fn get_bssrdf(
    //     &self,
    //     dg: &Rc<DifferentialGeometry>,
    //     object_to_world: &Transform,
    //     arena: &MemoryArena
    // ) -> BSSRDF {
    //     let dgs = self.shape.get_shading_geometry(self.object_to_world, dg);
    //     // return
    //     material.get_bssrdf(dg, &dgs, arena)
    // }
}
