//! # Renderable primitives
//!
//! Definitions of primitives that bind geometry and a material.
use std::rc::Rc;
use std::sync::Arc;

use log::error;

use crate::core::geometry::{AABB, DifferentialGeometry, Ray, Transform};
use crate::core::intersection::Intersection;
use crate::core::reflection::BSDF;

pub mod geometric;
pub mod transformed;

pub use geometric::GeometricPrimitive;
pub use transformed::TransformedPrimitive;


// -----------------------------------------------------------------------------
//                                   T Y P E S
// -----------------------------------------------------------------------------

pub type PrimitiveArc = Arc<dyn Primitive + Send + Sync>;

// -----------------------------------------------------------------------------
//                                   FUNCTIONS
// -----------------------------------------------------------------------------

static mut NEXT_PRIMITIVE_ID: u64 = 1;

/// Returns the next [Primitive] id to be used and increments the global counter
pub fn get_next_primitive_id() -> u64 {
    unsafe {
        let t = NEXT_PRIMITIVE_ID;
        NEXT_PRIMITIVE_ID += 1;
        t
    }
}

// -----------------------------------------------------------------------------
//                               P R I M I T I V E
// -----------------------------------------------------------------------------

// ------------------------P R I M I T I V E   T R A I T------------------------
/// Defines a renderable element
pub trait Primitive {
    // --------------------------------FUNCTIONS--------------------------------
    /// Returns the unique id of this primitive
    fn id(&self) -> u64;

    /// Returns the bounds of this primitive's geometry in world space
    fn world_bound(&self) -> AABB;

    /// Returns whether this [Primitive] can compute ray intersections
    /// **Note:** Since this defaults to returning `true`, only [Primitive]s
    ///           that are non-intersectable need to override this function.
    fn can_intersect(&self) -> bool {
        true
    }

    /// Returns whether the given [Ray] intersects with this [Primitive] and
    /// information about the [Intersection] if it occured.
    ///
    /// * `ray` The [Ray] to check intersections against.
    /// * `this` The shared pointer to this primitive so it can be used to
    ///          construct the returned [Intersection] object.
    fn intersect(
        &self,
        ray: &mut Ray,
        this: &PrimitiveArc
    ) -> Option<Intersection>;


    /// Returns whether the given [Ray] intersects with this [Primitive].
    fn intersect_p(&self, ray: &Ray) -> bool;

    /// If can_intersect returns `false` this function is called to refine this
    /// [Primitive] into a group of new [Primitive]s. The resulting
    /// [Primitive]'s can be intersectable or non-intersectable and require
    /// further refinement.
    fn refine(&self) -> Vec<PrimitiveArc> {
        error!("Unimplemented Primitive::refine() function called");
        Vec::new()
    }

    /// Repeatly refines this Primitive and returns the list of fully refined
    /// [Primitive]s.
    fn fully_refine(&self) -> Vec<PrimitiveArc> {
        let mut ret:  Vec<PrimitiveArc> = Vec::new();
        // can this be refined?
        if !self.can_intersect() {
            // refine until there is no work left to do
            let mut to_refine = self.refine();
            while !to_refine.is_empty() {
                let prim = to_refine.pop().unwrap();
                // refine further?
                if prim.can_intersect() {
                    ret.push(prim);
                }
                else {
                    to_refine.extend(prim.refine());
                }
            }
        }
        // return
        ret
    }

    // TODO: improve this?
    // /// TODO: document
    // fn get_area_light(&self) -> Option<Rc<AreaLight>>;

    /// Returns a [BSDF] that desribes the local light scattering properties at
    /// the intersection point.
    fn get_bsdf(
        &self,
        dg: &Rc<DifferentialGeometry>,
        object_to_world: &Transform
    ) -> BSDF;

    // TODO:
    // /// Returns a [BSSRDF] which describes the subsurface scattering inside the
    // /// primitive
    // fn get_bssrdf(
    //     &self,
    //     dg: &Rc<DifferentialGeometry>,
    //     object_to_world: &Transform,
    //     arena: &MemoryArena
    // ) -> BSSRDF;
}
//
