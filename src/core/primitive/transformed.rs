use std::rc::Rc;
use std::sync::Arc;

use crate::core::geometry::*;
use crate::core::intersection::Intersection;
use crate::core::primitive::{self, Primitive, PrimitiveArc};
use crate::core::reflection::BSDF;


// -----------------------------------------------------------------------------
//                   T R A N S F O R M E D   P R I M I T I V E
// -----------------------------------------------------------------------------

// -----------T R A N S F O R M E D   P R I M I T I V E   S T R U C T-----------
/// A [Primitive] that adds another layer of transformations to its contained
/// [Primitive] for animation and/or instancing.
///
/// **Note:** The child [Primitive] must be intersectable.
pub struct TransformedPrimitive {
    /// The unique identifier of this [Primitive]
    pub primitive_id: u64,
    /// The child [Primitive] to be transformed by this
    pub child: PrimitiveArc,
    /// The [AnimatedTransform] to apply to the [Primitive]
    pub world_to_child: AnimatedTransform,
}

// ---T R A N S F O R M E D   P R I M I T I V E   I M P L E M E N T A T I O N---
impl TransformedPrimitive {
    // -------------------------------COSNTRUCTORS------------------------------
    /// Constructs a new [TransformedPrimitive] from the given paramters.
    pub fn new(
        child: &PrimitiveArc,
        world_to_child: AnimatedTransform
    ) -> TransformedPrimitive {
        TransformedPrimitive{
            primitive_id: primitive::get_next_primitive_id(),
            child: Arc::clone(child),
            world_to_child: world_to_child
        }
    }
}

// -------------------------P R I M T I V E   T R A I T-------------------------
impl Primitive for TransformedPrimitive {
    fn id(&self) -> u64 {
        self.primitive_id
    }

    fn world_bound(&self) -> AABB {
        self.world_to_child.compute_motion_bounds(
            &self.child.world_bound(),
            true
        )
    }

    fn intersect(
        &self,
        ray: &mut Ray,
        _this: &PrimitiveArc
    ) -> Option<Intersection> {
        let world_to_child = self.world_to_child.interpolate(ray.time);
        // transform the ray into this primitive space
        let mut r = world_to_child.eval(ray);
        // check if ray intersects with child
        let isect = self.child.intersect(&mut r, &self.child);
        let mut intersection = if let Some(i) = isect {
            i
        }
        else {
            return None;
        };
        // update max_t on the original ray
        ray.max_t = r.max_t;
        // update the intersection info
        intersection.primitive_id = self.primitive_id;
        if !world_to_child.is_identity() {
            // compute the new world to object transforms
            intersection.world_to_object *= &world_to_child;
            intersection.object_to_world =
                intersection.world_to_object.inverse();
            // transform DifferentialGeometry into world space
            let primitive_to_world = world_to_child.inverse();
            // TODO: optimise this
            let dg = intersection.dg;
            intersection.dg = Rc::new(DifferentialGeometry::new(
                primitive_to_world.eval(&dg.p),
                // primitive_to_world.eval(&dg.n).normalize(),
                dg.u,
                dg.v,
                primitive_to_world.eval(&dg.dpdu),
                primitive_to_world.eval(&dg.dpdv),
                primitive_to_world.eval(&dg.dndu),
                primitive_to_world.eval(&dg.dndv),
                &dg.shape
            ));
        }
        // return
        Some(intersection)
    }

    fn intersect_p(&self, ray: &Ray) -> bool {
        let world_to_child = self.world_to_child.interpolate(ray.time);
        self.child.intersect_p(&world_to_child.eval(ray))
    }

    // TODO:
    // fn get_area_light(&self) -> Option<Rc<AreaLight>> {
    //     panic!("TransformedPrimitive::get_area_light should never be called");
    // }

    fn get_bsdf(
        &self,
        _dg: &Rc<DifferentialGeometry>,
        _object_to_world: &Transform
    ) -> BSDF {
        panic!("TransformedPrimitive::get_bsdf should never be called");
    }

    // TODO:
    // fn get_bssrdf(
    //     &self,
    //     dg: &Rc<DifferentialGeometry>,
    //     object_to_world: &Transform,
    //     arena: &MemoryArena
    // ) -> BSSRDF {
    //     panic!("TransformedPrimitive::get_bssrdf should never be called");
    // }
}
