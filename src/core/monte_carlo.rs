use std::mem;
use std::slice;

use crate::core::rng::RNG;
use crate::core::types::*;


// TODO: clean up, possibly move, and document

// -----------------------------------------------------------------------------
//                                   FUNCTIONS
// -----------------------------------------------------------------------------

/// Fills `samples` with 1 dimensional stratified samples
pub fn stratified_sample_1d(
    samples: &mut [GFloat],
    jitter: bool,
    rng: &mut RNG
) {
    let inv_n = 1.0 / (samples.len() as GFloat);
    for i in 0..samples.len() {
        let delta = if jitter {rng.gen_f32()} else {0.5} as GFloat;
        samples[i] = (i as GFloat + delta) * inv_n;
    }
}

/// Fills `samples` with 2 dimensional stratified samples
pub fn stratified_sample_2d(
    samples: &mut [GFloat],
    n_strata: (usize, usize),
    jitter: bool,
    rng: &mut RNG
) {
    let dx = 1.0 / (n_strata.0 as GFloat);
    let dy = 1.0 / (n_strata.1 as GFloat);
    for y in 0..n_strata.1 {
        for x in 0..n_strata.0 {
            let jx = if jitter {rng.gen_f32()} else {0.5} as GFloat;
            let jy = if jitter {rng.gen_f32()} else {0.5} as GFloat;
            let offset = ((y * n_strata.0) + x) * 2;
            samples[offset + 0] = (x as GFloat + jx) * dx;
            samples[offset + 1] = (y as GFloat + jy) * dy;
        }
    }
}

/// Shuffles a given n-dimensional array of floats
///
/// * `samples` The array of samples to shuffle.
/// * `count` The number of n-dimensional samples to shuffle
/// * `dimensions` The number of dimensions per sample
/// * `rng` Random number generator
pub fn shuffle(
    samples: &mut [GFloat],
    count: usize,
    dimensions: usize,
    rng: &mut RNG
) {
    let ptr = samples.as_mut_ptr();
    let swap_a;
    let swap_b;
    unsafe {
        swap_a = slice::from_raw_parts_mut(ptr, samples.len());
        swap_b = slice::from_raw_parts_mut(ptr, samples.len());
    }
    for i in 0..count {
        let other = i + ((rng.gen_u64() as usize) % (count - i));
        for j in 0..dimensions {
            mem::swap(
                &mut swap_a[(i     * dimensions) + j],
                &mut swap_b[(other * dimensions) + j]
            );
        }
    }
}

/// Generates an arbitrary number of Latin Hypercube samples in an arbitrary
/// dimension.
///
/// * `samples` The array to generate samples into.
/// * `count` The number of n-dimensional samples to generate.
/// * `dimensions` The number of dimensions per sample
/// * `rng` Random number generator
pub fn latin_hypercube(
    samples: &mut [GFloat],
    count: usize,
    dimensions: usize,
    rng: &mut RNG
) {
    // generate LHS samples along diagonal
    let delta = 1.0 / (count as GFloat);
    for i in 0..count {
        for j in 0..dimensions {
            samples[(i * dimensions) + j] =
                (i as GFloat + (rng.gen_f32() as GFloat)) * delta;
        }
    }
    // permute LHS samples in each dimension
    let ptr = samples.as_mut_ptr();
    let swap_a;
    let swap_b;
    unsafe {
        swap_a = slice::from_raw_parts_mut(ptr, samples.len());
        swap_b = slice::from_raw_parts_mut(ptr, samples.len());
    }
    for i in 0..dimensions {
        for j in 0..count {
            let other = j + ((rng.gen_u64() as usize) % (count - j));
            mem::swap(
                &mut swap_a[(j     * dimensions) + i],
                &mut swap_b[(other * dimensions) + i]
            );
        }
    }
}

/// Concentricly maps co-ordinates on a square to co-ordinates on a unit disk
pub fn concentric_sample_disk(u: GFloat, v: GFloat) -> (GFloat, GFloat) {
    // map uniform random numbers to -1, 1
    let sx = (2.0 * u) - 1.0;
    let sy = (2.0 * v) - 1.0;
    // handle degeneracy at the origin
    if sx == 0.0 && sy == 0.0 {
        return (0.0, 0.0);
    }
    // map square to (r, Θ)
    let r;
    let mut theta;
    if sx >= -sy {
        if sx > sy {
            // handle first region of disk
            r = sx;
            if sy > 0.0 {
                theta = sy / r;
            }
            else {
                theta = 8.0 + (sy / r);
            }
        }
        else {
            // handle second region of disk
            r = sy;
            theta = 2.0 - (sx / r);
        }
    }
    else {
        if sx <= sy {
            // handle third region of disk
            r = -sx;
            theta = 4.0 - (sy / r);
        }
        else {
            // handle fourth region of disk
            r = -sy;
            theta = 6.0 + (sx / r);
        }
    }
    theta *= G_PI / 4.0;
    // return
    (r * theta.cos(), r * theta.sin())
}
