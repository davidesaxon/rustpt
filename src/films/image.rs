use std::fs::File;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

use tiff::encoder::*;

use crate::core::film::Film;
use crate::core::filter::*;
use crate::core::sampler::CameraSample;
use crate::core::spectrum::{self, Spectrum};
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                               C O N S T A N T S
// -----------------------------------------------------------------------------

const FILTER_TABLE_SIZE: usize = 16;

// -----------------------------------------------------------------------------
//                              I M A G E   F I L M
// -----------------------------------------------------------------------------

// ----------------------I M A G E   F I L M   S T R U C T----------------------
/// Film implementation that filters image samples with a reconstruction filter
/// and writes the resulting image to disk
pub struct ImageFilm {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    resolution: (usize, usize),
    // TODO: remove crop window?
    _crop_window: ((GFloat, GFloat), (GFloat, GFloat)),
    pixel_start: (i32, i32),
    n_pixels: (i32, i32),
    pixels: Vec<Mutex<Pixel>>,
    filter: FilterArc,
    filter_table: Vec<GFloat>,
    file_path: PathBuf
}

// ---------------I M A G E   F I L M   I M P L E M E N A T I O N---------------
impl ImageFilm {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [ImageFilm] from the given parameters
    ///
    /// * `resolution` The resolution of the image in pixels
    /// * `crop_window` Sub-rectangle `((x_min, y_min), (x_max, y_max))` of the
    ///                 image that will actually be rendered.
    /// * `filter` The [Filter] that will be used for image reconstruction.
    /// * `file_path` The file path the resulting image will be written to.
    pub fn new(
        resolution: (usize, usize),
        crop_window: ((GFloat, GFloat), (GFloat, GFloat)),
        filter: &FilterArc,
        file_path: &Path
    ) -> ImageFilm {
        let f_res = (resolution.0 as GFloat, resolution.1 as GFloat);
        let pixel_start = (
            (f_res.0 * crop_window.0.0).ceil() as i32,
            (f_res.1 * crop_window.0.1).ceil() as i32
        );
        let n_pixels = (
            (
                (f_res.0 * crop_window.1.0) - (pixel_start.0 as GFloat)
            ).ceil().max(1.0) as i32,
            (
                (f_res.1 * crop_window.1.1) - (pixel_start.1 as GFloat)
            ).ceil().max(1.0) as i32,
        );
        // allocate image data
        let mut pixels = Vec::with_capacity((n_pixels.0 * n_pixels.1) as usize);
        for _ in 0..(n_pixels.0 * n_pixels.1) as usize {
            pixels.push(Mutex::new(Pixel::new()));
        }
        // precompute filter weight table
        let fc = filter.common();
        let mut filter_table =
            Vec::with_capacity(FILTER_TABLE_SIZE * FILTER_TABLE_SIZE);
        for y in 0..FILTER_TABLE_SIZE {
            let fy =
                ((y as GFloat) + 0.5) *
                (fc.width.1 / (FILTER_TABLE_SIZE as GFloat));
            for x in 0..FILTER_TABLE_SIZE {
                let fx =
                    ((x as GFloat) + 0.5) *
                    (fc.width.0 / (FILTER_TABLE_SIZE as GFloat));
                filter_table.push(filter.evalulate((fx, fy)));
            }
        }
        // return
        ImageFilm{
            resolution: resolution,
            _crop_window: crop_window,
            pixel_start: pixel_start,
            n_pixels: n_pixels,
            pixels: pixels,
            filter: Arc::clone(filter),
            file_path: file_path.to_path_buf(),
            filter_table: filter_table
        }
    }
}

// --------------------F I L M   I M P L E M E N T A T I O N--------------------
impl Film for ImageFilm {
    fn resolution(&self) -> &(usize, usize) {
        &self.resolution
    }

    fn sample_extent(&self) -> ((i32, i32), (i32, i32)) {
        // we need to sample beyond the resolution to account for the
        // reconstruction filter
        let fc = self.filter.common();
        let pxs = self.pixel_start.0 as GFloat + 0.5;
        let pxe = pxs + self.n_pixels.0 as GFloat;
        let pys = self.pixel_start.1 as GFloat + 0.5;
        let pye = pys + self.n_pixels.1 as GFloat;
        (
            ((pxs - fc.width.0) as i32, (pys - fc.width.1) as i32),
            ((pxe + fc.width.0) as i32, (pye + fc.width.1) as i32)
        )
    }

    fn pixel_extent(&self) -> ((i32, i32), (i32, i32)) {
        (
            (self.pixel_start.0, self.pixel_start.0 + self.n_pixels.0),
            (self.pixel_start.1, self.pixel_start.1 + self.n_pixels.1)
        )
    }

    fn add_sample(&self, sample: &CameraSample, s: &Spectrum) {
        let fc = self.filter.common();
        // compute sample's raster extent
        let image_dx = sample.image_pos.0 - 0.5;
        let image_dy = sample.image_pos.1 - 0.5;
        let x0 =
            ((image_dx - fc.width.0).ceil() as i32).max(self.pixel_start.0);
        let y0 =
            ((image_dy - fc.width.1).ceil() as i32).max(self.pixel_start.1);
        let x1 =
            ((image_dx + fc.width.0).floor() as i32).min(
                self.pixel_start.0 + self.n_pixels.0 - 1
            );
        let y1 =
            ((image_dy + fc.width.1).floor() as i32).min(
                self.pixel_start.1 + self.n_pixels.1 - 1
            );
        if (x1 - x0) < 0 || (y1 - y0) < 0 {
            return;
        }
        // get xyz for radiance
        let xyz = s.to_xyz();
        // precompute x and y filter table offsets
        // TODO: this can be optmized! - alloca or use memory pool
        // TODO: https://stackoverflow.com/questions/27859822/is-it-possible-to-
        //       have-stack-allocated-arrays-with-the-size-determined-at-runtim
        let mut ifx = vec![0_i32; (x1 - x0 + 1) as usize];
        for x in x0..(x1 + 1) {
            let fx =
                (((x as GFloat) - image_dx) *
                fc.inv_width.0 *
                (FILTER_TABLE_SIZE as GFloat)).abs() as i32;
            ifx[(x - x0) as usize]  = fx.min(FILTER_TABLE_SIZE as i32 - 1);
        }
        let mut ify = vec![0_i32; (y1 - y0 + 1) as usize];
        for y in y0..(y1 + 1) {
            let fy =
                (((y as GFloat) - image_dy) *
                fc.inv_width.1 *
                (FILTER_TABLE_SIZE as GFloat)).abs() as i32;
            ify[(y - y0) as usize] = fy.min(FILTER_TABLE_SIZE as i32 - 1);
        }
        // loop over filter and add sample to pixel arrays
        for y in y0..(y1 + 1) {
            for x in x0..(x1 + 1) {
                // evalulate filter value (x, y) at pixel
                let offset =
                    (ify[(y - y0) as usize] * FILTER_TABLE_SIZE as i32) +
                    ifx[(x - x0) as usize];
                let filter_w = self.filter_table[offset as usize];
                // update pixel values with filtered sample contribution
                let pixel_mutex = &self.pixels[
                    (((y - self.pixel_start.1) * self.n_pixels.0) +
                    (x - self.pixel_start.0)) as usize
                ];
                // TODO: can we use atomic adds instead?
                let mut pixel = pixel_mutex.lock().unwrap();
                pixel.l_xyz.0 += xyz.0 * filter_w;
                pixel.l_xyz.1 += xyz.1 * filter_w;
                pixel.l_xyz.2 += xyz.2 * filter_w;
                pixel.weight_sum += filter_w;

            }
        }
    }

    fn splat(&self, sample: &CameraSample, s: &Spectrum) {
        // get xyz for radiance
        let xyz = s.to_xyz();
        // TODO: this currently just uses a pixel sized box filter - we should
        //       support filters here
        let x = sample.image_pos.0 as i32;
        let y  = sample.image_pos.1 as i32;
        if x < self.pixel_start.0 ||
           (x - self.pixel_start.0) >= self.n_pixels.0 ||
           y < self.pixel_start.1 ||
           (y - self.pixel_start.1) >= self.n_pixels.1
        {
            let pixel_mutex = &self.pixels[
                (((y - self.pixel_start.1) * self.n_pixels.0) +
                (x - self.pixel_start.0)) as usize
            ];
            // TODO: can we use atomic adds instead?
            let mut pixel = pixel_mutex.lock().unwrap();
            pixel.l_xyz.0 += xyz.0;
            pixel.l_xyz.1 += xyz.1;
            pixel.l_xyz.2 += xyz.2;
        }
    }

    fn write_image(&self, splat_scale: f32) {
        // convert image to RGB and comptue final pixel values
        let n_pixels = (self.n_pixels.0 * self.n_pixels.1) as usize;
        let mut rgb = vec![0.0_f32; 3 * n_pixels];
        let mut offset = 0;
        for y in 0..self.n_pixels.1 {
            for x in 0..self.n_pixels.0 {
                // convert pixel XYZ to RGB
                let pixel_mutex =
                    &self.pixels[((y * self.n_pixels.0) + x) as usize];
                let pixel = pixel_mutex.lock().unwrap();
                let mut rgb_p = spectrum::xyz_to_rgb(&pixel.l_xyz);
                // normalise pixel with weight sum
                if pixel.weight_sum != 0.0 {
                    let inv_wt = 1.0 / pixel.weight_sum;
                    // note: for now we clamp out of gamut (negative) values to
                    //       0
                    rgb_p.0 = (rgb_p.0 * inv_wt).max(0.0);
                    rgb_p.1 = (rgb_p.1 * inv_wt).max(0.0);
                    rgb_p.2 = (rgb_p.2 * inv_wt).max(0.0);
                }
                // add splat value at pixel
                let rgb_s = spectrum::xyz_to_rgb(&pixel.splat_xyz);
                rgb[offset + 0] = rgb_p.0 + (rgb_s.0 * splat_scale);
                rgb[offset + 1] = rgb_p.1 + (rgb_s.1 * splat_scale);
                rgb[offset + 2] = rgb_p.2 + (rgb_s.2 * splat_scale);
                // advance offset
                offset += 3;
            }
        }
        // write image
        // TODO: send to dispaly driver
        // TODO: this currently doesn't support crop window!
        // allocate tiff data
        let mut file = File::create(&self.file_path).unwrap();
        let mut tiff = TiffEncoder::new(&mut file).unwrap();
        let mut tiff_data = vec![0_u8; n_pixels * 3];
        for y in 0..(self.n_pixels.1 as usize) {
            for x in 0..(self.n_pixels.0 as usize) {
                let offset = 3 * ((y * (self.n_pixels.0) as usize) + x);
                tiff_data[offset + 0] = (rgb[offset + 0] * 255.0) as u8;
                tiff_data[offset + 1] = (rgb[offset + 1] * 255.0) as u8;
                tiff_data[offset + 2] = (rgb[offset + 2] * 255.0) as u8;
            }
        }
        tiff.write_image::<colortype::RGB8>(
            self.n_pixels.0 as u32,
            self.n_pixels.1 as u32,
            &tiff_data
        ).unwrap();
    }
}

// -----------------------------------------------------------------------------
//                                   P I X E L
// -----------------------------------------------------------------------------

// ---------------------------P I X E L   S T R U C T---------------------------
#[derive(Debug, Copy, Clone, PartialEq)]
struct Pixel {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    // stores the running weihted sums of pixel radiance values
    pub l_xyz: (SFloat, SFloat, SFloat),
    // the sum of filter weight values for the sample contributions to the pixel
    pub weight_sum: SFloat,
    // the unweight sum of sample splats
    pub splat_xyz: (SFloat, SFloat, SFloat)
}

// -------------------P I X E L   I M P L E M E N T A T I O N-------------------
impl Pixel {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new() -> Pixel {
        Pixel{
            l_xyz: (0.0, 0.0, 0.0),
            weight_sum: 0.0,
            splat_xyz: (0.0, 0.0, 0.0)
        }
    }
}
