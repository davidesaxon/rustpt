use std::rc::Rc;

use crate::core::geometry::traits::Eval;
use crate::core::math::{self, Dot};
use crate::core::shape::*;
use crate::core::geometry::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                                  S P H E R E
// -----------------------------------------------------------------------------

// --------------------------S P H E R E   S T R U C T--------------------------
/// A perfect sphere or partial perfect sphere
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Sphere {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    common: ShapeCommon,
    radius: GFloat,
    z_min: GFloat,
    z_max: GFloat,
    theta_min: GFloat,
    theta_max: GFloat,
    phi_max: GFloat,
}

// ------------------S P H E R E   I M P L E M E N T A T I O N------------------
impl Sphere {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new perfect [Sphere]
    ///
    /// * `object_to_world` Transforms from object space to world space
    /// * `world_to_object` Transforms from world space to object space
    /// * `reverse_orientation` Whether the orientation of this [Shape]'s
    ///                         geometry should be reversed or not.
    pub fn new(
        object_to_world: &Transform,
        world_to_object: &Transform,
        reverse_orientation: bool,
        radius: GFloat
    ) -> Sphere {
        Sphere{
            common: ShapeCommon::new(
                object_to_world,
                world_to_object,
                reverse_orientation
            ),
            radius: radius,
            z_min: -radius,
            z_max: radius,
            theta_min: (-1.0 as GFloat).acos(),
            theta_max: (1.0 as GFloat).acos(),
            phi_max: (360.0 as GFloat).to_radians()
        }
    }

    /// Constructs a new partial [Sphere]
    ///
    /// * `object_to_world` Transforms from object space to world space
    /// * `world_to_object` Transforms from world space to object space
    /// * `reverse_orientation` Whether the orientation of this [Shape]'s
    ///                         geometry should be reversed or not.
    /// * `radius` The radius of this partial [Sphere].
    /// * `z_min` The minimum z distance along the radius in z object space to
    ///           be considered part of this partial [Sphere]'s geometry (from
    ///           -1 to 1).
    /// * `z_max` The maximum z distance along the radius in z object space to
    ///           be considered part of this partial [Sphere]'s geometry (from
    ///           -1 to 1).
    /// * `phi_max` The maximum phi angle (in degrees) to be considered part of
    ///             this partial [Sphere]'s geometry.
    pub fn new_partial(
        object_to_world: &Transform,
        world_to_object: &Transform,
        reverse_orientation: bool,
        radius: GFloat,
        z_min: GFloat,
        z_max: GFloat,
        phi_max: GFloat
    ) -> Sphere {
        let z_min = (z_min * radius).clamp(-radius, radius);
        let z_max = (z_max * radius).clamp(-radius, radius);
        Sphere{
            common: ShapeCommon::new(
                object_to_world,
                world_to_object,
                reverse_orientation
            ),
            radius: radius,
            z_min: z_min,
            z_max: z_max,
            theta_min: (z_min / radius).clamp(-1.0, 1.0).acos(),
            theta_max: (z_max / radius).clamp(-1.0, 1.0).acos(),
            phi_max: phi_max.clamp(0.0, 360.0).to_radians()
        }
    }

    // ----------------------------PRIVATE FUNCTIONS----------------------------
    // the common logic between Shape::intersect and Shape::intersect_p
    fn intersect_common(
        &self,
        ray: &Ray
    ) -> Option<(GFloat, Point, GFloat)> {
        // transform the ray into object space
        let ray = self.common.world_to_object.eval(ray);
        // compute quadratic sphere coefficients
        let a =
            (ray.d.x  * ray.d.x) +
            (ray.d.y  * ray.d.y) +
            (ray.d.z  * ray.d.z);
        let b = 2.0 * (
            (ray.d.x * ray.o.x) +
            (ray.d.y * ray.o.y) +
            (ray.d.z * ray.o.z)
        );
        let c =
            (ray.o.x * ray.o.x) +
            (ray.o.y * ray.o.y) +
            (ray.o.z * ray.o.z) -
            (self.radius * self.radius);
        // solve the quadratic equation for t values
        let (t0, t1) = if let Some(t) = math::quadratic(a, b, c) {
            t
        }
        else {
            return None;
        };
        // handle only one result
        if t0.is_nan() || t1.is_nan() {
            return None;
        }
        // compute intersection distance along ray
        if t0 > ray.max_t || t1 < ray.min_t {
            return None;
        }
        let mut t_hit = if t0 < ray.min_t {
            if t1 > ray.max_t {
                return None;
            }
            t1
        }
        else {
            t0
        };
        // TODO: this can be cleaned up optimised
        // compute sphere hit position and phi
        let mut p_hit = ray.eval(t_hit);
        // TODO: REMOVE ME
        if p_hit.has_nans() {
            panic!(
                "nan phit: ray: {:?} t_hit: {} abc: {},{},{} t0: {} t1: {}",
                ray,
                t_hit,
                a,
                b,
                c,
                t0,
                t1
            );
        }
        if p_hit.x == 0.0 && p_hit.y == 0.0 {
            p_hit.x = 1e-5 * self.radius;
        }
        let mut phi = p_hit.y.atan2(p_hit.x);
        if phi < 0.0 {
            phi += 2.0 * G_PI;
        }
        // test sphere intersection against clipping parameters
        if (self.z_min > -self.radius && p_hit.z < self.z_min) ||
           (self.z_max <  self.radius && p_hit.z > self.z_max) ||
           phi > self.phi_max {
            if t_hit == t1 {
                return None;
            }
            if t1 > ray.max_t {
                return None;
            }
            t_hit = t1;
            // conmpute sphere hit position and phi for t1 to retry
            p_hit = ray.eval(t_hit);
            if p_hit.x == 0.0 && p_hit.y == 0.0 {
                p_hit.x = 1e-5 * self.radius;
            }
            phi = p_hit.y.atan2(p_hit.x);
            if phi < 0.0 {
                phi += 2.0 * G_PI;
            }
            // test sphere intersection against clipping parameters again
            if (self.z_min > -self.radius && p_hit.z < self.z_min) ||
               (self.z_max <  self.radius && p_hit.z > self.z_max) ||
               phi > self.phi_max {
                return None;
            }
        }
        // return
        Some((t_hit, p_hit, phi))
    }
}

// ---------------------S P H E R E   S H A P E   T R A I T---------------------
impl Shape for Sphere {
    fn common(&self) -> &ShapeCommon {
        &self.common
    }

    fn object_bound(&self) -> AABB {
        // TODO: improve this to also take phi_max into account
        AABB::new(
            &Point::new(-self.radius, -self.radius, self.z_min),
            &Point::new( self.radius,  self.radius, self.z_max),
        )
    }

    fn intersect(
        &self,
        ray: &Ray,
        this: &ShapeArc
    ) -> Option<(GFloat, GFloat, Rc<DifferentialGeometry>)> {
        // call common implementation
        let (t_hit, p_hit, phi) =
            if let Some(data) = self.intersect_common(ray) {
                data
            }
            else {
                return None;
            };
        // find the parametric representation of the sphere hit
        let u = phi / self.phi_max;
        let theta = (p_hit.z / self.radius).clamp(-1.0, 1.0).acos();
        let v = (theta - self.theta_min) / (self.theta_max - self.theta_min);
        // compute sphere dpdu and dpdv
        let z_radius = ((p_hit.x * p_hit.x) + (p_hit.y * p_hit.y)).sqrt();
        let inv_z_radius = 1.0 / z_radius;
        let cos_phi = p_hit.x * inv_z_radius;
        let sin_phi = p_hit.y * inv_z_radius;
        let dpdu = Vector::new(
            -self.phi_max * p_hit.y,
            self.phi_max * p_hit.x,
            0.0
        );
        let dpdv = &Vector::new(
            p_hit.z * cos_phi,
            p_hit.z * sin_phi,
            -self.radius * theta.sin()
        ) * (self.theta_max - self.theta_min);
        // compute dndu and dndv
        let d2pduu =
            &Vector::new(p_hit.x, p_hit.y, 0.0) *
            (-self.phi_max * self.phi_max);
        let d2pduv =
            &Vector::new(-sin_phi, cos_phi, 0.0) *
            ((self.theta_max - self.theta_min) * p_hit.z * self.phi_max);
        let d2pdvv =
            &Vector::new(p_hit.x, p_hit.y, p_hit.z) *
            (-(self.theta_max - self.theta_min) *
             (self.theta_max - self.theta_min));
        // compute coefficients for fundamental forms
        let e = dpdu.dot(&dpdu);
        let f = dpdu.dot(&dpdv);
        let g = dpdv.dot(&dpdv);
        let n = dpdu.cross(&dpdv).normalize();
        let e2 = n.dot(&d2pduu);
        let f2 = n.dot(&d2pduv);
        let g2 = n.dot(&d2pdvv);
        //compute dndu and dndv from fundamental form coefficients
        let inv_egff = 1.0 / (e * g - f * f);
        let dndu = Normal::from_vector(
            &(&(&dpdu * ((f2 * f - e2 * g) * inv_egff)) +
              &(&dpdv * ((e2 * f - f2 * e) * inv_egff)))
        );
        let dndv = Normal::from_vector(
            &(&(&dpdu * ((g2 * f - f2 * g) * inv_egff)) +
              &(&dpdv * ((f2 * f - g2 * e) * inv_egff)))
        );
        // initialise differential geometry from parametric information
        let object_to_world: &Transform = &self.common().object_to_world;
        let dg = Rc::new(DifferentialGeometry::new(
            object_to_world.eval(&p_hit),
            u,
            v,
            object_to_world.eval(&dpdu),
            object_to_world.eval(&dpdv),
            object_to_world.eval(&dndu),
            object_to_world.eval(&dndv),
            this
        ));
        // return
        Some((t_hit, 5e-4 * t_hit, dg))
    }

    fn intersect_p(&self, ray: &Ray) -> bool {
        // call common implementation
        match self.intersect_common(ray) {
            Some(_) => true,
            None => false
        }
    }

    fn surface_area(&self) -> GFloat {
        self.phi_max * self.radius * (self.z_max - self.z_min)
    }
}
