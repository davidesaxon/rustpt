use std::sync::Arc;

use crate::accelerators::GridAccelerator;
use crate::core::geometry::{Transform, Vector};
use crate::core::primitive::{GeometricPrimitive, Primitive, PrimitiveArc};
use crate::core::shape::*;
use crate::proto::refine_sphere::RefineSphere;
use crate::shapes::Sphere;

// ------------------------------F U N C T I O N S------------------------------

pub fn build() -> PrimitiveArc {
    // build spheres
    let sphere1_t = Transform::translation(&Vector::new(35.0, 0.0, 35.0));
    let sphere1: ShapeArc = Arc::new(Sphere::new(
        &sphere1_t,
        &sphere1_t.inverse(),
        false,
        25.0
    ));
    let prim_1 = Arc::new(GeometricPrimitive::proto_new(&sphere1));
    //
    let sphere2_t = Transform::translation(&Vector::new(-35.0, 0.0, 35.0));
    let sphere2: ShapeArc = Arc::new(Sphere::new(
        &sphere2_t,
        &sphere2_t.inverse(),
        false,
        25.0
    ));
    let prim_2 = Arc::new(GeometricPrimitive::proto_new(&sphere2));
    //
    let sphere3_t = Transform::translation(&Vector::new(-35.0, 0.0, -35.0));
    let sphere3: ShapeArc = Arc::new(Sphere::new(
        &sphere3_t,
        &sphere3_t.inverse(),
        false,
        25.0
    ));
    let prim_3 = Arc::new(GeometricPrimitive::proto_new(&sphere3));
    //
    let t4 = Transform::translation(&Vector::new(35.0, 0.0, -35.0));
    let shape4: ShapeArc = Arc::new(RefineSphere::new(
        &t4,
        &t4.inverse()
    ));
    let prim_4 = Arc::new(GeometricPrimitive::proto_new(&shape4));

    println!(
        "intial prims: {}, {}, {}, {}",
        prim_1.id(),
        prim_2.id(),
        prim_3.id(),
        prim_4.id()
    );

    // build the aggregate
    Arc::new(GridAccelerator::new(vec![prim_1, prim_2, prim_3, prim_4], false))
}
