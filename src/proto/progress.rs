use std::time::Instant;

use log::info;


pub struct ProgressReporter {
    n_tasks: usize,
    task_p: Vec<f32>,
    current_p: usize,
    timer: Instant,
}

impl ProgressReporter {
    pub fn new(n_tasks: usize) -> ProgressReporter {
        let task_p = vec![0.0; n_tasks];
        ProgressReporter{
            n_tasks: n_tasks,
            task_p: task_p,
            current_p: 0,
            timer: Instant::now()
        }
    }

    pub fn report(&mut self, n: usize, p: f32)
    {
        // println!("P: {}", p);
        self.task_p[n] = p;
        let mut total = 0;
        for tp in &self.task_p {
            total += (tp * 100.0) as usize;
        }
        // println!("TOTAL: {}", total);
        total /= self.n_tasks;
        if total >= self.current_p + 10 ||
           (self.timer.elapsed().as_secs_f32() >= 5.0 &&
            total != self.current_p)
        {
            info!("-> {}%", total);
            self.current_p = total;
            self.timer = Instant::now();
        }
    }
}
