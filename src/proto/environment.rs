use std::fs::File;

use log::info;

use crate::core::geometry::*;
use crate::core::types::*;


// -----------------------------------------------------------------------------
//                             E N V I R O N M E N T
// -----------------------------------------------------------------------------

pub struct Environment {
    pub pixel_width: usize,
    pub pixel_height: usize,
    pub colour: Vec<(f32, f32, f32)>,
}

impl Environment {
    pub fn new(image_path: &String) -> Environment {
        info!("loading environment image...");
        // open image file
        let decoder = png::Decoder::new(
            File::open(image_path).unwrap()
        );
        let (info, mut reader) = decoder.read_info().unwrap();
        // check we can load
        if info.color_type != png::ColorType::RGB  {
            panic!(
                "Don't know how to load color type {:?} from image {}",
                info.color_type,
                image_path
            );
        }
        if info.bit_depth != png::BitDepth::Eight {
            panic!(
                "Don't know how to load bit depth {:?} from image {}",
                info.bit_depth,
                image_path
            );
        }
        // load
        let mut buf = vec![0; info.buffer_size()];
        reader.next_frame(&mut buf).unwrap();
        // convert to colour data
        let mut colour: Vec<(f32, f32, f32)> = vec![
            (0.0, 0.0, 0.0);
            (info.width * info.height) as usize
        ];
        for y in 0..info.height {
            for x in 0..info.width {
                let offset = ((y * info.width) + x) as usize;
                colour[offset] = (
                    (buf[(offset * 3) + 0] as f32) / 255.0,
                    (buf[(offset * 3) + 1] as f32) / 255.0,
                    (buf[(offset * 3) + 2] as f32) / 255.0,
                );
            }
        }

        Environment{
            pixel_width: info.width as usize,
            pixel_height: info.height as usize,
            colour: colour
        }
    }

    pub fn intersect(&self, ray: &Ray) -> (f32, f32, f32) {
        let n_d = ray.d.normalize();
        let mut x = ((n_d.z.atan2(n_d.x) / 3.1415926) + 1.0) / 2.0;
        x = x.clamp(0.0, 1.0);
        let mut y = (-n_d.y + 1.0) / 2.0;
        y = y.clamp(0.0, 1.0);
        let offset_x = (x * (self.pixel_width - 1) as GFloat) as usize;
        let offset_y = (y * (self.pixel_height - 1) as GFloat) as usize;
        // return
        self.colour[(offset_y * self.pixel_width) + offset_x]
    }
}
