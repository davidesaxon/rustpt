use std::sync::Arc;

use crate::core::geometry::{AABB, Point, Transform, Vector};
use crate::core::shape::*;
use crate::shapes::Sphere;


#[derive(Debug, Copy, Clone, PartialEq)]
pub struct RefineSphere {
    common: ShapeCommon
}

impl RefineSphere {
    pub fn new(
        object_to_world: &Transform,
        world_to_object: &Transform
    ) -> RefineSphere {
        RefineSphere{
            common: ShapeCommon::new(
                object_to_world,
                world_to_object,
                false
            )
        }
    }
}

impl Shape for RefineSphere {
    fn common(&self) -> &ShapeCommon {
        &self.common
    }

    fn object_bound(&self) -> AABB {
        // TODO:
        AABB::new(
            &Point::new(-35.0, -35.0, -35.0),
            &Point::new( 35.0,  35.0,  35.0)
        )
    }

    fn can_intersect(&self) -> bool {
        false
    }

    fn refine(&self) -> Vec<ShapeArc> {
        let mut t1 = Transform::translation(&Vector::new(10.0, -10.0, 0.0));
        t1 = &self.common.object_to_world * &t1;
        let sphere1: ShapeArc = Arc::new(Sphere::new(
            &t1,
            &t1.inverse(),
            false,
            10.0
        ));
        let mut t2 = Transform::translation(&Vector::new(-10.0, 10.0, 0.0));
        t2 = &self.common.object_to_world * &t2;
        let sphere2: ShapeArc = Arc::new(Sphere::new(
            &t2,
            &t2.inverse(),
            false,
            10.0
        ));
        vec![sphere1, sphere2]
    }
}
