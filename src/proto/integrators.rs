use std::sync::Arc;

use crate::core::geometry::*;
use crate::core::integrator::*;
use crate::core::intersection::Intersection;
use crate::core::math::Dot;
use crate::core::renderer::RendererInterface;
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::scene::Scene;
use crate::core::spectrum::Spectrum;


pub struct ProtoSurfaceIntegrator {
}

impl SurfaceIntegrator for ProtoSurfaceIntegrator {
    fn li(
        &self,
        scene: &Arc<Scene>,
        renderer: &dyn RendererInterface,
        ray: &mut Ray,
        intersection: &Intersection,
        sample: Option<&Sample>,
        rng: &mut RNG,
    ) -> Spectrum {
        let dg: &DifferentialGeometry = &intersection.dg;

        let this_li;
        if dg.p.x < 0.0 {
            if dg.p.z < 0.0 {
                this_li = Spectrum::from_rgb(&(1.0, 0.4, 0.4));
            }
            else {
                this_li = Spectrum::from_rgb(&(1.0, 1.0, 1.0));
            }
        }
        else {
            if dg.p.z > 0.0 {
                this_li = Spectrum::from_rgb(&(0.4, 1.0, 0.7));
            }
            else {
                this_li = Spectrum::from_rgb(&(1.0, 1.0, 1.0));
            }
        }

        let mut d = ray.d.normalize();
        if d.x == 0.0 && d.y == 0.0 && d.z == 0.0 {
            d = Vector::new(1.0, 0.0, 0.0);
        }
        let mut n = Vector::from_normal(&dg.n.normalize());
        if n.x == 0.0 && n.y == 0.0 && n.z == 0.0 {
            n = Vector::new(1.0, 0.0, 0.0);
        }
        let reflect = &d - &(&n * (2.0 * d.dot(&n)));

        let mut next_ray = Ray::new_child(
            &dg.p,
            &reflect,
            0.001,
            ray
        );
        // recurse
        let (next_li, _, _) = renderer.li(scene, &mut next_ray, sample, rng);
        // return
        &this_li * &next_li
    }
}

pub struct ProtoVolumeIntegrator {
}

impl VolumeIntegrator for ProtoVolumeIntegrator {
    fn li(
        &self,
        _scene: &Arc<Scene>,
        _renderer: &dyn RendererInterface,
        _ray: &mut Ray,
        _sample: Option<&Sample>,
        _rng: &mut RNG,
        // TODO: memory pool
    ) -> (Spectrum, Spectrum) {
        // TODO:
        (Spectrum::black(), Spectrum::white())
    }
}
