use std::sync::{Arc, Mutex};
use std::thread;

use log::{info, warn};

use crate::core::camera::*;
use crate::core::geometry::Ray;
use crate::core::integrator::*;
use crate::core::intersection::Intersection;
use crate::core::renderer::{RendererInterface, RendererInterfaceArc};
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::scene::Scene;
use crate::core::spectrum::*;
use crate::core::types::*;
// TODO: REMOVE ME
use crate::proto::environment::Environment;
use crate::proto::progress::ProgressReporter;


// -----------------------------------------------------------------------------
//                         S A M P L E   R E N D E R E R
// -----------------------------------------------------------------------------

// -----------------S A M P L E   R E N D E R E R   S T R U C T-----------------
/// Renderer that renders the scene by sampling points on the image plane to
/// compute light contribution
pub struct SampleRenderer {
    // ----------------------------PUBLIC ATTRIBUTES----------------------------
    // TODO: the camera should come from the scene?
    camera: CameraArc,
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    interface: RendererInterfaceArc,
    sampler: SamplerArc,
}

// ---------S A M P L E   R E N D E R E R   I M P L E M E N T A T I O N---------
impl SampleRenderer {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new [SampleRenderer] from the given parameters
    pub fn new(
        camera: &CameraArc,
        sampler: &SamplerArc,
        surface_integrator: &SurfaceIntegratorArc,
        volume_integrator: &VolumeIntegratorArc,
        // TODO: REMOVE ME
        environment: &Arc<Environment>
    ) -> SampleRenderer {
        SampleRenderer{
            camera: Arc::clone(camera),
            interface: Arc::new(SampleRendererInterface{
                surface_integrator: Arc::clone(surface_integrator),
                volume_integrator: Arc::clone(volume_integrator),
                environment: Arc::clone(environment)
            }),
            sampler: Arc::clone(sampler)
        }
    }

    // -----------------------------PUBLIC FUNCTIONS----------------------------
    /// Renders the given scene
    pub fn render(&self, scene: &Arc<Scene>) {
        // TODO: REMOVE ME
        warn!("SampleRenderer::render has an unfinished implementation");

        // integrator preprocessing
        info!("Preprocessing intergrators...");
        self.interface.surface_integrator().preprocess(
            &self.camera,
            scene,
            &self.interface
        );
        self.interface.volume_integrator().preprocess(
            &self.camera,
            scene,
            &self.interface
        );

        // compute the number of render tasks to create
        // TODO: this could go into a function with better smarts
        // TODO: n_pixels could be defined on the camera?
        // let n_pixels =
        //     self.camera.common().film.res_x *
        //     self.camera.common().film.res_y;
        // TODO: need a scheduler for this
        // let n_tasks = math::round_up_pow2(cmp::max(
        //     (num_cpus::get() * 32) as i32,
        //     (n_pixels / (16 * 16)) as i32
        // )) as usize;
        let n_tasks = 8;

        // ---------------------------------------------------------------------
        // TODO: PROTOTYPING
        // ---------------------------------------------------------------------
        let progress_reporter = Arc::new(Mutex::new(
            ProgressReporter::new(n_tasks)
        ));
        // ---------------------------------------------------------------------

        // build the tasks
        // TODO:
        // let mut render_tasks: Vec<SamplerRendererTask> = Vec::new();
        // for n in 0..n_tasks {
        //     // TODO: propbably need to use Arc pointers for arguments
        //     render_tasks.push(SamplerRendererTask::new(
        //         scene,
        //         &self,
        //         &self.camera,
        //         &self.sampler,
        //         &sample,
        //         // TODO: why does this need to be backwards?
        //         n_tasks - 1 - n,
        //         n_tasks
        //     ));
        // }
        // run and then wait for tasks
        // TODO: should we actually use a scheduler object here?
        // enqueue_tasks(render_tasks);
        // wait_for_all_tasks();

        // ---------------------------------------------------------------------
        // TODO: PROTOTYPING
        // ---------------------------------------------------------------------
        let mut handles = Vec::new();
        for n in 0..n_tasks {
            let task = SamplerRenderTask::new(
                n_tasks - 1 - n,
                n_tasks,
                &self.camera,
                &scene,
                &self.interface,
                &self.sampler,
                &progress_reporter
            );
            handles.push(thread::spawn(move || {task.run();}));
        }
        for handle in handles {
            handle.join().unwrap();
        }
        // ---------------------------------------------------------------------


        // free render task and sample memory before proceeding
        // mem::drop(render_tasks);

        // write the image to disk
        // TODO:
        // camera.film.write_image();
    }
}

// -----------------------------------------------------------------------------
//               S A M P L E   R E N D E R E R   I N T E R F A C E
// -----------------------------------------------------------------------------

// -------S A M P L E   R E N D E R E R   I N T E R F A C E   S T R U C T-------
pub struct SampleRendererInterface {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    surface_integrator: SurfaceIntegratorArc,
    volume_integrator: VolumeIntegratorArc,
    // TODO: REMOVE ME
    environment: Arc<Environment>,
}

// ------R E N D E R E R   I N T E R F A C E   I M P L E M E N T A T I O N------
impl RendererInterface for SampleRendererInterface {
    fn surface_integrator(&self) -> &SurfaceIntegratorArc {
        &self.surface_integrator
    }

    fn volume_integrator(&self) -> &VolumeIntegratorArc {
        &self.volume_integrator
    }

    fn li(
        &self,
        scene: &Arc<Scene>,
        ray: &mut Ray,
        sample: Option<&Sample>,
        rng: &mut RNG
    ) -> (Spectrum, Spectrum, Option<Intersection>) {

        // TODO: REMOVE DEPTH CHECK
        let mut intersect = None;
        if ray.depth < 5 {
            intersect = scene.intersect(ray)
        }
        let (li, isect) = if let Some(i) = intersect {
            let li = self.surface_integrator.li(
                scene,
                self,
                ray,
                &i,
                sample,
                rng
            );
            (li, Some(i))
        } else {
            // TODO: this could be optimised to only run for environment lights?
            // get any light contributions from lights
            let li = Spectrum::from_rgb(&self.environment.intersect(ray));
            // TODO:
            // for &light in scene.lights {
            //     li += light.le(ray);
            // }
            (li, None)
        };
        let (lvi, t) = self.volume_integrator.li(
            scene,
            self,
            ray,
            sample,
            rng
        );
        //
        (&(&t * &li) + &lvi, t, isect)
    }

    fn transmittance(
        &self,
        _scene: &Arc<Scene>,
        _ray: &mut Ray,
        _sample: Option<&Sample>,
        _rng: &mut RNG
    ) -> Spectrum {
        // self.volume_integrator.transmittance(scene, self, ray, sample, rng, arena)
        panic!("SampleRendererInterface::transmittance implementation unfinished");
    }
}

// -----------------------------------------------------------------------------
//                      S A M P L E   R E N D E R   T A S K
// -----------------------------------------------------------------------------

// --------------S A M P L E   R E N D E R   T A S K   S T R U C T--------------
struct SamplerRenderTask {
    // ----------------------------PRIVATE ATTRIBUTES---------------------------
    n: usize,
    n_tasks: usize,
    camera: CameraArc,
    scene: Arc<Scene>,
    interface: RendererInterfaceArc,
    main_sampler: SamplerArc,
    // TODO: REMOVE ME
    progress_reporter: Arc<Mutex<ProgressReporter>>,
}

// ------S A M P L E   R E N D E R   T A S K   I M P L E M E N T A T I O N------
impl SamplerRenderTask {
    // -------------------------------CONSTRUCTORS------------------------------
    pub fn new(
        n: usize,
        n_tasks: usize,
        camera: &CameraArc,
        scene: &Arc<Scene>,
        interface: &RendererInterfaceArc,
        main_sampler: &SamplerArc,
        progress_reporter: &Arc<Mutex<ProgressReporter>>
    ) -> SamplerRenderTask {
        SamplerRenderTask{
            n: n,
            n_tasks: n_tasks,
            camera: Arc::clone(camera),
            scene: Arc::clone(scene),
            interface: Arc::clone(interface),
            main_sampler: Arc::clone(main_sampler),
            progress_reporter: Arc::clone(progress_reporter)
        }
    }
    // -----------------------------PUBLIC FUNCTIONS----------------------------
    pub fn run(&self) {
        // get the sub sampler that will only gnerate samples for the subset of
        // the image this task is rendering for. The main sampler is responsible
        // for determining the image image region based on the task number and
        // the number of tasks
        let mut sampler = if let Some(s) = self.main_sampler.get_sub_sampler(
            self.n,
            self.n_tasks
        ).as_mut() {
            Arc::clone(s)
        }
        else {
            return;
        };

        // TODO: use memory pool?
        // let arena = MemoryArena::new();
        let mut rng = RNG::new_seed(self.n as u64);

        // allocate space for samples and intersections based on the maximum
        // number of samples that can be returned at once from the sampler
        let max_samples = sampler.max_sample_count();
        let mut samples = Vec::with_capacity(max_samples);
        let mut rays = Vec::with_capacity(max_samples);
        let mut ls = Vec::with_capacity(max_samples);
        let mut intersections = Vec::with_capacity(max_samples);
        for _ in 0..max_samples {
            samples.push(Sample::new(
                &sampler,
                Some(self.interface.surface_integrator()),
                Some(self.interface.volume_integrator()),
                &self.scene
            ));
        }
        // // spectra for the radiance along the ray
        // let mut ls = [Spectrum::new(), max_samples];
        // // the transmittance along the ray
        // let mut ts = [Spectrum::new(), max_samples];
        // let mut intersections = [Intersection::new(), max_samples];

        let sampler = Arc::get_mut(&mut sampler).unwrap();

        // render samples from the sampler until it has finished generating
        // samples
        let mut sample_count = sampler.get_more_samples(&mut samples, &mut rng);
        while sample_count > 0 {
            // generate camera rays for each sample and compute radiance to the
            // sampler
            for s in 0..sample_count {
                let sample = &samples[s];
                // TODO: is preallocating rays like samples faster?
                // find camera ray for this sample - the returned ray weight
                // determines how much this ray actually contributions to the
                // final image based on the lens of the camera model
                let (mut ray, ray_weight) =
                    self.camera.generate_ray_differential(
                        &sample.camera_sample
                    );
                // scales the differental rays to account for the actual spacing
                // between each pixel samples on the film plane
                ray.scale_differentials(
                    1.0 / (sampler.common().samples_per_pixel as GFloat).sqrt()
                );
                // evaluate radiance along the ray
                let (li, isect) = if ray_weight > 0.0 {
                    let (li, _, isect) = self.interface.li(
                        &self.scene,
                        &mut ray,
                        Some(&sample),
                        &mut rng
                    );
                    (&li * ray_weight, isect)
                }
                else {
                    (Spectrum::black(), None)
                };
                // check if any unexpected values are found
                debug_assert!(
                    !li.has_nans(),
                    "NaN radiance computed!"
                );
                debug_assert!(
                    li.to_y() >= -1e-5,
                    "Negative luminance {} computed!",
                    li.to_y()
                );
                debug_assert!(
                    li.to_y() != SFloat::INFINITY,
                    "Ifinite luminance computed!",
                );
                // update vectors
                rays.push(ray);
                ls.push(li);
                intersections.push(isect);
            }

            // report sample results to Sampler - and then add contributions to
            // the image
            // note: the sampler can decide that results should be discarded
            //       rather than added to the image
            let (use_samples, progress) = sampler.report_results(
                &samples,
                &rays,
                &ls,
                &intersections,
                sample_count
            );
            if use_samples {
                for s in 0..sample_count {
                    self.camera.common().film.add_sample(
                        &samples[s].camera_sample,
                        &ls[s]
                    );
                }
            }

            // report progress
            let mut progress_reporter = self.progress_reporter.lock().unwrap();
            progress_reporter.report(self.n, progress);

            // clean up
            rays.clear();
            ls.clear();
            intersections.clear();

            // get next samples
            sample_count = sampler.get_more_samples(&mut samples, &mut rng);
        }
    }
}
