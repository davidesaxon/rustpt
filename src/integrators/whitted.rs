use std::sync::Arc;

use crate::core::geometry::Ray;
use crate::core::integrator::SurfaceIntegrator;
use crate::core::intersection::Intersection;
use crate::core::renderer::RendererInterface;
use crate::core::rng::RNG;
use crate::core::sampler::*;
use crate::core::scene::Scene;
use crate::core::spectrum::Spectrum;


// -----------------------------------------------------------------------------
//                      W H I T T E D   I N T E G R A T O R
// -----------------------------------------------------------------------------

// --------------W H I T T E D   I N T E G R A T O R   S T R U C T--------------
/// Simple integrator that can compute reflected and transmitted light from specular surfaces
pub struct WhittedIntegrator {
    /// the maximum number of rays that will be traced per sample
    _max_depth: u32
}

// ------W H I T T E D   I N T E G R A T O R   I M P L E M E N T A T I O N------
impl WhittedIntegrator {
    // -------------------------------CONSTRUCTORS------------------------------
    /// Constructs a new WhittedIntegrator
    pub fn _new(max_depth: u32) -> WhittedIntegrator {
        WhittedIntegrator{_max_depth: max_depth}
    }
}

// ---------------S U R F A C E   I N T E G R A T O R   T R A I T---------------
impl SurfaceIntegrator for WhittedIntegrator {
    // ---------------------------F U N C T I O N S-----------------------------

    fn li(
        &self,
        _scene: &Arc<Scene>,
        _renderer: &dyn RendererInterface,
        _ray: &mut Ray,
        _intersection: &Intersection,
        _sample: Option<&Sample>,
        _rng: &mut RNG,
    ) -> Spectrum {
        // TODO: continue
        // let mut l = Spectrum::black();
        // get the BSDF at the ray hit point
        // TODO:

        panic!("WhittedIntegrator::li not implemented");
    }

    // TODO:
    // fn li(
    //     &self,
    //     scene: &Scene,
    //     renderer: Box<dyn Renderer>,
    //     ray: &RayDifferential,
    //     intersection: &Intersection,
    //     sample: &Sample,
    //     rng: &mut Rng,
    //     arena: &mut MemoryArena
    // ) -> Spectrum {
    //     l = Spectrum::new(0.0f32);
    //     // get the BSDF at the ray hit point
    //     bsdf = intersection.get_bsdf(ray, arena);
    //     // get intersection data
    //     p = &bsdf.dg_shading.p;
    //     n = &bsdf.dg_shading.nn;
    //     wo = -ray.d;
    //     // get emiited light (returns black if object is not emissive)
    //     l += intersection.le(wo);
    //     // add the contribution of each light source
    //     for &light in scene.lights {
    //         // determine the radiance from the light falling on the surface, the direction vector
    //         // from the shaded point to the light, the probably density for the light to have
    //         // sampled the direction wi, and a visibilty tester to determine whether an primitives
    //         // are between the surface and the light
    //         let (li, wi, pdf, visibility)  = light.sample_l(
    //             &p,
    //             intersection.ray_epsilon,
    //             LightSample::new(rng),
    //             ray.time,
    //         );
    //         if li.is_black() || pdf == 0.0f32 {
    //             continue;
    //         }
    //         // determine whether any light is scattered along the direction of the viewing ray (wo)
    //         let f = bsdf.f(wo, wi);
    //         // check whether the light is occluded or not
    //         if !f.is_black() && visibilty.unoccluded(scene) {
    //             // TODO: linear algebra library??
    //             // add light contribution
    //             l +=
    //                 f * li * wi.abs().dot(&n.abs()) *
    //                 visibilty.transmittance(scene, renderer, sample, rng, arena) / pfd;
    //         }
    //     }
    //     // recursively trace?
    //     if (ray.depth + 1) < self.max_depth {
    //         // trace rays for specular reflection and refraction
    //         l += integrator::specular_reflect(
    //             ray,
    //             &bsdf,
    //             rng,
    //             intersection,
    //             renderer,
    //             scene,
    //             sample,
    //             arena
    //         );
    //         l += integrator::specular_transmit(
    //             ray,
    //             &bsdf,
    //             rng,
    //             intersection,
    //             renderer,
    //             scene,
    //             sample,
    //             arena
    //         );
    //     }
    //     l
    // }
}
